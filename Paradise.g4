grammar Paradise;

source: sourceItem*;
sourceItem:
    FUNCTION functionSignature
       functionBody
    END_FUNCTION;

functionBody: statement* ;
functionSignature: identifier functionArgs;
functionArgs: LBRACE argDefList RBRACE ('as' typeRef)?;

typeRef
    : 'bool'
    | 'byte'
    | 'int'
    | 'uint'
    | 'long'
    | 'ulong'
    | 'char'
    | 'string'
    | identifier
    ;

statement
    : 'if' ifCondition 'then' ifTrueStatement 'end if' #IfThenStmnt
    | 'if' ifCondition 'then' ifTrueStatement 'else' ifFalseStatement 'end if' #IfThenElseStmnt
    | 'while' whileCondition whileBody 'wend' #WhileStmnt
    | 'do' doWhileBody 'loop' ('while'|'until') doWhileCondition #DoWhileStmnt
    | 'break' ';' #BreakStmnt
    | identifier '=' expr ';' #IdentifierStmnt
    | expr ';' #ExprStmnt
    | 'return' expr ';' #ReturnStmnt
    | sourceItem #NestedFunctionDeclaration
    ;

whileCondition: expr;
whileBody: statement*;
doWhileBody: statement*;
doWhileCondition: expr;
ifCondition: expr;
ifTrueStatement: statement*;
ifFalseStatement: statement*;

expr
    : expr binary expr  #BinaryExpr
    | ( BOOL | STRING | CHAR | HEX | BITS | DEC ) #ValueExpr
    | unary expr #PrefixUnaryExpr
    | expr  unary #PostfixUnaryExpr
    | expr '(' exprList ')' #FunctionCall
    | identifier #Variable
    | LBRACE expr RBRACE #BracedExpr
    ;

condition: expr;

argDef: identifier 'as' typeRef ;
argDefList: (argDef (',' argDef)*)? ;
exprList:  (expr (',' expr)*)? ;
identifier: IDENTIFIER;

fragment LETTERS : [a-zA-Z_]+;
fragment DIGIT : [0-9]+;

FUNCTION: 'function';
END_FUNCTION: 'end function';

binary : '+' #SumBinary
       | '-' #SubBinary
       | '/' #DivBinary
       | '*' #MulBinary
       | '&' #ByteAndBinary
       | '&&' #AndBinary
       | '|' #ByteOrBinary
       | '||' #OrBinary
       | '==' #EqBinary
       | '<' #LtBinary
       | '<=' #LtOrEtBinary
       | '>' #GtBinary
       | '>=' #LtOrEtBinary
       | '%' #ModBinary
       ;

unary  : '+' #SumUnary
       | '-' #SubUnary
       | '!' #NotUnary
       | '--' #DecrementUnary
       | '++' #IncrementUnary
       ;

LBRACE: '(';
RBRACE: ')';
CHAR : '\'' .? '\'';
STRING: '"' .*? '"'  ;
HEX: '0'[xX][0-9A-Fa-f]+;
BITS: '0'[bB][01]+;
DEC: DIGIT+;
BOOL : 'true' | 'false' ;
IDENTIFIER: LETTERS(LETTERS|DIGIT)*;

COMMENT: '#' .*? '\n' -> skip;
WS: [ \t\n\r]+ -> skip ;