// Generated from /Users/tva/ITMO_old/program_models_verification/Paradise.g4 by ANTLR 4.7.

package paradise // Paradise
import "github.com/antlr/antlr4/runtime/Go/antlr"

type BaseParadiseVisitor struct {
	*antlr.BaseParseTreeVisitor
}

func (v *BaseParadiseVisitor) VisitSource(ctx *SourceContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseParadiseVisitor) VisitSourceItem(ctx *SourceItemContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseParadiseVisitor) VisitFunctionBody(ctx *FunctionBodyContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseParadiseVisitor) VisitFunctionSignature(ctx *FunctionSignatureContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseParadiseVisitor) VisitFunctionArgs(ctx *FunctionArgsContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseParadiseVisitor) VisitTypeRef(ctx *TypeRefContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseParadiseVisitor) VisitIfThenStmnt(ctx *IfThenStmntContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseParadiseVisitor) VisitIfThenElseStmnt(ctx *IfThenElseStmntContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseParadiseVisitor) VisitWhileStmnt(ctx *WhileStmntContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseParadiseVisitor) VisitDoWhileStmnt(ctx *DoWhileStmntContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseParadiseVisitor) VisitBreakStmnt(ctx *BreakStmntContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseParadiseVisitor) VisitIdentifierStmnt(ctx *IdentifierStmntContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseParadiseVisitor) VisitExprStmnt(ctx *ExprStmntContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseParadiseVisitor) VisitReturnStmnt(ctx *ReturnStmntContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseParadiseVisitor) VisitNestedFunctionDeclaration(ctx *NestedFunctionDeclarationContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseParadiseVisitor) VisitWhileCondition(ctx *WhileConditionContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseParadiseVisitor) VisitWhileBody(ctx *WhileBodyContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseParadiseVisitor) VisitDoWhileBody(ctx *DoWhileBodyContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseParadiseVisitor) VisitDoWhileCondition(ctx *DoWhileConditionContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseParadiseVisitor) VisitIfCondition(ctx *IfConditionContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseParadiseVisitor) VisitIfTrueStatement(ctx *IfTrueStatementContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseParadiseVisitor) VisitIfFalseStatement(ctx *IfFalseStatementContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseParadiseVisitor) VisitPrefixUnaryExpr(ctx *PrefixUnaryExprContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseParadiseVisitor) VisitValueExpr(ctx *ValueExprContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseParadiseVisitor) VisitVariable(ctx *VariableContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseParadiseVisitor) VisitBinaryExpr(ctx *BinaryExprContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseParadiseVisitor) VisitBracedExpr(ctx *BracedExprContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseParadiseVisitor) VisitPostfixUnaryExpr(ctx *PostfixUnaryExprContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseParadiseVisitor) VisitFunctionCall(ctx *FunctionCallContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseParadiseVisitor) VisitCondition(ctx *ConditionContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseParadiseVisitor) VisitArgDef(ctx *ArgDefContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseParadiseVisitor) VisitArgDefList(ctx *ArgDefListContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseParadiseVisitor) VisitExprList(ctx *ExprListContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseParadiseVisitor) VisitIdentifier(ctx *IdentifierContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseParadiseVisitor) VisitSumBinary(ctx *SumBinaryContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseParadiseVisitor) VisitSubBinary(ctx *SubBinaryContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseParadiseVisitor) VisitDivBinary(ctx *DivBinaryContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseParadiseVisitor) VisitMulBinary(ctx *MulBinaryContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseParadiseVisitor) VisitByteAndBinary(ctx *ByteAndBinaryContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseParadiseVisitor) VisitAndBinary(ctx *AndBinaryContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseParadiseVisitor) VisitByteOrBinary(ctx *ByteOrBinaryContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseParadiseVisitor) VisitOrBinary(ctx *OrBinaryContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseParadiseVisitor) VisitEqBinary(ctx *EqBinaryContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseParadiseVisitor) VisitLtBinary(ctx *LtBinaryContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseParadiseVisitor) VisitLtOrEtBinary(ctx *LtOrEtBinaryContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseParadiseVisitor) VisitGtBinary(ctx *GtBinaryContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseParadiseVisitor) VisitModBinary(ctx *ModBinaryContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseParadiseVisitor) VisitSumUnary(ctx *SumUnaryContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseParadiseVisitor) VisitSubUnary(ctx *SubUnaryContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseParadiseVisitor) VisitNotUnary(ctx *NotUnaryContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseParadiseVisitor) VisitDecrementUnary(ctx *DecrementUnaryContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseParadiseVisitor) VisitIncrementUnary(ctx *IncrementUnaryContext) interface{} {
	return v.VisitChildren(ctx)
}
