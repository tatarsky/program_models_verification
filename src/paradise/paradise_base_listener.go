// Generated from /Users/tva/ITMO_old/program_models_verification/Paradise.g4 by ANTLR 4.7.

package paradise // Paradise
import "github.com/antlr/antlr4/runtime/Go/antlr"

// BaseParadiseListener is a complete listener for a parse tree produced by ParadiseParser.
type BaseParadiseListener struct{}

var _ ParadiseListener = &BaseParadiseListener{}

// VisitTerminal is called when a terminal node is visited.
func (s *BaseParadiseListener) VisitTerminal(node antlr.TerminalNode) {}

// VisitErrorNode is called when an error node is visited.
func (s *BaseParadiseListener) VisitErrorNode(node antlr.ErrorNode) {}

// EnterEveryRule is called when any rule is entered.
func (s *BaseParadiseListener) EnterEveryRule(ctx antlr.ParserRuleContext) {}

// ExitEveryRule is called when any rule is exited.
func (s *BaseParadiseListener) ExitEveryRule(ctx antlr.ParserRuleContext) {}

// EnterSource is called when production source is entered.
func (s *BaseParadiseListener) EnterSource(ctx *SourceContext) {}

// ExitSource is called when production source is exited.
func (s *BaseParadiseListener) ExitSource(ctx *SourceContext) {}

// EnterSourceItem is called when production sourceItem is entered.
func (s *BaseParadiseListener) EnterSourceItem(ctx *SourceItemContext) {}

// ExitSourceItem is called when production sourceItem is exited.
func (s *BaseParadiseListener) ExitSourceItem(ctx *SourceItemContext) {}

// EnterFunctionBody is called when production functionBody is entered.
func (s *BaseParadiseListener) EnterFunctionBody(ctx *FunctionBodyContext) {}

// ExitFunctionBody is called when production functionBody is exited.
func (s *BaseParadiseListener) ExitFunctionBody(ctx *FunctionBodyContext) {}

// EnterFunctionSignature is called when production functionSignature is entered.
func (s *BaseParadiseListener) EnterFunctionSignature(ctx *FunctionSignatureContext) {}

// ExitFunctionSignature is called when production functionSignature is exited.
func (s *BaseParadiseListener) ExitFunctionSignature(ctx *FunctionSignatureContext) {}

// EnterFunctionArgs is called when production functionArgs is entered.
func (s *BaseParadiseListener) EnterFunctionArgs(ctx *FunctionArgsContext) {}

// ExitFunctionArgs is called when production functionArgs is exited.
func (s *BaseParadiseListener) ExitFunctionArgs(ctx *FunctionArgsContext) {}

// EnterTypeRef is called when production typeRef is entered.
func (s *BaseParadiseListener) EnterTypeRef(ctx *TypeRefContext) {}

// ExitTypeRef is called when production typeRef is exited.
func (s *BaseParadiseListener) ExitTypeRef(ctx *TypeRefContext) {}

// EnterIfThenStmnt is called when production IfThenStmnt is entered.
func (s *BaseParadiseListener) EnterIfThenStmnt(ctx *IfThenStmntContext) {}

// ExitIfThenStmnt is called when production IfThenStmnt is exited.
func (s *BaseParadiseListener) ExitIfThenStmnt(ctx *IfThenStmntContext) {}

// EnterIfThenElseStmnt is called when production IfThenElseStmnt is entered.
func (s *BaseParadiseListener) EnterIfThenElseStmnt(ctx *IfThenElseStmntContext) {}

// ExitIfThenElseStmnt is called when production IfThenElseStmnt is exited.
func (s *BaseParadiseListener) ExitIfThenElseStmnt(ctx *IfThenElseStmntContext) {}

// EnterWhileStmnt is called when production WhileStmnt is entered.
func (s *BaseParadiseListener) EnterWhileStmnt(ctx *WhileStmntContext) {}

// ExitWhileStmnt is called when production WhileStmnt is exited.
func (s *BaseParadiseListener) ExitWhileStmnt(ctx *WhileStmntContext) {}

// EnterDoWhileStmnt is called when production DoWhileStmnt is entered.
func (s *BaseParadiseListener) EnterDoWhileStmnt(ctx *DoWhileStmntContext) {}

// ExitDoWhileStmnt is called when production DoWhileStmnt is exited.
func (s *BaseParadiseListener) ExitDoWhileStmnt(ctx *DoWhileStmntContext) {}

// EnterBreakStmnt is called when production BreakStmnt is entered.
func (s *BaseParadiseListener) EnterBreakStmnt(ctx *BreakStmntContext) {}

// ExitBreakStmnt is called when production BreakStmnt is exited.
func (s *BaseParadiseListener) ExitBreakStmnt(ctx *BreakStmntContext) {}

// EnterIdentifierStmnt is called when production IdentifierStmnt is entered.
func (s *BaseParadiseListener) EnterIdentifierStmnt(ctx *IdentifierStmntContext) {}

// ExitIdentifierStmnt is called when production IdentifierStmnt is exited.
func (s *BaseParadiseListener) ExitIdentifierStmnt(ctx *IdentifierStmntContext) {}

// EnterExprStmnt is called when production ExprStmnt is entered.
func (s *BaseParadiseListener) EnterExprStmnt(ctx *ExprStmntContext) {}

// ExitExprStmnt is called when production ExprStmnt is exited.
func (s *BaseParadiseListener) ExitExprStmnt(ctx *ExprStmntContext) {}

// EnterReturnStmnt is called when production ReturnStmnt is entered.
func (s *BaseParadiseListener) EnterReturnStmnt(ctx *ReturnStmntContext) {}

// ExitReturnStmnt is called when production ReturnStmnt is exited.
func (s *BaseParadiseListener) ExitReturnStmnt(ctx *ReturnStmntContext) {}

// EnterNestedFunctionDeclaration is called when production NestedFunctionDeclaration is entered.
func (s *BaseParadiseListener) EnterNestedFunctionDeclaration(ctx *NestedFunctionDeclarationContext) {}

// ExitNestedFunctionDeclaration is called when production NestedFunctionDeclaration is exited.
func (s *BaseParadiseListener) ExitNestedFunctionDeclaration(ctx *NestedFunctionDeclarationContext) {}

// EnterWhileCondition is called when production whileCondition is entered.
func (s *BaseParadiseListener) EnterWhileCondition(ctx *WhileConditionContext) {}

// ExitWhileCondition is called when production whileCondition is exited.
func (s *BaseParadiseListener) ExitWhileCondition(ctx *WhileConditionContext) {}

// EnterWhileBody is called when production whileBody is entered.
func (s *BaseParadiseListener) EnterWhileBody(ctx *WhileBodyContext) {}

// ExitWhileBody is called when production whileBody is exited.
func (s *BaseParadiseListener) ExitWhileBody(ctx *WhileBodyContext) {}

// EnterDoWhileBody is called when production doWhileBody is entered.
func (s *BaseParadiseListener) EnterDoWhileBody(ctx *DoWhileBodyContext) {}

// ExitDoWhileBody is called when production doWhileBody is exited.
func (s *BaseParadiseListener) ExitDoWhileBody(ctx *DoWhileBodyContext) {}

// EnterDoWhileCondition is called when production doWhileCondition is entered.
func (s *BaseParadiseListener) EnterDoWhileCondition(ctx *DoWhileConditionContext) {}

// ExitDoWhileCondition is called when production doWhileCondition is exited.
func (s *BaseParadiseListener) ExitDoWhileCondition(ctx *DoWhileConditionContext) {}

// EnterIfCondition is called when production ifCondition is entered.
func (s *BaseParadiseListener) EnterIfCondition(ctx *IfConditionContext) {}

// ExitIfCondition is called when production ifCondition is exited.
func (s *BaseParadiseListener) ExitIfCondition(ctx *IfConditionContext) {}

// EnterIfTrueStatement is called when production ifTrueStatement is entered.
func (s *BaseParadiseListener) EnterIfTrueStatement(ctx *IfTrueStatementContext) {}

// ExitIfTrueStatement is called when production ifTrueStatement is exited.
func (s *BaseParadiseListener) ExitIfTrueStatement(ctx *IfTrueStatementContext) {}

// EnterIfFalseStatement is called when production ifFalseStatement is entered.
func (s *BaseParadiseListener) EnterIfFalseStatement(ctx *IfFalseStatementContext) {}

// ExitIfFalseStatement is called when production ifFalseStatement is exited.
func (s *BaseParadiseListener) ExitIfFalseStatement(ctx *IfFalseStatementContext) {}

// EnterPrefixUnaryExpr is called when production PrefixUnaryExpr is entered.
func (s *BaseParadiseListener) EnterPrefixUnaryExpr(ctx *PrefixUnaryExprContext) {}

// ExitPrefixUnaryExpr is called when production PrefixUnaryExpr is exited.
func (s *BaseParadiseListener) ExitPrefixUnaryExpr(ctx *PrefixUnaryExprContext) {}

// EnterValueExpr is called when production ValueExpr is entered.
func (s *BaseParadiseListener) EnterValueExpr(ctx *ValueExprContext) {}

// ExitValueExpr is called when production ValueExpr is exited.
func (s *BaseParadiseListener) ExitValueExpr(ctx *ValueExprContext) {}

// EnterVariable is called when production Variable is entered.
func (s *BaseParadiseListener) EnterVariable(ctx *VariableContext) {}

// ExitVariable is called when production Variable is exited.
func (s *BaseParadiseListener) ExitVariable(ctx *VariableContext) {}

// EnterBinaryExpr is called when production BinaryExpr is entered.
func (s *BaseParadiseListener) EnterBinaryExpr(ctx *BinaryExprContext) {}

// ExitBinaryExpr is called when production BinaryExpr is exited.
func (s *BaseParadiseListener) ExitBinaryExpr(ctx *BinaryExprContext) {}

// EnterBracedExpr is called when production BracedExpr is entered.
func (s *BaseParadiseListener) EnterBracedExpr(ctx *BracedExprContext) {}

// ExitBracedExpr is called when production BracedExpr is exited.
func (s *BaseParadiseListener) ExitBracedExpr(ctx *BracedExprContext) {}

// EnterPostfixUnaryExpr is called when production PostfixUnaryExpr is entered.
func (s *BaseParadiseListener) EnterPostfixUnaryExpr(ctx *PostfixUnaryExprContext) {}

// ExitPostfixUnaryExpr is called when production PostfixUnaryExpr is exited.
func (s *BaseParadiseListener) ExitPostfixUnaryExpr(ctx *PostfixUnaryExprContext) {}

// EnterFunctionCall is called when production FunctionCall is entered.
func (s *BaseParadiseListener) EnterFunctionCall(ctx *FunctionCallContext) {}

// ExitFunctionCall is called when production FunctionCall is exited.
func (s *BaseParadiseListener) ExitFunctionCall(ctx *FunctionCallContext) {}

// EnterCondition is called when production condition is entered.
func (s *BaseParadiseListener) EnterCondition(ctx *ConditionContext) {}

// ExitCondition is called when production condition is exited.
func (s *BaseParadiseListener) ExitCondition(ctx *ConditionContext) {}

// EnterArgDef is called when production argDef is entered.
func (s *BaseParadiseListener) EnterArgDef(ctx *ArgDefContext) {}

// ExitArgDef is called when production argDef is exited.
func (s *BaseParadiseListener) ExitArgDef(ctx *ArgDefContext) {}

// EnterArgDefList is called when production argDefList is entered.
func (s *BaseParadiseListener) EnterArgDefList(ctx *ArgDefListContext) {}

// ExitArgDefList is called when production argDefList is exited.
func (s *BaseParadiseListener) ExitArgDefList(ctx *ArgDefListContext) {}

// EnterExprList is called when production exprList is entered.
func (s *BaseParadiseListener) EnterExprList(ctx *ExprListContext) {}

// ExitExprList is called when production exprList is exited.
func (s *BaseParadiseListener) ExitExprList(ctx *ExprListContext) {}

// EnterIdentifier is called when production identifier is entered.
func (s *BaseParadiseListener) EnterIdentifier(ctx *IdentifierContext) {}

// ExitIdentifier is called when production identifier is exited.
func (s *BaseParadiseListener) ExitIdentifier(ctx *IdentifierContext) {}

// EnterSumBinary is called when production SumBinary is entered.
func (s *BaseParadiseListener) EnterSumBinary(ctx *SumBinaryContext) {}

// ExitSumBinary is called when production SumBinary is exited.
func (s *BaseParadiseListener) ExitSumBinary(ctx *SumBinaryContext) {}

// EnterSubBinary is called when production SubBinary is entered.
func (s *BaseParadiseListener) EnterSubBinary(ctx *SubBinaryContext) {}

// ExitSubBinary is called when production SubBinary is exited.
func (s *BaseParadiseListener) ExitSubBinary(ctx *SubBinaryContext) {}

// EnterDivBinary is called when production DivBinary is entered.
func (s *BaseParadiseListener) EnterDivBinary(ctx *DivBinaryContext) {}

// ExitDivBinary is called when production DivBinary is exited.
func (s *BaseParadiseListener) ExitDivBinary(ctx *DivBinaryContext) {}

// EnterMulBinary is called when production MulBinary is entered.
func (s *BaseParadiseListener) EnterMulBinary(ctx *MulBinaryContext) {}

// ExitMulBinary is called when production MulBinary is exited.
func (s *BaseParadiseListener) ExitMulBinary(ctx *MulBinaryContext) {}

// EnterByteAndBinary is called when production ByteAndBinary is entered.
func (s *BaseParadiseListener) EnterByteAndBinary(ctx *ByteAndBinaryContext) {}

// ExitByteAndBinary is called when production ByteAndBinary is exited.
func (s *BaseParadiseListener) ExitByteAndBinary(ctx *ByteAndBinaryContext) {}

// EnterAndBinary is called when production AndBinary is entered.
func (s *BaseParadiseListener) EnterAndBinary(ctx *AndBinaryContext) {}

// ExitAndBinary is called when production AndBinary is exited.
func (s *BaseParadiseListener) ExitAndBinary(ctx *AndBinaryContext) {}

// EnterByteOrBinary is called when production ByteOrBinary is entered.
func (s *BaseParadiseListener) EnterByteOrBinary(ctx *ByteOrBinaryContext) {}

// ExitByteOrBinary is called when production ByteOrBinary is exited.
func (s *BaseParadiseListener) ExitByteOrBinary(ctx *ByteOrBinaryContext) {}

// EnterOrBinary is called when production OrBinary is entered.
func (s *BaseParadiseListener) EnterOrBinary(ctx *OrBinaryContext) {}

// ExitOrBinary is called when production OrBinary is exited.
func (s *BaseParadiseListener) ExitOrBinary(ctx *OrBinaryContext) {}

// EnterEqBinary is called when production EqBinary is entered.
func (s *BaseParadiseListener) EnterEqBinary(ctx *EqBinaryContext) {}

// ExitEqBinary is called when production EqBinary is exited.
func (s *BaseParadiseListener) ExitEqBinary(ctx *EqBinaryContext) {}

// EnterLtBinary is called when production LtBinary is entered.
func (s *BaseParadiseListener) EnterLtBinary(ctx *LtBinaryContext) {}

// ExitLtBinary is called when production LtBinary is exited.
func (s *BaseParadiseListener) ExitLtBinary(ctx *LtBinaryContext) {}

// EnterLtOrEtBinary is called when production LtOrEtBinary is entered.
func (s *BaseParadiseListener) EnterLtOrEtBinary(ctx *LtOrEtBinaryContext) {}

// ExitLtOrEtBinary is called when production LtOrEtBinary is exited.
func (s *BaseParadiseListener) ExitLtOrEtBinary(ctx *LtOrEtBinaryContext) {}

// EnterGtBinary is called when production GtBinary is entered.
func (s *BaseParadiseListener) EnterGtBinary(ctx *GtBinaryContext) {}

// ExitGtBinary is called when production GtBinary is exited.
func (s *BaseParadiseListener) ExitGtBinary(ctx *GtBinaryContext) {}

// EnterModBinary is called when production ModBinary is entered.
func (s *BaseParadiseListener) EnterModBinary(ctx *ModBinaryContext) {}

// ExitModBinary is called when production ModBinary is exited.
func (s *BaseParadiseListener) ExitModBinary(ctx *ModBinaryContext) {}

// EnterSumUnary is called when production SumUnary is entered.
func (s *BaseParadiseListener) EnterSumUnary(ctx *SumUnaryContext) {}

// ExitSumUnary is called when production SumUnary is exited.
func (s *BaseParadiseListener) ExitSumUnary(ctx *SumUnaryContext) {}

// EnterSubUnary is called when production SubUnary is entered.
func (s *BaseParadiseListener) EnterSubUnary(ctx *SubUnaryContext) {}

// ExitSubUnary is called when production SubUnary is exited.
func (s *BaseParadiseListener) ExitSubUnary(ctx *SubUnaryContext) {}

// EnterNotUnary is called when production NotUnary is entered.
func (s *BaseParadiseListener) EnterNotUnary(ctx *NotUnaryContext) {}

// ExitNotUnary is called when production NotUnary is exited.
func (s *BaseParadiseListener) ExitNotUnary(ctx *NotUnaryContext) {}

// EnterDecrementUnary is called when production DecrementUnary is entered.
func (s *BaseParadiseListener) EnterDecrementUnary(ctx *DecrementUnaryContext) {}

// ExitDecrementUnary is called when production DecrementUnary is exited.
func (s *BaseParadiseListener) ExitDecrementUnary(ctx *DecrementUnaryContext) {}

// EnterIncrementUnary is called when production IncrementUnary is entered.
func (s *BaseParadiseListener) EnterIncrementUnary(ctx *IncrementUnaryContext) {}

// ExitIncrementUnary is called when production IncrementUnary is exited.
func (s *BaseParadiseListener) ExitIncrementUnary(ctx *IncrementUnaryContext) {}
