// Generated from /Users/tva/ITMO_old/program_models_verification/Paradise.g4 by ANTLR 4.7.

package paradise // Paradise
import (
	"fmt"
	"reflect"
	"strconv"

	"github.com/antlr/antlr4/runtime/Go/antlr"
)

// Suppress unused import errors
var _ = fmt.Printf
var _ = reflect.Copy
var _ = strconv.Itoa

var parserATN = []uint16{
	3, 24715, 42794, 33075, 47597, 16764, 15335, 30598, 22884, 3, 55, 236,
	4, 2, 9, 2, 4, 3, 9, 3, 4, 4, 9, 4, 4, 5, 9, 5, 4, 6, 9, 6, 4, 7, 9, 7,
	4, 8, 9, 8, 4, 9, 9, 9, 4, 10, 9, 10, 4, 11, 9, 11, 4, 12, 9, 12, 4, 13,
	9, 13, 4, 14, 9, 14, 4, 15, 9, 15, 4, 16, 9, 16, 4, 17, 9, 17, 4, 18, 9,
	18, 4, 19, 9, 19, 4, 20, 9, 20, 4, 21, 9, 21, 4, 22, 9, 22, 4, 23, 9, 23,
	3, 2, 7, 2, 48, 10, 2, 12, 2, 14, 2, 51, 11, 2, 3, 3, 3, 3, 3, 3, 3, 3,
	3, 3, 3, 4, 7, 4, 59, 10, 4, 12, 4, 14, 4, 62, 11, 4, 3, 5, 3, 5, 3, 5,
	3, 6, 3, 6, 3, 6, 3, 6, 3, 6, 5, 6, 72, 10, 6, 3, 7, 3, 7, 3, 7, 3, 7,
	3, 7, 3, 7, 3, 7, 3, 7, 3, 7, 5, 7, 83, 10, 7, 3, 8, 3, 8, 3, 8, 3, 8,
	3, 8, 3, 8, 3, 8, 3, 8, 3, 8, 3, 8, 3, 8, 3, 8, 3, 8, 3, 8, 3, 8, 3, 8,
	3, 8, 3, 8, 3, 8, 3, 8, 3, 8, 3, 8, 3, 8, 3, 8, 3, 8, 3, 8, 3, 8, 3, 8,
	3, 8, 3, 8, 3, 8, 3, 8, 3, 8, 3, 8, 3, 8, 3, 8, 3, 8, 3, 8, 3, 8, 3, 8,
	5, 8, 125, 10, 8, 3, 9, 3, 9, 3, 10, 7, 10, 130, 10, 10, 12, 10, 14, 10,
	133, 11, 10, 3, 11, 7, 11, 136, 10, 11, 12, 11, 14, 11, 139, 11, 11, 3,
	12, 3, 12, 3, 13, 3, 13, 3, 14, 7, 14, 146, 10, 14, 12, 14, 14, 14, 149,
	11, 14, 3, 15, 7, 15, 152, 10, 15, 12, 15, 14, 15, 155, 11, 15, 3, 16,
	3, 16, 3, 16, 3, 16, 3, 16, 3, 16, 3, 16, 3, 16, 3, 16, 3, 16, 5, 16, 167,
	10, 16, 3, 16, 3, 16, 3, 16, 3, 16, 3, 16, 3, 16, 3, 16, 3, 16, 3, 16,
	3, 16, 3, 16, 7, 16, 180, 10, 16, 12, 16, 14, 16, 183, 11, 16, 3, 17, 3,
	17, 3, 18, 3, 18, 3, 18, 3, 18, 3, 19, 3, 19, 3, 19, 7, 19, 194, 10, 19,
	12, 19, 14, 19, 197, 11, 19, 5, 19, 199, 10, 19, 3, 20, 3, 20, 3, 20, 7,
	20, 204, 10, 20, 12, 20, 14, 20, 207, 11, 20, 5, 20, 209, 10, 20, 3, 21,
	3, 21, 3, 22, 3, 22, 3, 22, 3, 22, 3, 22, 3, 22, 3, 22, 3, 22, 3, 22, 3,
	22, 3, 22, 3, 22, 3, 22, 3, 22, 5, 22, 227, 10, 22, 3, 23, 3, 23, 3, 23,
	3, 23, 3, 23, 5, 23, 234, 10, 23, 3, 23, 2, 3, 30, 24, 2, 4, 6, 8, 10,
	12, 14, 16, 18, 20, 22, 24, 26, 28, 30, 32, 34, 36, 38, 40, 42, 44, 2,
	4, 4, 2, 16, 16, 20, 20, 3, 2, 47, 52, 2, 263, 2, 49, 3, 2, 2, 2, 4, 52,
	3, 2, 2, 2, 6, 60, 3, 2, 2, 2, 8, 63, 3, 2, 2, 2, 10, 66, 3, 2, 2, 2, 12,
	82, 3, 2, 2, 2, 14, 124, 3, 2, 2, 2, 16, 126, 3, 2, 2, 2, 18, 131, 3, 2,
	2, 2, 20, 137, 3, 2, 2, 2, 22, 140, 3, 2, 2, 2, 24, 142, 3, 2, 2, 2, 26,
	147, 3, 2, 2, 2, 28, 153, 3, 2, 2, 2, 30, 166, 3, 2, 2, 2, 32, 184, 3,
	2, 2, 2, 34, 186, 3, 2, 2, 2, 36, 198, 3, 2, 2, 2, 38, 208, 3, 2, 2, 2,
	40, 210, 3, 2, 2, 2, 42, 226, 3, 2, 2, 2, 44, 233, 3, 2, 2, 2, 46, 48,
	5, 4, 3, 2, 47, 46, 3, 2, 2, 2, 48, 51, 3, 2, 2, 2, 49, 47, 3, 2, 2, 2,
	49, 50, 3, 2, 2, 2, 50, 3, 3, 2, 2, 2, 51, 49, 3, 2, 2, 2, 52, 53, 7, 43,
	2, 2, 53, 54, 5, 8, 5, 2, 54, 55, 5, 6, 4, 2, 55, 56, 7, 44, 2, 2, 56,
	5, 3, 2, 2, 2, 57, 59, 5, 14, 8, 2, 58, 57, 3, 2, 2, 2, 59, 62, 3, 2, 2,
	2, 60, 58, 3, 2, 2, 2, 60, 61, 3, 2, 2, 2, 61, 7, 3, 2, 2, 2, 62, 60, 3,
	2, 2, 2, 63, 64, 5, 40, 21, 2, 64, 65, 5, 10, 6, 2, 65, 9, 3, 2, 2, 2,
	66, 67, 7, 45, 2, 2, 67, 68, 5, 36, 19, 2, 68, 71, 7, 46, 2, 2, 69, 70,
	7, 3, 2, 2, 70, 72, 5, 12, 7, 2, 71, 69, 3, 2, 2, 2, 71, 72, 3, 2, 2, 2,
	72, 11, 3, 2, 2, 2, 73, 83, 7, 4, 2, 2, 74, 83, 7, 5, 2, 2, 75, 83, 7,
	6, 2, 2, 76, 83, 7, 7, 2, 2, 77, 83, 7, 8, 2, 2, 78, 83, 7, 9, 2, 2, 79,
	83, 7, 10, 2, 2, 80, 83, 7, 11, 2, 2, 81, 83, 5, 40, 21, 2, 82, 73, 3,
	2, 2, 2, 82, 74, 3, 2, 2, 2, 82, 75, 3, 2, 2, 2, 82, 76, 3, 2, 2, 2, 82,
	77, 3, 2, 2, 2, 82, 78, 3, 2, 2, 2, 82, 79, 3, 2, 2, 2, 82, 80, 3, 2, 2,
	2, 82, 81, 3, 2, 2, 2, 83, 13, 3, 2, 2, 2, 84, 85, 7, 12, 2, 2, 85, 86,
	5, 24, 13, 2, 86, 87, 7, 13, 2, 2, 87, 88, 5, 26, 14, 2, 88, 89, 7, 14,
	2, 2, 89, 125, 3, 2, 2, 2, 90, 91, 7, 12, 2, 2, 91, 92, 5, 24, 13, 2, 92,
	93, 7, 13, 2, 2, 93, 94, 5, 26, 14, 2, 94, 95, 7, 15, 2, 2, 95, 96, 5,
	28, 15, 2, 96, 97, 7, 14, 2, 2, 97, 125, 3, 2, 2, 2, 98, 99, 7, 16, 2,
	2, 99, 100, 5, 16, 9, 2, 100, 101, 5, 18, 10, 2, 101, 102, 7, 17, 2, 2,
	102, 125, 3, 2, 2, 2, 103, 104, 7, 18, 2, 2, 104, 105, 5, 20, 11, 2, 105,
	106, 7, 19, 2, 2, 106, 107, 9, 2, 2, 2, 107, 108, 5, 22, 12, 2, 108, 125,
	3, 2, 2, 2, 109, 110, 7, 21, 2, 2, 110, 125, 7, 22, 2, 2, 111, 112, 5,
	40, 21, 2, 112, 113, 7, 23, 2, 2, 113, 114, 5, 30, 16, 2, 114, 115, 7,
	22, 2, 2, 115, 125, 3, 2, 2, 2, 116, 117, 5, 30, 16, 2, 117, 118, 7, 22,
	2, 2, 118, 125, 3, 2, 2, 2, 119, 120, 7, 24, 2, 2, 120, 121, 5, 30, 16,
	2, 121, 122, 7, 22, 2, 2, 122, 125, 3, 2, 2, 2, 123, 125, 5, 4, 3, 2, 124,
	84, 3, 2, 2, 2, 124, 90, 3, 2, 2, 2, 124, 98, 3, 2, 2, 2, 124, 103, 3,
	2, 2, 2, 124, 109, 3, 2, 2, 2, 124, 111, 3, 2, 2, 2, 124, 116, 3, 2, 2,
	2, 124, 119, 3, 2, 2, 2, 124, 123, 3, 2, 2, 2, 125, 15, 3, 2, 2, 2, 126,
	127, 5, 30, 16, 2, 127, 17, 3, 2, 2, 2, 128, 130, 5, 14, 8, 2, 129, 128,
	3, 2, 2, 2, 130, 133, 3, 2, 2, 2, 131, 129, 3, 2, 2, 2, 131, 132, 3, 2,
	2, 2, 132, 19, 3, 2, 2, 2, 133, 131, 3, 2, 2, 2, 134, 136, 5, 14, 8, 2,
	135, 134, 3, 2, 2, 2, 136, 139, 3, 2, 2, 2, 137, 135, 3, 2, 2, 2, 137,
	138, 3, 2, 2, 2, 138, 21, 3, 2, 2, 2, 139, 137, 3, 2, 2, 2, 140, 141, 5,
	30, 16, 2, 141, 23, 3, 2, 2, 2, 142, 143, 5, 30, 16, 2, 143, 25, 3, 2,
	2, 2, 144, 146, 5, 14, 8, 2, 145, 144, 3, 2, 2, 2, 146, 149, 3, 2, 2, 2,
	147, 145, 3, 2, 2, 2, 147, 148, 3, 2, 2, 2, 148, 27, 3, 2, 2, 2, 149, 147,
	3, 2, 2, 2, 150, 152, 5, 14, 8, 2, 151, 150, 3, 2, 2, 2, 152, 155, 3, 2,
	2, 2, 153, 151, 3, 2, 2, 2, 153, 154, 3, 2, 2, 2, 154, 29, 3, 2, 2, 2,
	155, 153, 3, 2, 2, 2, 156, 157, 8, 16, 1, 2, 157, 167, 9, 3, 2, 2, 158,
	159, 5, 44, 23, 2, 159, 160, 5, 30, 16, 7, 160, 167, 3, 2, 2, 2, 161, 167,
	5, 40, 21, 2, 162, 163, 7, 45, 2, 2, 163, 164, 5, 30, 16, 2, 164, 165,
	7, 46, 2, 2, 165, 167, 3, 2, 2, 2, 166, 156, 3, 2, 2, 2, 166, 158, 3, 2,
	2, 2, 166, 161, 3, 2, 2, 2, 166, 162, 3, 2, 2, 2, 167, 181, 3, 2, 2, 2,
	168, 169, 12, 9, 2, 2, 169, 170, 5, 42, 22, 2, 170, 171, 5, 30, 16, 10,
	171, 180, 3, 2, 2, 2, 172, 173, 12, 6, 2, 2, 173, 180, 5, 44, 23, 2, 174,
	175, 12, 5, 2, 2, 175, 176, 7, 45, 2, 2, 176, 177, 5, 38, 20, 2, 177, 178,
	7, 46, 2, 2, 178, 180, 3, 2, 2, 2, 179, 168, 3, 2, 2, 2, 179, 172, 3, 2,
	2, 2, 179, 174, 3, 2, 2, 2, 180, 183, 3, 2, 2, 2, 181, 179, 3, 2, 2, 2,
	181, 182, 3, 2, 2, 2, 182, 31, 3, 2, 2, 2, 183, 181, 3, 2, 2, 2, 184, 185,
	5, 30, 16, 2, 185, 33, 3, 2, 2, 2, 186, 187, 5, 40, 21, 2, 187, 188, 7,
	3, 2, 2, 188, 189, 5, 12, 7, 2, 189, 35, 3, 2, 2, 2, 190, 195, 5, 34, 18,
	2, 191, 192, 7, 25, 2, 2, 192, 194, 5, 34, 18, 2, 193, 191, 3, 2, 2, 2,
	194, 197, 3, 2, 2, 2, 195, 193, 3, 2, 2, 2, 195, 196, 3, 2, 2, 2, 196,
	199, 3, 2, 2, 2, 197, 195, 3, 2, 2, 2, 198, 190, 3, 2, 2, 2, 198, 199,
	3, 2, 2, 2, 199, 37, 3, 2, 2, 2, 200, 205, 5, 30, 16, 2, 201, 202, 7, 25,
	2, 2, 202, 204, 5, 30, 16, 2, 203, 201, 3, 2, 2, 2, 204, 207, 3, 2, 2,
	2, 205, 203, 3, 2, 2, 2, 205, 206, 3, 2, 2, 2, 206, 209, 3, 2, 2, 2, 207,
	205, 3, 2, 2, 2, 208, 200, 3, 2, 2, 2, 208, 209, 3, 2, 2, 2, 209, 39, 3,
	2, 2, 2, 210, 211, 7, 53, 2, 2, 211, 41, 3, 2, 2, 2, 212, 227, 7, 26, 2,
	2, 213, 227, 7, 27, 2, 2, 214, 227, 7, 28, 2, 2, 215, 227, 7, 29, 2, 2,
	216, 227, 7, 30, 2, 2, 217, 227, 7, 31, 2, 2, 218, 227, 7, 32, 2, 2, 219,
	227, 7, 33, 2, 2, 220, 227, 7, 34, 2, 2, 221, 227, 7, 35, 2, 2, 222, 227,
	7, 36, 2, 2, 223, 227, 7, 37, 2, 2, 224, 227, 7, 38, 2, 2, 225, 227, 7,
	39, 2, 2, 226, 212, 3, 2, 2, 2, 226, 213, 3, 2, 2, 2, 226, 214, 3, 2, 2,
	2, 226, 215, 3, 2, 2, 2, 226, 216, 3, 2, 2, 2, 226, 217, 3, 2, 2, 2, 226,
	218, 3, 2, 2, 2, 226, 219, 3, 2, 2, 2, 226, 220, 3, 2, 2, 2, 226, 221,
	3, 2, 2, 2, 226, 222, 3, 2, 2, 2, 226, 223, 3, 2, 2, 2, 226, 224, 3, 2,
	2, 2, 226, 225, 3, 2, 2, 2, 227, 43, 3, 2, 2, 2, 228, 234, 7, 26, 2, 2,
	229, 234, 7, 27, 2, 2, 230, 234, 7, 40, 2, 2, 231, 234, 7, 41, 2, 2, 232,
	234, 7, 42, 2, 2, 233, 228, 3, 2, 2, 2, 233, 229, 3, 2, 2, 2, 233, 230,
	3, 2, 2, 2, 233, 231, 3, 2, 2, 2, 233, 232, 3, 2, 2, 2, 234, 45, 3, 2,
	2, 2, 20, 49, 60, 71, 82, 124, 131, 137, 147, 153, 166, 179, 181, 195,
	198, 205, 208, 226, 233,
}
var deserializer = antlr.NewATNDeserializer(nil)
var deserializedATN = deserializer.DeserializeFromUInt16(parserATN)

var literalNames = []string{
	"", "'as'", "'bool'", "'byte'", "'int'", "'uint'", "'long'", "'ulong'",
	"'char'", "'string'", "'if'", "'then'", "'end if'", "'else'", "'while'",
	"'wend'", "'do'", "'loop'", "'until'", "'break'", "';'", "'='", "'return'",
	"','", "'+'", "'-'", "'/'", "'*'", "'&'", "'&&'", "'|'", "'||'", "'=='",
	"'<'", "'<='", "'>'", "'>='", "'%'", "'!'", "'--'", "'++'", "'function'",
	"'end function'", "'('", "')'",
}
var symbolicNames = []string{
	"", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "",
	"", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "",
	"", "", "", "", "", "FUNCTION", "END_FUNCTION", "LBRACE", "RBRACE", "CHAR",
	"STRING", "HEX", "BITS", "DEC", "BOOL", "IDENTIFIER", "COMMENT", "WS",
}

var ruleNames = []string{
	"source", "sourceItem", "functionBody", "functionSignature", "functionArgs",
	"typeRef", "statement", "whileCondition", "whileBody", "doWhileBody", "doWhileCondition",
	"ifCondition", "ifTrueStatement", "ifFalseStatement", "expr", "condition",
	"argDef", "argDefList", "exprList", "identifier", "binary", "unary",
}
var decisionToDFA = make([]*antlr.DFA, len(deserializedATN.DecisionToState))

func init() {
	for index, ds := range deserializedATN.DecisionToState {
		decisionToDFA[index] = antlr.NewDFA(ds, index)
	}
}

type ParadiseParser struct {
	*antlr.BaseParser
}

func NewParadiseParser(input antlr.TokenStream) *ParadiseParser {
	this := new(ParadiseParser)

	this.BaseParser = antlr.NewBaseParser(input)

	this.Interpreter = antlr.NewParserATNSimulator(this, deserializedATN, decisionToDFA, antlr.NewPredictionContextCache())
	this.RuleNames = ruleNames
	this.LiteralNames = literalNames
	this.SymbolicNames = symbolicNames
	this.GrammarFileName = "Paradise.g4"

	return this
}

// ParadiseParser tokens.
const (
	ParadiseParserEOF          = antlr.TokenEOF
	ParadiseParserT__0         = 1
	ParadiseParserT__1         = 2
	ParadiseParserT__2         = 3
	ParadiseParserT__3         = 4
	ParadiseParserT__4         = 5
	ParadiseParserT__5         = 6
	ParadiseParserT__6         = 7
	ParadiseParserT__7         = 8
	ParadiseParserT__8         = 9
	ParadiseParserT__9         = 10
	ParadiseParserT__10        = 11
	ParadiseParserT__11        = 12
	ParadiseParserT__12        = 13
	ParadiseParserT__13        = 14
	ParadiseParserT__14        = 15
	ParadiseParserT__15        = 16
	ParadiseParserT__16        = 17
	ParadiseParserT__17        = 18
	ParadiseParserT__18        = 19
	ParadiseParserT__19        = 20
	ParadiseParserT__20        = 21
	ParadiseParserT__21        = 22
	ParadiseParserT__22        = 23
	ParadiseParserT__23        = 24
	ParadiseParserT__24        = 25
	ParadiseParserT__25        = 26
	ParadiseParserT__26        = 27
	ParadiseParserT__27        = 28
	ParadiseParserT__28        = 29
	ParadiseParserT__29        = 30
	ParadiseParserT__30        = 31
	ParadiseParserT__31        = 32
	ParadiseParserT__32        = 33
	ParadiseParserT__33        = 34
	ParadiseParserT__34        = 35
	ParadiseParserT__35        = 36
	ParadiseParserT__36        = 37
	ParadiseParserT__37        = 38
	ParadiseParserT__38        = 39
	ParadiseParserT__39        = 40
	ParadiseParserFUNCTION     = 41
	ParadiseParserEND_FUNCTION = 42
	ParadiseParserLBRACE       = 43
	ParadiseParserRBRACE       = 44
	ParadiseParserCHAR         = 45
	ParadiseParserSTRING       = 46
	ParadiseParserHEX          = 47
	ParadiseParserBITS         = 48
	ParadiseParserDEC          = 49
	ParadiseParserBOOL         = 50
	ParadiseParserIDENTIFIER   = 51
	ParadiseParserCOMMENT      = 52
	ParadiseParserWS           = 53
)

// ParadiseParser rules.
const (
	ParadiseParserRULE_source            = 0
	ParadiseParserRULE_sourceItem        = 1
	ParadiseParserRULE_functionBody      = 2
	ParadiseParserRULE_functionSignature = 3
	ParadiseParserRULE_functionArgs      = 4
	ParadiseParserRULE_typeRef           = 5
	ParadiseParserRULE_statement         = 6
	ParadiseParserRULE_whileCondition    = 7
	ParadiseParserRULE_whileBody         = 8
	ParadiseParserRULE_doWhileBody       = 9
	ParadiseParserRULE_doWhileCondition  = 10
	ParadiseParserRULE_ifCondition       = 11
	ParadiseParserRULE_ifTrueStatement   = 12
	ParadiseParserRULE_ifFalseStatement  = 13
	ParadiseParserRULE_expr              = 14
	ParadiseParserRULE_condition         = 15
	ParadiseParserRULE_argDef            = 16
	ParadiseParserRULE_argDefList        = 17
	ParadiseParserRULE_exprList          = 18
	ParadiseParserRULE_identifier        = 19
	ParadiseParserRULE_binary            = 20
	ParadiseParserRULE_unary             = 21
)

// ISourceContext is an interface to support dynamic dispatch.
type ISourceContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// IsSourceContext differentiates from other interfaces.
	IsSourceContext()
}

type SourceContext struct {
	*antlr.BaseParserRuleContext
	parser antlr.Parser
}

func NewEmptySourceContext() *SourceContext {
	var p = new(SourceContext)
	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(nil, -1)
	p.RuleIndex = ParadiseParserRULE_source
	return p
}

func (*SourceContext) IsSourceContext() {}

func NewSourceContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *SourceContext {
	var p = new(SourceContext)

	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(parent, invokingState)

	p.parser = parser
	p.RuleIndex = ParadiseParserRULE_source

	return p
}

func (s *SourceContext) GetParser() antlr.Parser { return s.parser }

func (s *SourceContext) AllSourceItem() []ISourceItemContext {
	var ts = s.GetTypedRuleContexts(reflect.TypeOf((*ISourceItemContext)(nil)).Elem())
	var tst = make([]ISourceItemContext, len(ts))

	for i, t := range ts {
		if t != nil {
			tst[i] = t.(ISourceItemContext)
		}
	}

	return tst
}

func (s *SourceContext) SourceItem(i int) ISourceItemContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*ISourceItemContext)(nil)).Elem(), i)

	if t == nil {
		return nil
	}

	return t.(ISourceItemContext)
}

func (s *SourceContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *SourceContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *SourceContext) EnterRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(ParadiseListener); ok {
		listenerT.EnterSource(s)
	}
}

func (s *SourceContext) ExitRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(ParadiseListener); ok {
		listenerT.ExitSource(s)
	}
}

func (s *SourceContext) Accept(visitor antlr.ParseTreeVisitor) interface{} {
	switch t := visitor.(type) {
	case ParadiseVisitor:
		return t.VisitSource(s)

	default:
		return t.VisitChildren(s)
	}
}

func (p *ParadiseParser) Source() (localctx ISourceContext) {
	localctx = NewSourceContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 0, ParadiseParserRULE_source)
	var _la int

	defer func() {
		p.ExitRule()
	}()

	defer func() {
		if err := recover(); err != nil {
			if v, ok := err.(antlr.RecognitionException); ok {
				localctx.SetException(v)
				p.GetErrorHandler().ReportError(p, v)
				p.GetErrorHandler().Recover(p, v)
			} else {
				panic(err)
			}
		}
	}()

	p.EnterOuterAlt(localctx, 1)
	p.SetState(47)
	p.GetErrorHandler().Sync(p)
	_la = p.GetTokenStream().LA(1)

	for _la == ParadiseParserFUNCTION {
		{
			p.SetState(44)
			p.SourceItem()
		}

		p.SetState(49)
		p.GetErrorHandler().Sync(p)
		_la = p.GetTokenStream().LA(1)
	}

	return localctx
}

// ISourceItemContext is an interface to support dynamic dispatch.
type ISourceItemContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// IsSourceItemContext differentiates from other interfaces.
	IsSourceItemContext()
}

type SourceItemContext struct {
	*antlr.BaseParserRuleContext
	parser antlr.Parser
}

func NewEmptySourceItemContext() *SourceItemContext {
	var p = new(SourceItemContext)
	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(nil, -1)
	p.RuleIndex = ParadiseParserRULE_sourceItem
	return p
}

func (*SourceItemContext) IsSourceItemContext() {}

func NewSourceItemContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *SourceItemContext {
	var p = new(SourceItemContext)

	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(parent, invokingState)

	p.parser = parser
	p.RuleIndex = ParadiseParserRULE_sourceItem

	return p
}

func (s *SourceItemContext) GetParser() antlr.Parser { return s.parser }

func (s *SourceItemContext) FUNCTION() antlr.TerminalNode {
	return s.GetToken(ParadiseParserFUNCTION, 0)
}

func (s *SourceItemContext) FunctionSignature() IFunctionSignatureContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*IFunctionSignatureContext)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(IFunctionSignatureContext)
}

func (s *SourceItemContext) FunctionBody() IFunctionBodyContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*IFunctionBodyContext)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(IFunctionBodyContext)
}

func (s *SourceItemContext) END_FUNCTION() antlr.TerminalNode {
	return s.GetToken(ParadiseParserEND_FUNCTION, 0)
}

func (s *SourceItemContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *SourceItemContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *SourceItemContext) EnterRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(ParadiseListener); ok {
		listenerT.EnterSourceItem(s)
	}
}

func (s *SourceItemContext) ExitRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(ParadiseListener); ok {
		listenerT.ExitSourceItem(s)
	}
}

func (s *SourceItemContext) Accept(visitor antlr.ParseTreeVisitor) interface{} {
	switch t := visitor.(type) {
	case ParadiseVisitor:
		return t.VisitSourceItem(s)

	default:
		return t.VisitChildren(s)
	}
}

func (p *ParadiseParser) SourceItem() (localctx ISourceItemContext) {
	localctx = NewSourceItemContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 2, ParadiseParserRULE_sourceItem)

	defer func() {
		p.ExitRule()
	}()

	defer func() {
		if err := recover(); err != nil {
			if v, ok := err.(antlr.RecognitionException); ok {
				localctx.SetException(v)
				p.GetErrorHandler().ReportError(p, v)
				p.GetErrorHandler().Recover(p, v)
			} else {
				panic(err)
			}
		}
	}()

	p.EnterOuterAlt(localctx, 1)
	{
		p.SetState(50)
		p.Match(ParadiseParserFUNCTION)
	}
	{
		p.SetState(51)
		p.FunctionSignature()
	}
	{
		p.SetState(52)
		p.FunctionBody()
	}
	{
		p.SetState(53)
		p.Match(ParadiseParserEND_FUNCTION)
	}

	return localctx
}

// IFunctionBodyContext is an interface to support dynamic dispatch.
type IFunctionBodyContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// IsFunctionBodyContext differentiates from other interfaces.
	IsFunctionBodyContext()
}

type FunctionBodyContext struct {
	*antlr.BaseParserRuleContext
	parser antlr.Parser
}

func NewEmptyFunctionBodyContext() *FunctionBodyContext {
	var p = new(FunctionBodyContext)
	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(nil, -1)
	p.RuleIndex = ParadiseParserRULE_functionBody
	return p
}

func (*FunctionBodyContext) IsFunctionBodyContext() {}

func NewFunctionBodyContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *FunctionBodyContext {
	var p = new(FunctionBodyContext)

	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(parent, invokingState)

	p.parser = parser
	p.RuleIndex = ParadiseParserRULE_functionBody

	return p
}

func (s *FunctionBodyContext) GetParser() antlr.Parser { return s.parser }

func (s *FunctionBodyContext) AllStatement() []IStatementContext {
	var ts = s.GetTypedRuleContexts(reflect.TypeOf((*IStatementContext)(nil)).Elem())
	var tst = make([]IStatementContext, len(ts))

	for i, t := range ts {
		if t != nil {
			tst[i] = t.(IStatementContext)
		}
	}

	return tst
}

func (s *FunctionBodyContext) Statement(i int) IStatementContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*IStatementContext)(nil)).Elem(), i)

	if t == nil {
		return nil
	}

	return t.(IStatementContext)
}

func (s *FunctionBodyContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *FunctionBodyContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *FunctionBodyContext) EnterRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(ParadiseListener); ok {
		listenerT.EnterFunctionBody(s)
	}
}

func (s *FunctionBodyContext) ExitRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(ParadiseListener); ok {
		listenerT.ExitFunctionBody(s)
	}
}

func (s *FunctionBodyContext) Accept(visitor antlr.ParseTreeVisitor) interface{} {
	switch t := visitor.(type) {
	case ParadiseVisitor:
		return t.VisitFunctionBody(s)

	default:
		return t.VisitChildren(s)
	}
}

func (p *ParadiseParser) FunctionBody() (localctx IFunctionBodyContext) {
	localctx = NewFunctionBodyContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 4, ParadiseParserRULE_functionBody)
	var _la int

	defer func() {
		p.ExitRule()
	}()

	defer func() {
		if err := recover(); err != nil {
			if v, ok := err.(antlr.RecognitionException); ok {
				localctx.SetException(v)
				p.GetErrorHandler().ReportError(p, v)
				p.GetErrorHandler().Recover(p, v)
			} else {
				panic(err)
			}
		}
	}()

	p.EnterOuterAlt(localctx, 1)
	p.SetState(58)
	p.GetErrorHandler().Sync(p)
	_la = p.GetTokenStream().LA(1)

	for (((_la)&-(0x1f+1)) == 0 && ((1<<uint(_la))&((1<<ParadiseParserT__9)|(1<<ParadiseParserT__13)|(1<<ParadiseParserT__15)|(1<<ParadiseParserT__18)|(1<<ParadiseParserT__21)|(1<<ParadiseParserT__23)|(1<<ParadiseParserT__24))) != 0) || (((_la-38)&-(0x1f+1)) == 0 && ((1<<uint((_la-38)))&((1<<(ParadiseParserT__37-38))|(1<<(ParadiseParserT__38-38))|(1<<(ParadiseParserT__39-38))|(1<<(ParadiseParserFUNCTION-38))|(1<<(ParadiseParserLBRACE-38))|(1<<(ParadiseParserCHAR-38))|(1<<(ParadiseParserSTRING-38))|(1<<(ParadiseParserHEX-38))|(1<<(ParadiseParserBITS-38))|(1<<(ParadiseParserDEC-38))|(1<<(ParadiseParserBOOL-38))|(1<<(ParadiseParserIDENTIFIER-38)))) != 0) {
		{
			p.SetState(55)
			p.Statement()
		}

		p.SetState(60)
		p.GetErrorHandler().Sync(p)
		_la = p.GetTokenStream().LA(1)
	}

	return localctx
}

// IFunctionSignatureContext is an interface to support dynamic dispatch.
type IFunctionSignatureContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// IsFunctionSignatureContext differentiates from other interfaces.
	IsFunctionSignatureContext()
}

type FunctionSignatureContext struct {
	*antlr.BaseParserRuleContext
	parser antlr.Parser
}

func NewEmptyFunctionSignatureContext() *FunctionSignatureContext {
	var p = new(FunctionSignatureContext)
	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(nil, -1)
	p.RuleIndex = ParadiseParserRULE_functionSignature
	return p
}

func (*FunctionSignatureContext) IsFunctionSignatureContext() {}

func NewFunctionSignatureContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *FunctionSignatureContext {
	var p = new(FunctionSignatureContext)

	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(parent, invokingState)

	p.parser = parser
	p.RuleIndex = ParadiseParserRULE_functionSignature

	return p
}

func (s *FunctionSignatureContext) GetParser() antlr.Parser { return s.parser }

func (s *FunctionSignatureContext) Identifier() IIdentifierContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*IIdentifierContext)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(IIdentifierContext)
}

func (s *FunctionSignatureContext) FunctionArgs() IFunctionArgsContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*IFunctionArgsContext)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(IFunctionArgsContext)
}

func (s *FunctionSignatureContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *FunctionSignatureContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *FunctionSignatureContext) EnterRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(ParadiseListener); ok {
		listenerT.EnterFunctionSignature(s)
	}
}

func (s *FunctionSignatureContext) ExitRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(ParadiseListener); ok {
		listenerT.ExitFunctionSignature(s)
	}
}

func (s *FunctionSignatureContext) Accept(visitor antlr.ParseTreeVisitor) interface{} {
	switch t := visitor.(type) {
	case ParadiseVisitor:
		return t.VisitFunctionSignature(s)

	default:
		return t.VisitChildren(s)
	}
}

func (p *ParadiseParser) FunctionSignature() (localctx IFunctionSignatureContext) {
	localctx = NewFunctionSignatureContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 6, ParadiseParserRULE_functionSignature)

	defer func() {
		p.ExitRule()
	}()

	defer func() {
		if err := recover(); err != nil {
			if v, ok := err.(antlr.RecognitionException); ok {
				localctx.SetException(v)
				p.GetErrorHandler().ReportError(p, v)
				p.GetErrorHandler().Recover(p, v)
			} else {
				panic(err)
			}
		}
	}()

	p.EnterOuterAlt(localctx, 1)
	{
		p.SetState(61)
		p.Identifier()
	}
	{
		p.SetState(62)
		p.FunctionArgs()
	}

	return localctx
}

// IFunctionArgsContext is an interface to support dynamic dispatch.
type IFunctionArgsContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// IsFunctionArgsContext differentiates from other interfaces.
	IsFunctionArgsContext()
}

type FunctionArgsContext struct {
	*antlr.BaseParserRuleContext
	parser antlr.Parser
}

func NewEmptyFunctionArgsContext() *FunctionArgsContext {
	var p = new(FunctionArgsContext)
	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(nil, -1)
	p.RuleIndex = ParadiseParserRULE_functionArgs
	return p
}

func (*FunctionArgsContext) IsFunctionArgsContext() {}

func NewFunctionArgsContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *FunctionArgsContext {
	var p = new(FunctionArgsContext)

	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(parent, invokingState)

	p.parser = parser
	p.RuleIndex = ParadiseParserRULE_functionArgs

	return p
}

func (s *FunctionArgsContext) GetParser() antlr.Parser { return s.parser }

func (s *FunctionArgsContext) LBRACE() antlr.TerminalNode {
	return s.GetToken(ParadiseParserLBRACE, 0)
}

func (s *FunctionArgsContext) ArgDefList() IArgDefListContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*IArgDefListContext)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(IArgDefListContext)
}

func (s *FunctionArgsContext) RBRACE() antlr.TerminalNode {
	return s.GetToken(ParadiseParserRBRACE, 0)
}

func (s *FunctionArgsContext) TypeRef() ITypeRefContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*ITypeRefContext)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(ITypeRefContext)
}

func (s *FunctionArgsContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *FunctionArgsContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *FunctionArgsContext) EnterRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(ParadiseListener); ok {
		listenerT.EnterFunctionArgs(s)
	}
}

func (s *FunctionArgsContext) ExitRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(ParadiseListener); ok {
		listenerT.ExitFunctionArgs(s)
	}
}

func (s *FunctionArgsContext) Accept(visitor antlr.ParseTreeVisitor) interface{} {
	switch t := visitor.(type) {
	case ParadiseVisitor:
		return t.VisitFunctionArgs(s)

	default:
		return t.VisitChildren(s)
	}
}

func (p *ParadiseParser) FunctionArgs() (localctx IFunctionArgsContext) {
	localctx = NewFunctionArgsContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 8, ParadiseParserRULE_functionArgs)
	var _la int

	defer func() {
		p.ExitRule()
	}()

	defer func() {
		if err := recover(); err != nil {
			if v, ok := err.(antlr.RecognitionException); ok {
				localctx.SetException(v)
				p.GetErrorHandler().ReportError(p, v)
				p.GetErrorHandler().Recover(p, v)
			} else {
				panic(err)
			}
		}
	}()

	p.EnterOuterAlt(localctx, 1)
	{
		p.SetState(64)
		p.Match(ParadiseParserLBRACE)
	}
	{
		p.SetState(65)
		p.ArgDefList()
	}
	{
		p.SetState(66)
		p.Match(ParadiseParserRBRACE)
	}
	p.SetState(69)
	p.GetErrorHandler().Sync(p)
	_la = p.GetTokenStream().LA(1)

	if _la == ParadiseParserT__0 {
		{
			p.SetState(67)
			p.Match(ParadiseParserT__0)
		}
		{
			p.SetState(68)
			p.TypeRef()
		}

	}

	return localctx
}

// ITypeRefContext is an interface to support dynamic dispatch.
type ITypeRefContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// IsTypeRefContext differentiates from other interfaces.
	IsTypeRefContext()
}

type TypeRefContext struct {
	*antlr.BaseParserRuleContext
	parser antlr.Parser
}

func NewEmptyTypeRefContext() *TypeRefContext {
	var p = new(TypeRefContext)
	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(nil, -1)
	p.RuleIndex = ParadiseParserRULE_typeRef
	return p
}

func (*TypeRefContext) IsTypeRefContext() {}

func NewTypeRefContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *TypeRefContext {
	var p = new(TypeRefContext)

	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(parent, invokingState)

	p.parser = parser
	p.RuleIndex = ParadiseParserRULE_typeRef

	return p
}

func (s *TypeRefContext) GetParser() antlr.Parser { return s.parser }

func (s *TypeRefContext) Identifier() IIdentifierContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*IIdentifierContext)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(IIdentifierContext)
}

func (s *TypeRefContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *TypeRefContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *TypeRefContext) EnterRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(ParadiseListener); ok {
		listenerT.EnterTypeRef(s)
	}
}

func (s *TypeRefContext) ExitRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(ParadiseListener); ok {
		listenerT.ExitTypeRef(s)
	}
}

func (s *TypeRefContext) Accept(visitor antlr.ParseTreeVisitor) interface{} {
	switch t := visitor.(type) {
	case ParadiseVisitor:
		return t.VisitTypeRef(s)

	default:
		return t.VisitChildren(s)
	}
}

func (p *ParadiseParser) TypeRef() (localctx ITypeRefContext) {
	localctx = NewTypeRefContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 10, ParadiseParserRULE_typeRef)

	defer func() {
		p.ExitRule()
	}()

	defer func() {
		if err := recover(); err != nil {
			if v, ok := err.(antlr.RecognitionException); ok {
				localctx.SetException(v)
				p.GetErrorHandler().ReportError(p, v)
				p.GetErrorHandler().Recover(p, v)
			} else {
				panic(err)
			}
		}
	}()

	p.SetState(80)
	p.GetErrorHandler().Sync(p)

	switch p.GetTokenStream().LA(1) {
	case ParadiseParserT__1:
		p.EnterOuterAlt(localctx, 1)
		{
			p.SetState(71)
			p.Match(ParadiseParserT__1)
		}

	case ParadiseParserT__2:
		p.EnterOuterAlt(localctx, 2)
		{
			p.SetState(72)
			p.Match(ParadiseParserT__2)
		}

	case ParadiseParserT__3:
		p.EnterOuterAlt(localctx, 3)
		{
			p.SetState(73)
			p.Match(ParadiseParserT__3)
		}

	case ParadiseParserT__4:
		p.EnterOuterAlt(localctx, 4)
		{
			p.SetState(74)
			p.Match(ParadiseParserT__4)
		}

	case ParadiseParserT__5:
		p.EnterOuterAlt(localctx, 5)
		{
			p.SetState(75)
			p.Match(ParadiseParserT__5)
		}

	case ParadiseParserT__6:
		p.EnterOuterAlt(localctx, 6)
		{
			p.SetState(76)
			p.Match(ParadiseParserT__6)
		}

	case ParadiseParserT__7:
		p.EnterOuterAlt(localctx, 7)
		{
			p.SetState(77)
			p.Match(ParadiseParserT__7)
		}

	case ParadiseParserT__8:
		p.EnterOuterAlt(localctx, 8)
		{
			p.SetState(78)
			p.Match(ParadiseParserT__8)
		}

	case ParadiseParserIDENTIFIER:
		p.EnterOuterAlt(localctx, 9)
		{
			p.SetState(79)
			p.Identifier()
		}

	default:
		panic(antlr.NewNoViableAltException(p, nil, nil, nil, nil, nil))
	}

	return localctx
}

// IStatementContext is an interface to support dynamic dispatch.
type IStatementContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// IsStatementContext differentiates from other interfaces.
	IsStatementContext()
}

type StatementContext struct {
	*antlr.BaseParserRuleContext
	parser antlr.Parser
}

func NewEmptyStatementContext() *StatementContext {
	var p = new(StatementContext)
	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(nil, -1)
	p.RuleIndex = ParadiseParserRULE_statement
	return p
}

func (*StatementContext) IsStatementContext() {}

func NewStatementContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *StatementContext {
	var p = new(StatementContext)

	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(parent, invokingState)

	p.parser = parser
	p.RuleIndex = ParadiseParserRULE_statement

	return p
}

func (s *StatementContext) GetParser() antlr.Parser { return s.parser }

func (s *StatementContext) CopyFrom(ctx *StatementContext) {
	s.BaseParserRuleContext.CopyFrom(ctx.BaseParserRuleContext)
}

func (s *StatementContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *StatementContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

type WhileStmntContext struct {
	*StatementContext
}

func NewWhileStmntContext(parser antlr.Parser, ctx antlr.ParserRuleContext) *WhileStmntContext {
	var p = new(WhileStmntContext)

	p.StatementContext = NewEmptyStatementContext()
	p.parser = parser
	p.CopyFrom(ctx.(*StatementContext))

	return p
}

func (s *WhileStmntContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *WhileStmntContext) WhileCondition() IWhileConditionContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*IWhileConditionContext)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(IWhileConditionContext)
}

func (s *WhileStmntContext) WhileBody() IWhileBodyContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*IWhileBodyContext)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(IWhileBodyContext)
}

func (s *WhileStmntContext) EnterRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(ParadiseListener); ok {
		listenerT.EnterWhileStmnt(s)
	}
}

func (s *WhileStmntContext) ExitRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(ParadiseListener); ok {
		listenerT.ExitWhileStmnt(s)
	}
}

func (s *WhileStmntContext) Accept(visitor antlr.ParseTreeVisitor) interface{} {
	switch t := visitor.(type) {
	case ParadiseVisitor:
		return t.VisitWhileStmnt(s)

	default:
		return t.VisitChildren(s)
	}
}

type IfThenStmntContext struct {
	*StatementContext
}

func NewIfThenStmntContext(parser antlr.Parser, ctx antlr.ParserRuleContext) *IfThenStmntContext {
	var p = new(IfThenStmntContext)

	p.StatementContext = NewEmptyStatementContext()
	p.parser = parser
	p.CopyFrom(ctx.(*StatementContext))

	return p
}

func (s *IfThenStmntContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *IfThenStmntContext) IfCondition() IIfConditionContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*IIfConditionContext)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(IIfConditionContext)
}

func (s *IfThenStmntContext) IfTrueStatement() IIfTrueStatementContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*IIfTrueStatementContext)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(IIfTrueStatementContext)
}

func (s *IfThenStmntContext) EnterRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(ParadiseListener); ok {
		listenerT.EnterIfThenStmnt(s)
	}
}

func (s *IfThenStmntContext) ExitRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(ParadiseListener); ok {
		listenerT.ExitIfThenStmnt(s)
	}
}

func (s *IfThenStmntContext) Accept(visitor antlr.ParseTreeVisitor) interface{} {
	switch t := visitor.(type) {
	case ParadiseVisitor:
		return t.VisitIfThenStmnt(s)

	default:
		return t.VisitChildren(s)
	}
}

type NestedFunctionDeclarationContext struct {
	*StatementContext
}

func NewNestedFunctionDeclarationContext(parser antlr.Parser, ctx antlr.ParserRuleContext) *NestedFunctionDeclarationContext {
	var p = new(NestedFunctionDeclarationContext)

	p.StatementContext = NewEmptyStatementContext()
	p.parser = parser
	p.CopyFrom(ctx.(*StatementContext))

	return p
}

func (s *NestedFunctionDeclarationContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *NestedFunctionDeclarationContext) SourceItem() ISourceItemContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*ISourceItemContext)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(ISourceItemContext)
}

func (s *NestedFunctionDeclarationContext) EnterRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(ParadiseListener); ok {
		listenerT.EnterNestedFunctionDeclaration(s)
	}
}

func (s *NestedFunctionDeclarationContext) ExitRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(ParadiseListener); ok {
		listenerT.ExitNestedFunctionDeclaration(s)
	}
}

func (s *NestedFunctionDeclarationContext) Accept(visitor antlr.ParseTreeVisitor) interface{} {
	switch t := visitor.(type) {
	case ParadiseVisitor:
		return t.VisitNestedFunctionDeclaration(s)

	default:
		return t.VisitChildren(s)
	}
}

type ExprStmntContext struct {
	*StatementContext
}

func NewExprStmntContext(parser antlr.Parser, ctx antlr.ParserRuleContext) *ExprStmntContext {
	var p = new(ExprStmntContext)

	p.StatementContext = NewEmptyStatementContext()
	p.parser = parser
	p.CopyFrom(ctx.(*StatementContext))

	return p
}

func (s *ExprStmntContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *ExprStmntContext) Expr() IExprContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*IExprContext)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(IExprContext)
}

func (s *ExprStmntContext) EnterRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(ParadiseListener); ok {
		listenerT.EnterExprStmnt(s)
	}
}

func (s *ExprStmntContext) ExitRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(ParadiseListener); ok {
		listenerT.ExitExprStmnt(s)
	}
}

func (s *ExprStmntContext) Accept(visitor antlr.ParseTreeVisitor) interface{} {
	switch t := visitor.(type) {
	case ParadiseVisitor:
		return t.VisitExprStmnt(s)

	default:
		return t.VisitChildren(s)
	}
}

type DoWhileStmntContext struct {
	*StatementContext
}

func NewDoWhileStmntContext(parser antlr.Parser, ctx antlr.ParserRuleContext) *DoWhileStmntContext {
	var p = new(DoWhileStmntContext)

	p.StatementContext = NewEmptyStatementContext()
	p.parser = parser
	p.CopyFrom(ctx.(*StatementContext))

	return p
}

func (s *DoWhileStmntContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *DoWhileStmntContext) DoWhileBody() IDoWhileBodyContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*IDoWhileBodyContext)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(IDoWhileBodyContext)
}

func (s *DoWhileStmntContext) DoWhileCondition() IDoWhileConditionContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*IDoWhileConditionContext)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(IDoWhileConditionContext)
}

func (s *DoWhileStmntContext) EnterRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(ParadiseListener); ok {
		listenerT.EnterDoWhileStmnt(s)
	}
}

func (s *DoWhileStmntContext) ExitRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(ParadiseListener); ok {
		listenerT.ExitDoWhileStmnt(s)
	}
}

func (s *DoWhileStmntContext) Accept(visitor antlr.ParseTreeVisitor) interface{} {
	switch t := visitor.(type) {
	case ParadiseVisitor:
		return t.VisitDoWhileStmnt(s)

	default:
		return t.VisitChildren(s)
	}
}

type ReturnStmntContext struct {
	*StatementContext
}

func NewReturnStmntContext(parser antlr.Parser, ctx antlr.ParserRuleContext) *ReturnStmntContext {
	var p = new(ReturnStmntContext)

	p.StatementContext = NewEmptyStatementContext()
	p.parser = parser
	p.CopyFrom(ctx.(*StatementContext))

	return p
}

func (s *ReturnStmntContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *ReturnStmntContext) Expr() IExprContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*IExprContext)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(IExprContext)
}

func (s *ReturnStmntContext) EnterRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(ParadiseListener); ok {
		listenerT.EnterReturnStmnt(s)
	}
}

func (s *ReturnStmntContext) ExitRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(ParadiseListener); ok {
		listenerT.ExitReturnStmnt(s)
	}
}

func (s *ReturnStmntContext) Accept(visitor antlr.ParseTreeVisitor) interface{} {
	switch t := visitor.(type) {
	case ParadiseVisitor:
		return t.VisitReturnStmnt(s)

	default:
		return t.VisitChildren(s)
	}
}

type BreakStmntContext struct {
	*StatementContext
}

func NewBreakStmntContext(parser antlr.Parser, ctx antlr.ParserRuleContext) *BreakStmntContext {
	var p = new(BreakStmntContext)

	p.StatementContext = NewEmptyStatementContext()
	p.parser = parser
	p.CopyFrom(ctx.(*StatementContext))

	return p
}

func (s *BreakStmntContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *BreakStmntContext) EnterRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(ParadiseListener); ok {
		listenerT.EnterBreakStmnt(s)
	}
}

func (s *BreakStmntContext) ExitRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(ParadiseListener); ok {
		listenerT.ExitBreakStmnt(s)
	}
}

func (s *BreakStmntContext) Accept(visitor antlr.ParseTreeVisitor) interface{} {
	switch t := visitor.(type) {
	case ParadiseVisitor:
		return t.VisitBreakStmnt(s)

	default:
		return t.VisitChildren(s)
	}
}

type IdentifierStmntContext struct {
	*StatementContext
}

func NewIdentifierStmntContext(parser antlr.Parser, ctx antlr.ParserRuleContext) *IdentifierStmntContext {
	var p = new(IdentifierStmntContext)

	p.StatementContext = NewEmptyStatementContext()
	p.parser = parser
	p.CopyFrom(ctx.(*StatementContext))

	return p
}

func (s *IdentifierStmntContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *IdentifierStmntContext) Identifier() IIdentifierContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*IIdentifierContext)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(IIdentifierContext)
}

func (s *IdentifierStmntContext) Expr() IExprContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*IExprContext)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(IExprContext)
}

func (s *IdentifierStmntContext) EnterRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(ParadiseListener); ok {
		listenerT.EnterIdentifierStmnt(s)
	}
}

func (s *IdentifierStmntContext) ExitRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(ParadiseListener); ok {
		listenerT.ExitIdentifierStmnt(s)
	}
}

func (s *IdentifierStmntContext) Accept(visitor antlr.ParseTreeVisitor) interface{} {
	switch t := visitor.(type) {
	case ParadiseVisitor:
		return t.VisitIdentifierStmnt(s)

	default:
		return t.VisitChildren(s)
	}
}

type IfThenElseStmntContext struct {
	*StatementContext
}

func NewIfThenElseStmntContext(parser antlr.Parser, ctx antlr.ParserRuleContext) *IfThenElseStmntContext {
	var p = new(IfThenElseStmntContext)

	p.StatementContext = NewEmptyStatementContext()
	p.parser = parser
	p.CopyFrom(ctx.(*StatementContext))

	return p
}

func (s *IfThenElseStmntContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *IfThenElseStmntContext) IfCondition() IIfConditionContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*IIfConditionContext)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(IIfConditionContext)
}

func (s *IfThenElseStmntContext) IfTrueStatement() IIfTrueStatementContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*IIfTrueStatementContext)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(IIfTrueStatementContext)
}

func (s *IfThenElseStmntContext) IfFalseStatement() IIfFalseStatementContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*IIfFalseStatementContext)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(IIfFalseStatementContext)
}

func (s *IfThenElseStmntContext) EnterRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(ParadiseListener); ok {
		listenerT.EnterIfThenElseStmnt(s)
	}
}

func (s *IfThenElseStmntContext) ExitRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(ParadiseListener); ok {
		listenerT.ExitIfThenElseStmnt(s)
	}
}

func (s *IfThenElseStmntContext) Accept(visitor antlr.ParseTreeVisitor) interface{} {
	switch t := visitor.(type) {
	case ParadiseVisitor:
		return t.VisitIfThenElseStmnt(s)

	default:
		return t.VisitChildren(s)
	}
}

func (p *ParadiseParser) Statement() (localctx IStatementContext) {
	localctx = NewStatementContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 12, ParadiseParserRULE_statement)
	var _la int

	defer func() {
		p.ExitRule()
	}()

	defer func() {
		if err := recover(); err != nil {
			if v, ok := err.(antlr.RecognitionException); ok {
				localctx.SetException(v)
				p.GetErrorHandler().ReportError(p, v)
				p.GetErrorHandler().Recover(p, v)
			} else {
				panic(err)
			}
		}
	}()

	p.SetState(122)
	p.GetErrorHandler().Sync(p)
	switch p.GetInterpreter().AdaptivePredict(p.GetTokenStream(), 4, p.GetParserRuleContext()) {
	case 1:
		localctx = NewIfThenStmntContext(p, localctx)
		p.EnterOuterAlt(localctx, 1)
		{
			p.SetState(82)
			p.Match(ParadiseParserT__9)
		}
		{
			p.SetState(83)
			p.IfCondition()
		}
		{
			p.SetState(84)
			p.Match(ParadiseParserT__10)
		}
		{
			p.SetState(85)
			p.IfTrueStatement()
		}
		{
			p.SetState(86)
			p.Match(ParadiseParserT__11)
		}

	case 2:
		localctx = NewIfThenElseStmntContext(p, localctx)
		p.EnterOuterAlt(localctx, 2)
		{
			p.SetState(88)
			p.Match(ParadiseParserT__9)
		}
		{
			p.SetState(89)
			p.IfCondition()
		}
		{
			p.SetState(90)
			p.Match(ParadiseParserT__10)
		}
		{
			p.SetState(91)
			p.IfTrueStatement()
		}
		{
			p.SetState(92)
			p.Match(ParadiseParserT__12)
		}
		{
			p.SetState(93)
			p.IfFalseStatement()
		}
		{
			p.SetState(94)
			p.Match(ParadiseParserT__11)
		}

	case 3:
		localctx = NewWhileStmntContext(p, localctx)
		p.EnterOuterAlt(localctx, 3)
		{
			p.SetState(96)
			p.Match(ParadiseParserT__13)
		}
		{
			p.SetState(97)
			p.WhileCondition()
		}
		{
			p.SetState(98)
			p.WhileBody()
		}
		{
			p.SetState(99)
			p.Match(ParadiseParserT__14)
		}

	case 4:
		localctx = NewDoWhileStmntContext(p, localctx)
		p.EnterOuterAlt(localctx, 4)
		{
			p.SetState(101)
			p.Match(ParadiseParserT__15)
		}
		{
			p.SetState(102)
			p.DoWhileBody()
		}
		{
			p.SetState(103)
			p.Match(ParadiseParserT__16)
		}
		p.SetState(104)
		_la = p.GetTokenStream().LA(1)

		if !(_la == ParadiseParserT__13 || _la == ParadiseParserT__17) {
			p.GetErrorHandler().RecoverInline(p)
		} else {
			p.GetErrorHandler().ReportMatch(p)
			p.Consume()
		}
		{
			p.SetState(105)
			p.DoWhileCondition()
		}

	case 5:
		localctx = NewBreakStmntContext(p, localctx)
		p.EnterOuterAlt(localctx, 5)
		{
			p.SetState(107)
			p.Match(ParadiseParserT__18)
		}
		{
			p.SetState(108)
			p.Match(ParadiseParserT__19)
		}

	case 6:
		localctx = NewIdentifierStmntContext(p, localctx)
		p.EnterOuterAlt(localctx, 6)
		{
			p.SetState(109)
			p.Identifier()
		}
		{
			p.SetState(110)
			p.Match(ParadiseParserT__20)
		}
		{
			p.SetState(111)
			p.expr(0)
		}
		{
			p.SetState(112)
			p.Match(ParadiseParserT__19)
		}

	case 7:
		localctx = NewExprStmntContext(p, localctx)
		p.EnterOuterAlt(localctx, 7)
		{
			p.SetState(114)
			p.expr(0)
		}
		{
			p.SetState(115)
			p.Match(ParadiseParserT__19)
		}

	case 8:
		localctx = NewReturnStmntContext(p, localctx)
		p.EnterOuterAlt(localctx, 8)
		{
			p.SetState(117)
			p.Match(ParadiseParserT__21)
		}
		{
			p.SetState(118)
			p.expr(0)
		}
		{
			p.SetState(119)
			p.Match(ParadiseParserT__19)
		}

	case 9:
		localctx = NewNestedFunctionDeclarationContext(p, localctx)
		p.EnterOuterAlt(localctx, 9)
		{
			p.SetState(121)
			p.SourceItem()
		}

	}

	return localctx
}

// IWhileConditionContext is an interface to support dynamic dispatch.
type IWhileConditionContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// IsWhileConditionContext differentiates from other interfaces.
	IsWhileConditionContext()
}

type WhileConditionContext struct {
	*antlr.BaseParserRuleContext
	parser antlr.Parser
}

func NewEmptyWhileConditionContext() *WhileConditionContext {
	var p = new(WhileConditionContext)
	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(nil, -1)
	p.RuleIndex = ParadiseParserRULE_whileCondition
	return p
}

func (*WhileConditionContext) IsWhileConditionContext() {}

func NewWhileConditionContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *WhileConditionContext {
	var p = new(WhileConditionContext)

	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(parent, invokingState)

	p.parser = parser
	p.RuleIndex = ParadiseParserRULE_whileCondition

	return p
}

func (s *WhileConditionContext) GetParser() antlr.Parser { return s.parser }

func (s *WhileConditionContext) Expr() IExprContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*IExprContext)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(IExprContext)
}

func (s *WhileConditionContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *WhileConditionContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *WhileConditionContext) EnterRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(ParadiseListener); ok {
		listenerT.EnterWhileCondition(s)
	}
}

func (s *WhileConditionContext) ExitRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(ParadiseListener); ok {
		listenerT.ExitWhileCondition(s)
	}
}

func (s *WhileConditionContext) Accept(visitor antlr.ParseTreeVisitor) interface{} {
	switch t := visitor.(type) {
	case ParadiseVisitor:
		return t.VisitWhileCondition(s)

	default:
		return t.VisitChildren(s)
	}
}

func (p *ParadiseParser) WhileCondition() (localctx IWhileConditionContext) {
	localctx = NewWhileConditionContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 14, ParadiseParserRULE_whileCondition)

	defer func() {
		p.ExitRule()
	}()

	defer func() {
		if err := recover(); err != nil {
			if v, ok := err.(antlr.RecognitionException); ok {
				localctx.SetException(v)
				p.GetErrorHandler().ReportError(p, v)
				p.GetErrorHandler().Recover(p, v)
			} else {
				panic(err)
			}
		}
	}()

	p.EnterOuterAlt(localctx, 1)
	{
		p.SetState(124)
		p.expr(0)
	}

	return localctx
}

// IWhileBodyContext is an interface to support dynamic dispatch.
type IWhileBodyContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// IsWhileBodyContext differentiates from other interfaces.
	IsWhileBodyContext()
}

type WhileBodyContext struct {
	*antlr.BaseParserRuleContext
	parser antlr.Parser
}

func NewEmptyWhileBodyContext() *WhileBodyContext {
	var p = new(WhileBodyContext)
	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(nil, -1)
	p.RuleIndex = ParadiseParserRULE_whileBody
	return p
}

func (*WhileBodyContext) IsWhileBodyContext() {}

func NewWhileBodyContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *WhileBodyContext {
	var p = new(WhileBodyContext)

	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(parent, invokingState)

	p.parser = parser
	p.RuleIndex = ParadiseParserRULE_whileBody

	return p
}

func (s *WhileBodyContext) GetParser() antlr.Parser { return s.parser }

func (s *WhileBodyContext) AllStatement() []IStatementContext {
	var ts = s.GetTypedRuleContexts(reflect.TypeOf((*IStatementContext)(nil)).Elem())
	var tst = make([]IStatementContext, len(ts))

	for i, t := range ts {
		if t != nil {
			tst[i] = t.(IStatementContext)
		}
	}

	return tst
}

func (s *WhileBodyContext) Statement(i int) IStatementContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*IStatementContext)(nil)).Elem(), i)

	if t == nil {
		return nil
	}

	return t.(IStatementContext)
}

func (s *WhileBodyContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *WhileBodyContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *WhileBodyContext) EnterRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(ParadiseListener); ok {
		listenerT.EnterWhileBody(s)
	}
}

func (s *WhileBodyContext) ExitRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(ParadiseListener); ok {
		listenerT.ExitWhileBody(s)
	}
}

func (s *WhileBodyContext) Accept(visitor antlr.ParseTreeVisitor) interface{} {
	switch t := visitor.(type) {
	case ParadiseVisitor:
		return t.VisitWhileBody(s)

	default:
		return t.VisitChildren(s)
	}
}

func (p *ParadiseParser) WhileBody() (localctx IWhileBodyContext) {
	localctx = NewWhileBodyContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 16, ParadiseParserRULE_whileBody)
	var _la int

	defer func() {
		p.ExitRule()
	}()

	defer func() {
		if err := recover(); err != nil {
			if v, ok := err.(antlr.RecognitionException); ok {
				localctx.SetException(v)
				p.GetErrorHandler().ReportError(p, v)
				p.GetErrorHandler().Recover(p, v)
			} else {
				panic(err)
			}
		}
	}()

	p.EnterOuterAlt(localctx, 1)
	p.SetState(129)
	p.GetErrorHandler().Sync(p)
	_la = p.GetTokenStream().LA(1)

	for (((_la)&-(0x1f+1)) == 0 && ((1<<uint(_la))&((1<<ParadiseParserT__9)|(1<<ParadiseParserT__13)|(1<<ParadiseParserT__15)|(1<<ParadiseParserT__18)|(1<<ParadiseParserT__21)|(1<<ParadiseParserT__23)|(1<<ParadiseParserT__24))) != 0) || (((_la-38)&-(0x1f+1)) == 0 && ((1<<uint((_la-38)))&((1<<(ParadiseParserT__37-38))|(1<<(ParadiseParserT__38-38))|(1<<(ParadiseParserT__39-38))|(1<<(ParadiseParserFUNCTION-38))|(1<<(ParadiseParserLBRACE-38))|(1<<(ParadiseParserCHAR-38))|(1<<(ParadiseParserSTRING-38))|(1<<(ParadiseParserHEX-38))|(1<<(ParadiseParserBITS-38))|(1<<(ParadiseParserDEC-38))|(1<<(ParadiseParserBOOL-38))|(1<<(ParadiseParserIDENTIFIER-38)))) != 0) {
		{
			p.SetState(126)
			p.Statement()
		}

		p.SetState(131)
		p.GetErrorHandler().Sync(p)
		_la = p.GetTokenStream().LA(1)
	}

	return localctx
}

// IDoWhileBodyContext is an interface to support dynamic dispatch.
type IDoWhileBodyContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// IsDoWhileBodyContext differentiates from other interfaces.
	IsDoWhileBodyContext()
}

type DoWhileBodyContext struct {
	*antlr.BaseParserRuleContext
	parser antlr.Parser
}

func NewEmptyDoWhileBodyContext() *DoWhileBodyContext {
	var p = new(DoWhileBodyContext)
	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(nil, -1)
	p.RuleIndex = ParadiseParserRULE_doWhileBody
	return p
}

func (*DoWhileBodyContext) IsDoWhileBodyContext() {}

func NewDoWhileBodyContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *DoWhileBodyContext {
	var p = new(DoWhileBodyContext)

	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(parent, invokingState)

	p.parser = parser
	p.RuleIndex = ParadiseParserRULE_doWhileBody

	return p
}

func (s *DoWhileBodyContext) GetParser() antlr.Parser { return s.parser }

func (s *DoWhileBodyContext) AllStatement() []IStatementContext {
	var ts = s.GetTypedRuleContexts(reflect.TypeOf((*IStatementContext)(nil)).Elem())
	var tst = make([]IStatementContext, len(ts))

	for i, t := range ts {
		if t != nil {
			tst[i] = t.(IStatementContext)
		}
	}

	return tst
}

func (s *DoWhileBodyContext) Statement(i int) IStatementContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*IStatementContext)(nil)).Elem(), i)

	if t == nil {
		return nil
	}

	return t.(IStatementContext)
}

func (s *DoWhileBodyContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *DoWhileBodyContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *DoWhileBodyContext) EnterRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(ParadiseListener); ok {
		listenerT.EnterDoWhileBody(s)
	}
}

func (s *DoWhileBodyContext) ExitRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(ParadiseListener); ok {
		listenerT.ExitDoWhileBody(s)
	}
}

func (s *DoWhileBodyContext) Accept(visitor antlr.ParseTreeVisitor) interface{} {
	switch t := visitor.(type) {
	case ParadiseVisitor:
		return t.VisitDoWhileBody(s)

	default:
		return t.VisitChildren(s)
	}
}

func (p *ParadiseParser) DoWhileBody() (localctx IDoWhileBodyContext) {
	localctx = NewDoWhileBodyContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 18, ParadiseParserRULE_doWhileBody)
	var _la int

	defer func() {
		p.ExitRule()
	}()

	defer func() {
		if err := recover(); err != nil {
			if v, ok := err.(antlr.RecognitionException); ok {
				localctx.SetException(v)
				p.GetErrorHandler().ReportError(p, v)
				p.GetErrorHandler().Recover(p, v)
			} else {
				panic(err)
			}
		}
	}()

	p.EnterOuterAlt(localctx, 1)
	p.SetState(135)
	p.GetErrorHandler().Sync(p)
	_la = p.GetTokenStream().LA(1)

	for (((_la)&-(0x1f+1)) == 0 && ((1<<uint(_la))&((1<<ParadiseParserT__9)|(1<<ParadiseParserT__13)|(1<<ParadiseParserT__15)|(1<<ParadiseParserT__18)|(1<<ParadiseParserT__21)|(1<<ParadiseParserT__23)|(1<<ParadiseParserT__24))) != 0) || (((_la-38)&-(0x1f+1)) == 0 && ((1<<uint((_la-38)))&((1<<(ParadiseParserT__37-38))|(1<<(ParadiseParserT__38-38))|(1<<(ParadiseParserT__39-38))|(1<<(ParadiseParserFUNCTION-38))|(1<<(ParadiseParserLBRACE-38))|(1<<(ParadiseParserCHAR-38))|(1<<(ParadiseParserSTRING-38))|(1<<(ParadiseParserHEX-38))|(1<<(ParadiseParserBITS-38))|(1<<(ParadiseParserDEC-38))|(1<<(ParadiseParserBOOL-38))|(1<<(ParadiseParserIDENTIFIER-38)))) != 0) {
		{
			p.SetState(132)
			p.Statement()
		}

		p.SetState(137)
		p.GetErrorHandler().Sync(p)
		_la = p.GetTokenStream().LA(1)
	}

	return localctx
}

// IDoWhileConditionContext is an interface to support dynamic dispatch.
type IDoWhileConditionContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// IsDoWhileConditionContext differentiates from other interfaces.
	IsDoWhileConditionContext()
}

type DoWhileConditionContext struct {
	*antlr.BaseParserRuleContext
	parser antlr.Parser
}

func NewEmptyDoWhileConditionContext() *DoWhileConditionContext {
	var p = new(DoWhileConditionContext)
	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(nil, -1)
	p.RuleIndex = ParadiseParserRULE_doWhileCondition
	return p
}

func (*DoWhileConditionContext) IsDoWhileConditionContext() {}

func NewDoWhileConditionContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *DoWhileConditionContext {
	var p = new(DoWhileConditionContext)

	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(parent, invokingState)

	p.parser = parser
	p.RuleIndex = ParadiseParserRULE_doWhileCondition

	return p
}

func (s *DoWhileConditionContext) GetParser() antlr.Parser { return s.parser }

func (s *DoWhileConditionContext) Expr() IExprContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*IExprContext)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(IExprContext)
}

func (s *DoWhileConditionContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *DoWhileConditionContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *DoWhileConditionContext) EnterRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(ParadiseListener); ok {
		listenerT.EnterDoWhileCondition(s)
	}
}

func (s *DoWhileConditionContext) ExitRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(ParadiseListener); ok {
		listenerT.ExitDoWhileCondition(s)
	}
}

func (s *DoWhileConditionContext) Accept(visitor antlr.ParseTreeVisitor) interface{} {
	switch t := visitor.(type) {
	case ParadiseVisitor:
		return t.VisitDoWhileCondition(s)

	default:
		return t.VisitChildren(s)
	}
}

func (p *ParadiseParser) DoWhileCondition() (localctx IDoWhileConditionContext) {
	localctx = NewDoWhileConditionContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 20, ParadiseParserRULE_doWhileCondition)

	defer func() {
		p.ExitRule()
	}()

	defer func() {
		if err := recover(); err != nil {
			if v, ok := err.(antlr.RecognitionException); ok {
				localctx.SetException(v)
				p.GetErrorHandler().ReportError(p, v)
				p.GetErrorHandler().Recover(p, v)
			} else {
				panic(err)
			}
		}
	}()

	p.EnterOuterAlt(localctx, 1)
	{
		p.SetState(138)
		p.expr(0)
	}

	return localctx
}

// IIfConditionContext is an interface to support dynamic dispatch.
type IIfConditionContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// IsIfConditionContext differentiates from other interfaces.
	IsIfConditionContext()
}

type IfConditionContext struct {
	*antlr.BaseParserRuleContext
	parser antlr.Parser
}

func NewEmptyIfConditionContext() *IfConditionContext {
	var p = new(IfConditionContext)
	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(nil, -1)
	p.RuleIndex = ParadiseParserRULE_ifCondition
	return p
}

func (*IfConditionContext) IsIfConditionContext() {}

func NewIfConditionContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *IfConditionContext {
	var p = new(IfConditionContext)

	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(parent, invokingState)

	p.parser = parser
	p.RuleIndex = ParadiseParserRULE_ifCondition

	return p
}

func (s *IfConditionContext) GetParser() antlr.Parser { return s.parser }

func (s *IfConditionContext) Expr() IExprContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*IExprContext)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(IExprContext)
}

func (s *IfConditionContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *IfConditionContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *IfConditionContext) EnterRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(ParadiseListener); ok {
		listenerT.EnterIfCondition(s)
	}
}

func (s *IfConditionContext) ExitRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(ParadiseListener); ok {
		listenerT.ExitIfCondition(s)
	}
}

func (s *IfConditionContext) Accept(visitor antlr.ParseTreeVisitor) interface{} {
	switch t := visitor.(type) {
	case ParadiseVisitor:
		return t.VisitIfCondition(s)

	default:
		return t.VisitChildren(s)
	}
}

func (p *ParadiseParser) IfCondition() (localctx IIfConditionContext) {
	localctx = NewIfConditionContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 22, ParadiseParserRULE_ifCondition)

	defer func() {
		p.ExitRule()
	}()

	defer func() {
		if err := recover(); err != nil {
			if v, ok := err.(antlr.RecognitionException); ok {
				localctx.SetException(v)
				p.GetErrorHandler().ReportError(p, v)
				p.GetErrorHandler().Recover(p, v)
			} else {
				panic(err)
			}
		}
	}()

	p.EnterOuterAlt(localctx, 1)
	{
		p.SetState(140)
		p.expr(0)
	}

	return localctx
}

// IIfTrueStatementContext is an interface to support dynamic dispatch.
type IIfTrueStatementContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// IsIfTrueStatementContext differentiates from other interfaces.
	IsIfTrueStatementContext()
}

type IfTrueStatementContext struct {
	*antlr.BaseParserRuleContext
	parser antlr.Parser
}

func NewEmptyIfTrueStatementContext() *IfTrueStatementContext {
	var p = new(IfTrueStatementContext)
	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(nil, -1)
	p.RuleIndex = ParadiseParserRULE_ifTrueStatement
	return p
}

func (*IfTrueStatementContext) IsIfTrueStatementContext() {}

func NewIfTrueStatementContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *IfTrueStatementContext {
	var p = new(IfTrueStatementContext)

	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(parent, invokingState)

	p.parser = parser
	p.RuleIndex = ParadiseParserRULE_ifTrueStatement

	return p
}

func (s *IfTrueStatementContext) GetParser() antlr.Parser { return s.parser }

func (s *IfTrueStatementContext) AllStatement() []IStatementContext {
	var ts = s.GetTypedRuleContexts(reflect.TypeOf((*IStatementContext)(nil)).Elem())
	var tst = make([]IStatementContext, len(ts))

	for i, t := range ts {
		if t != nil {
			tst[i] = t.(IStatementContext)
		}
	}

	return tst
}

func (s *IfTrueStatementContext) Statement(i int) IStatementContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*IStatementContext)(nil)).Elem(), i)

	if t == nil {
		return nil
	}

	return t.(IStatementContext)
}

func (s *IfTrueStatementContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *IfTrueStatementContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *IfTrueStatementContext) EnterRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(ParadiseListener); ok {
		listenerT.EnterIfTrueStatement(s)
	}
}

func (s *IfTrueStatementContext) ExitRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(ParadiseListener); ok {
		listenerT.ExitIfTrueStatement(s)
	}
}

func (s *IfTrueStatementContext) Accept(visitor antlr.ParseTreeVisitor) interface{} {
	switch t := visitor.(type) {
	case ParadiseVisitor:
		return t.VisitIfTrueStatement(s)

	default:
		return t.VisitChildren(s)
	}
}

func (p *ParadiseParser) IfTrueStatement() (localctx IIfTrueStatementContext) {
	localctx = NewIfTrueStatementContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 24, ParadiseParserRULE_ifTrueStatement)
	var _la int

	defer func() {
		p.ExitRule()
	}()

	defer func() {
		if err := recover(); err != nil {
			if v, ok := err.(antlr.RecognitionException); ok {
				localctx.SetException(v)
				p.GetErrorHandler().ReportError(p, v)
				p.GetErrorHandler().Recover(p, v)
			} else {
				panic(err)
			}
		}
	}()

	p.EnterOuterAlt(localctx, 1)
	p.SetState(145)
	p.GetErrorHandler().Sync(p)
	_la = p.GetTokenStream().LA(1)

	for (((_la)&-(0x1f+1)) == 0 && ((1<<uint(_la))&((1<<ParadiseParserT__9)|(1<<ParadiseParserT__13)|(1<<ParadiseParserT__15)|(1<<ParadiseParserT__18)|(1<<ParadiseParserT__21)|(1<<ParadiseParserT__23)|(1<<ParadiseParserT__24))) != 0) || (((_la-38)&-(0x1f+1)) == 0 && ((1<<uint((_la-38)))&((1<<(ParadiseParserT__37-38))|(1<<(ParadiseParserT__38-38))|(1<<(ParadiseParserT__39-38))|(1<<(ParadiseParserFUNCTION-38))|(1<<(ParadiseParserLBRACE-38))|(1<<(ParadiseParserCHAR-38))|(1<<(ParadiseParserSTRING-38))|(1<<(ParadiseParserHEX-38))|(1<<(ParadiseParserBITS-38))|(1<<(ParadiseParserDEC-38))|(1<<(ParadiseParserBOOL-38))|(1<<(ParadiseParserIDENTIFIER-38)))) != 0) {
		{
			p.SetState(142)
			p.Statement()
		}

		p.SetState(147)
		p.GetErrorHandler().Sync(p)
		_la = p.GetTokenStream().LA(1)
	}

	return localctx
}

// IIfFalseStatementContext is an interface to support dynamic dispatch.
type IIfFalseStatementContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// IsIfFalseStatementContext differentiates from other interfaces.
	IsIfFalseStatementContext()
}

type IfFalseStatementContext struct {
	*antlr.BaseParserRuleContext
	parser antlr.Parser
}

func NewEmptyIfFalseStatementContext() *IfFalseStatementContext {
	var p = new(IfFalseStatementContext)
	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(nil, -1)
	p.RuleIndex = ParadiseParserRULE_ifFalseStatement
	return p
}

func (*IfFalseStatementContext) IsIfFalseStatementContext() {}

func NewIfFalseStatementContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *IfFalseStatementContext {
	var p = new(IfFalseStatementContext)

	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(parent, invokingState)

	p.parser = parser
	p.RuleIndex = ParadiseParserRULE_ifFalseStatement

	return p
}

func (s *IfFalseStatementContext) GetParser() antlr.Parser { return s.parser }

func (s *IfFalseStatementContext) AllStatement() []IStatementContext {
	var ts = s.GetTypedRuleContexts(reflect.TypeOf((*IStatementContext)(nil)).Elem())
	var tst = make([]IStatementContext, len(ts))

	for i, t := range ts {
		if t != nil {
			tst[i] = t.(IStatementContext)
		}
	}

	return tst
}

func (s *IfFalseStatementContext) Statement(i int) IStatementContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*IStatementContext)(nil)).Elem(), i)

	if t == nil {
		return nil
	}

	return t.(IStatementContext)
}

func (s *IfFalseStatementContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *IfFalseStatementContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *IfFalseStatementContext) EnterRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(ParadiseListener); ok {
		listenerT.EnterIfFalseStatement(s)
	}
}

func (s *IfFalseStatementContext) ExitRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(ParadiseListener); ok {
		listenerT.ExitIfFalseStatement(s)
	}
}

func (s *IfFalseStatementContext) Accept(visitor antlr.ParseTreeVisitor) interface{} {
	switch t := visitor.(type) {
	case ParadiseVisitor:
		return t.VisitIfFalseStatement(s)

	default:
		return t.VisitChildren(s)
	}
}

func (p *ParadiseParser) IfFalseStatement() (localctx IIfFalseStatementContext) {
	localctx = NewIfFalseStatementContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 26, ParadiseParserRULE_ifFalseStatement)
	var _la int

	defer func() {
		p.ExitRule()
	}()

	defer func() {
		if err := recover(); err != nil {
			if v, ok := err.(antlr.RecognitionException); ok {
				localctx.SetException(v)
				p.GetErrorHandler().ReportError(p, v)
				p.GetErrorHandler().Recover(p, v)
			} else {
				panic(err)
			}
		}
	}()

	p.EnterOuterAlt(localctx, 1)
	p.SetState(151)
	p.GetErrorHandler().Sync(p)
	_la = p.GetTokenStream().LA(1)

	for (((_la)&-(0x1f+1)) == 0 && ((1<<uint(_la))&((1<<ParadiseParserT__9)|(1<<ParadiseParserT__13)|(1<<ParadiseParserT__15)|(1<<ParadiseParserT__18)|(1<<ParadiseParserT__21)|(1<<ParadiseParserT__23)|(1<<ParadiseParserT__24))) != 0) || (((_la-38)&-(0x1f+1)) == 0 && ((1<<uint((_la-38)))&((1<<(ParadiseParserT__37-38))|(1<<(ParadiseParserT__38-38))|(1<<(ParadiseParserT__39-38))|(1<<(ParadiseParserFUNCTION-38))|(1<<(ParadiseParserLBRACE-38))|(1<<(ParadiseParserCHAR-38))|(1<<(ParadiseParserSTRING-38))|(1<<(ParadiseParserHEX-38))|(1<<(ParadiseParserBITS-38))|(1<<(ParadiseParserDEC-38))|(1<<(ParadiseParserBOOL-38))|(1<<(ParadiseParserIDENTIFIER-38)))) != 0) {
		{
			p.SetState(148)
			p.Statement()
		}

		p.SetState(153)
		p.GetErrorHandler().Sync(p)
		_la = p.GetTokenStream().LA(1)
	}

	return localctx
}

// IExprContext is an interface to support dynamic dispatch.
type IExprContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// IsExprContext differentiates from other interfaces.
	IsExprContext()
}

type ExprContext struct {
	*antlr.BaseParserRuleContext
	parser antlr.Parser
}

func NewEmptyExprContext() *ExprContext {
	var p = new(ExprContext)
	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(nil, -1)
	p.RuleIndex = ParadiseParserRULE_expr
	return p
}

func (*ExprContext) IsExprContext() {}

func NewExprContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *ExprContext {
	var p = new(ExprContext)

	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(parent, invokingState)

	p.parser = parser
	p.RuleIndex = ParadiseParserRULE_expr

	return p
}

func (s *ExprContext) GetParser() antlr.Parser { return s.parser }

func (s *ExprContext) CopyFrom(ctx *ExprContext) {
	s.BaseParserRuleContext.CopyFrom(ctx.BaseParserRuleContext)
}

func (s *ExprContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *ExprContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

type PrefixUnaryExprContext struct {
	*ExprContext
}

func NewPrefixUnaryExprContext(parser antlr.Parser, ctx antlr.ParserRuleContext) *PrefixUnaryExprContext {
	var p = new(PrefixUnaryExprContext)

	p.ExprContext = NewEmptyExprContext()
	p.parser = parser
	p.CopyFrom(ctx.(*ExprContext))

	return p
}

func (s *PrefixUnaryExprContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *PrefixUnaryExprContext) Unary() IUnaryContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*IUnaryContext)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(IUnaryContext)
}

func (s *PrefixUnaryExprContext) Expr() IExprContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*IExprContext)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(IExprContext)
}

func (s *PrefixUnaryExprContext) EnterRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(ParadiseListener); ok {
		listenerT.EnterPrefixUnaryExpr(s)
	}
}

func (s *PrefixUnaryExprContext) ExitRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(ParadiseListener); ok {
		listenerT.ExitPrefixUnaryExpr(s)
	}
}

func (s *PrefixUnaryExprContext) Accept(visitor antlr.ParseTreeVisitor) interface{} {
	switch t := visitor.(type) {
	case ParadiseVisitor:
		return t.VisitPrefixUnaryExpr(s)

	default:
		return t.VisitChildren(s)
	}
}

type ValueExprContext struct {
	*ExprContext
}

func NewValueExprContext(parser antlr.Parser, ctx antlr.ParserRuleContext) *ValueExprContext {
	var p = new(ValueExprContext)

	p.ExprContext = NewEmptyExprContext()
	p.parser = parser
	p.CopyFrom(ctx.(*ExprContext))

	return p
}

func (s *ValueExprContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *ValueExprContext) BOOL() antlr.TerminalNode {
	return s.GetToken(ParadiseParserBOOL, 0)
}

func (s *ValueExprContext) STRING() antlr.TerminalNode {
	return s.GetToken(ParadiseParserSTRING, 0)
}

func (s *ValueExprContext) CHAR() antlr.TerminalNode {
	return s.GetToken(ParadiseParserCHAR, 0)
}

func (s *ValueExprContext) HEX() antlr.TerminalNode {
	return s.GetToken(ParadiseParserHEX, 0)
}

func (s *ValueExprContext) BITS() antlr.TerminalNode {
	return s.GetToken(ParadiseParserBITS, 0)
}

func (s *ValueExprContext) DEC() antlr.TerminalNode {
	return s.GetToken(ParadiseParserDEC, 0)
}

func (s *ValueExprContext) EnterRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(ParadiseListener); ok {
		listenerT.EnterValueExpr(s)
	}
}

func (s *ValueExprContext) ExitRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(ParadiseListener); ok {
		listenerT.ExitValueExpr(s)
	}
}

func (s *ValueExprContext) Accept(visitor antlr.ParseTreeVisitor) interface{} {
	switch t := visitor.(type) {
	case ParadiseVisitor:
		return t.VisitValueExpr(s)

	default:
		return t.VisitChildren(s)
	}
}

type VariableContext struct {
	*ExprContext
}

func NewVariableContext(parser antlr.Parser, ctx antlr.ParserRuleContext) *VariableContext {
	var p = new(VariableContext)

	p.ExprContext = NewEmptyExprContext()
	p.parser = parser
	p.CopyFrom(ctx.(*ExprContext))

	return p
}

func (s *VariableContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *VariableContext) Identifier() IIdentifierContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*IIdentifierContext)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(IIdentifierContext)
}

func (s *VariableContext) EnterRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(ParadiseListener); ok {
		listenerT.EnterVariable(s)
	}
}

func (s *VariableContext) ExitRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(ParadiseListener); ok {
		listenerT.ExitVariable(s)
	}
}

func (s *VariableContext) Accept(visitor antlr.ParseTreeVisitor) interface{} {
	switch t := visitor.(type) {
	case ParadiseVisitor:
		return t.VisitVariable(s)

	default:
		return t.VisitChildren(s)
	}
}

type BinaryExprContext struct {
	*ExprContext
}

func NewBinaryExprContext(parser antlr.Parser, ctx antlr.ParserRuleContext) *BinaryExprContext {
	var p = new(BinaryExprContext)

	p.ExprContext = NewEmptyExprContext()
	p.parser = parser
	p.CopyFrom(ctx.(*ExprContext))

	return p
}

func (s *BinaryExprContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *BinaryExprContext) AllExpr() []IExprContext {
	var ts = s.GetTypedRuleContexts(reflect.TypeOf((*IExprContext)(nil)).Elem())
	var tst = make([]IExprContext, len(ts))

	for i, t := range ts {
		if t != nil {
			tst[i] = t.(IExprContext)
		}
	}

	return tst
}

func (s *BinaryExprContext) Expr(i int) IExprContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*IExprContext)(nil)).Elem(), i)

	if t == nil {
		return nil
	}

	return t.(IExprContext)
}

func (s *BinaryExprContext) Binary() IBinaryContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*IBinaryContext)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(IBinaryContext)
}

func (s *BinaryExprContext) EnterRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(ParadiseListener); ok {
		listenerT.EnterBinaryExpr(s)
	}
}

func (s *BinaryExprContext) ExitRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(ParadiseListener); ok {
		listenerT.ExitBinaryExpr(s)
	}
}

func (s *BinaryExprContext) Accept(visitor antlr.ParseTreeVisitor) interface{} {
	switch t := visitor.(type) {
	case ParadiseVisitor:
		return t.VisitBinaryExpr(s)

	default:
		return t.VisitChildren(s)
	}
}

type BracedExprContext struct {
	*ExprContext
}

func NewBracedExprContext(parser antlr.Parser, ctx antlr.ParserRuleContext) *BracedExprContext {
	var p = new(BracedExprContext)

	p.ExprContext = NewEmptyExprContext()
	p.parser = parser
	p.CopyFrom(ctx.(*ExprContext))

	return p
}

func (s *BracedExprContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *BracedExprContext) LBRACE() antlr.TerminalNode {
	return s.GetToken(ParadiseParserLBRACE, 0)
}

func (s *BracedExprContext) Expr() IExprContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*IExprContext)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(IExprContext)
}

func (s *BracedExprContext) RBRACE() antlr.TerminalNode {
	return s.GetToken(ParadiseParserRBRACE, 0)
}

func (s *BracedExprContext) EnterRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(ParadiseListener); ok {
		listenerT.EnterBracedExpr(s)
	}
}

func (s *BracedExprContext) ExitRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(ParadiseListener); ok {
		listenerT.ExitBracedExpr(s)
	}
}

func (s *BracedExprContext) Accept(visitor antlr.ParseTreeVisitor) interface{} {
	switch t := visitor.(type) {
	case ParadiseVisitor:
		return t.VisitBracedExpr(s)

	default:
		return t.VisitChildren(s)
	}
}

type PostfixUnaryExprContext struct {
	*ExprContext
}

func NewPostfixUnaryExprContext(parser antlr.Parser, ctx antlr.ParserRuleContext) *PostfixUnaryExprContext {
	var p = new(PostfixUnaryExprContext)

	p.ExprContext = NewEmptyExprContext()
	p.parser = parser
	p.CopyFrom(ctx.(*ExprContext))

	return p
}

func (s *PostfixUnaryExprContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *PostfixUnaryExprContext) Expr() IExprContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*IExprContext)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(IExprContext)
}

func (s *PostfixUnaryExprContext) Unary() IUnaryContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*IUnaryContext)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(IUnaryContext)
}

func (s *PostfixUnaryExprContext) EnterRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(ParadiseListener); ok {
		listenerT.EnterPostfixUnaryExpr(s)
	}
}

func (s *PostfixUnaryExprContext) ExitRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(ParadiseListener); ok {
		listenerT.ExitPostfixUnaryExpr(s)
	}
}

func (s *PostfixUnaryExprContext) Accept(visitor antlr.ParseTreeVisitor) interface{} {
	switch t := visitor.(type) {
	case ParadiseVisitor:
		return t.VisitPostfixUnaryExpr(s)

	default:
		return t.VisitChildren(s)
	}
}

type FunctionCallContext struct {
	*ExprContext
}

func NewFunctionCallContext(parser antlr.Parser, ctx antlr.ParserRuleContext) *FunctionCallContext {
	var p = new(FunctionCallContext)

	p.ExprContext = NewEmptyExprContext()
	p.parser = parser
	p.CopyFrom(ctx.(*ExprContext))

	return p
}

func (s *FunctionCallContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *FunctionCallContext) Expr() IExprContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*IExprContext)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(IExprContext)
}

func (s *FunctionCallContext) ExprList() IExprListContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*IExprListContext)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(IExprListContext)
}

func (s *FunctionCallContext) EnterRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(ParadiseListener); ok {
		listenerT.EnterFunctionCall(s)
	}
}

func (s *FunctionCallContext) ExitRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(ParadiseListener); ok {
		listenerT.ExitFunctionCall(s)
	}
}

func (s *FunctionCallContext) Accept(visitor antlr.ParseTreeVisitor) interface{} {
	switch t := visitor.(type) {
	case ParadiseVisitor:
		return t.VisitFunctionCall(s)

	default:
		return t.VisitChildren(s)
	}
}

func (p *ParadiseParser) Expr() (localctx IExprContext) {
	return p.expr(0)
}

func (p *ParadiseParser) expr(_p int) (localctx IExprContext) {
	var _parentctx antlr.ParserRuleContext = p.GetParserRuleContext()
	_parentState := p.GetState()
	localctx = NewExprContext(p, p.GetParserRuleContext(), _parentState)
	var _prevctx IExprContext = localctx
	var _ antlr.ParserRuleContext = _prevctx // TODO: To prevent unused variable warning.
	_startState := 28
	p.EnterRecursionRule(localctx, 28, ParadiseParserRULE_expr, _p)
	var _la int

	defer func() {
		p.UnrollRecursionContexts(_parentctx)
	}()

	defer func() {
		if err := recover(); err != nil {
			if v, ok := err.(antlr.RecognitionException); ok {
				localctx.SetException(v)
				p.GetErrorHandler().ReportError(p, v)
				p.GetErrorHandler().Recover(p, v)
			} else {
				panic(err)
			}
		}
	}()

	var _alt int

	p.EnterOuterAlt(localctx, 1)
	p.SetState(164)
	p.GetErrorHandler().Sync(p)

	switch p.GetTokenStream().LA(1) {
	case ParadiseParserCHAR, ParadiseParserSTRING, ParadiseParserHEX, ParadiseParserBITS, ParadiseParserDEC, ParadiseParserBOOL:
		localctx = NewValueExprContext(p, localctx)
		p.SetParserRuleContext(localctx)
		_prevctx = localctx

		p.SetState(155)
		_la = p.GetTokenStream().LA(1)

		if !(((_la-45)&-(0x1f+1)) == 0 && ((1<<uint((_la-45)))&((1<<(ParadiseParserCHAR-45))|(1<<(ParadiseParserSTRING-45))|(1<<(ParadiseParserHEX-45))|(1<<(ParadiseParserBITS-45))|(1<<(ParadiseParserDEC-45))|(1<<(ParadiseParserBOOL-45)))) != 0) {
			p.GetErrorHandler().RecoverInline(p)
		} else {
			p.GetErrorHandler().ReportMatch(p)
			p.Consume()
		}

	case ParadiseParserT__23, ParadiseParserT__24, ParadiseParserT__37, ParadiseParserT__38, ParadiseParserT__39:
		localctx = NewPrefixUnaryExprContext(p, localctx)
		p.SetParserRuleContext(localctx)
		_prevctx = localctx
		{
			p.SetState(156)
			p.Unary()
		}
		{
			p.SetState(157)
			p.expr(5)
		}

	case ParadiseParserIDENTIFIER:
		localctx = NewVariableContext(p, localctx)
		p.SetParserRuleContext(localctx)
		_prevctx = localctx
		{
			p.SetState(159)
			p.Identifier()
		}

	case ParadiseParserLBRACE:
		localctx = NewBracedExprContext(p, localctx)
		p.SetParserRuleContext(localctx)
		_prevctx = localctx
		{
			p.SetState(160)
			p.Match(ParadiseParserLBRACE)
		}
		{
			p.SetState(161)
			p.expr(0)
		}
		{
			p.SetState(162)
			p.Match(ParadiseParserRBRACE)
		}

	default:
		panic(antlr.NewNoViableAltException(p, nil, nil, nil, nil, nil))
	}
	p.GetParserRuleContext().SetStop(p.GetTokenStream().LT(-1))
	p.SetState(179)
	p.GetErrorHandler().Sync(p)
	_alt = p.GetInterpreter().AdaptivePredict(p.GetTokenStream(), 11, p.GetParserRuleContext())

	for _alt != 2 && _alt != antlr.ATNInvalidAltNumber {
		if _alt == 1 {
			if p.GetParseListeners() != nil {
				p.TriggerExitRuleEvent()
			}
			_prevctx = localctx
			p.SetState(177)
			p.GetErrorHandler().Sync(p)
			switch p.GetInterpreter().AdaptivePredict(p.GetTokenStream(), 10, p.GetParserRuleContext()) {
			case 1:
				localctx = NewBinaryExprContext(p, NewExprContext(p, _parentctx, _parentState))
				p.PushNewRecursionContext(localctx, _startState, ParadiseParserRULE_expr)
				p.SetState(166)

				if !(p.Precpred(p.GetParserRuleContext(), 7)) {
					panic(antlr.NewFailedPredicateException(p, "p.Precpred(p.GetParserRuleContext(), 7)", ""))
				}
				{
					p.SetState(167)
					p.Binary()
				}
				{
					p.SetState(168)
					p.expr(8)
				}

			case 2:
				localctx = NewPostfixUnaryExprContext(p, NewExprContext(p, _parentctx, _parentState))
				p.PushNewRecursionContext(localctx, _startState, ParadiseParserRULE_expr)
				p.SetState(170)

				if !(p.Precpred(p.GetParserRuleContext(), 4)) {
					panic(antlr.NewFailedPredicateException(p, "p.Precpred(p.GetParserRuleContext(), 4)", ""))
				}
				{
					p.SetState(171)
					p.Unary()
				}

			case 3:
				localctx = NewFunctionCallContext(p, NewExprContext(p, _parentctx, _parentState))
				p.PushNewRecursionContext(localctx, _startState, ParadiseParserRULE_expr)
				p.SetState(172)

				if !(p.Precpred(p.GetParserRuleContext(), 3)) {
					panic(antlr.NewFailedPredicateException(p, "p.Precpred(p.GetParserRuleContext(), 3)", ""))
				}
				{
					p.SetState(173)
					p.Match(ParadiseParserLBRACE)
				}
				{
					p.SetState(174)
					p.ExprList()
				}
				{
					p.SetState(175)
					p.Match(ParadiseParserRBRACE)
				}

			}

		}
		p.SetState(181)
		p.GetErrorHandler().Sync(p)
		_alt = p.GetInterpreter().AdaptivePredict(p.GetTokenStream(), 11, p.GetParserRuleContext())
	}

	return localctx
}

// IConditionContext is an interface to support dynamic dispatch.
type IConditionContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// IsConditionContext differentiates from other interfaces.
	IsConditionContext()
}

type ConditionContext struct {
	*antlr.BaseParserRuleContext
	parser antlr.Parser
}

func NewEmptyConditionContext() *ConditionContext {
	var p = new(ConditionContext)
	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(nil, -1)
	p.RuleIndex = ParadiseParserRULE_condition
	return p
}

func (*ConditionContext) IsConditionContext() {}

func NewConditionContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *ConditionContext {
	var p = new(ConditionContext)

	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(parent, invokingState)

	p.parser = parser
	p.RuleIndex = ParadiseParserRULE_condition

	return p
}

func (s *ConditionContext) GetParser() antlr.Parser { return s.parser }

func (s *ConditionContext) Expr() IExprContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*IExprContext)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(IExprContext)
}

func (s *ConditionContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *ConditionContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *ConditionContext) EnterRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(ParadiseListener); ok {
		listenerT.EnterCondition(s)
	}
}

func (s *ConditionContext) ExitRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(ParadiseListener); ok {
		listenerT.ExitCondition(s)
	}
}

func (s *ConditionContext) Accept(visitor antlr.ParseTreeVisitor) interface{} {
	switch t := visitor.(type) {
	case ParadiseVisitor:
		return t.VisitCondition(s)

	default:
		return t.VisitChildren(s)
	}
}

func (p *ParadiseParser) Condition() (localctx IConditionContext) {
	localctx = NewConditionContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 30, ParadiseParserRULE_condition)

	defer func() {
		p.ExitRule()
	}()

	defer func() {
		if err := recover(); err != nil {
			if v, ok := err.(antlr.RecognitionException); ok {
				localctx.SetException(v)
				p.GetErrorHandler().ReportError(p, v)
				p.GetErrorHandler().Recover(p, v)
			} else {
				panic(err)
			}
		}
	}()

	p.EnterOuterAlt(localctx, 1)
	{
		p.SetState(182)
		p.expr(0)
	}

	return localctx
}

// IArgDefContext is an interface to support dynamic dispatch.
type IArgDefContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// IsArgDefContext differentiates from other interfaces.
	IsArgDefContext()
}

type ArgDefContext struct {
	*antlr.BaseParserRuleContext
	parser antlr.Parser
}

func NewEmptyArgDefContext() *ArgDefContext {
	var p = new(ArgDefContext)
	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(nil, -1)
	p.RuleIndex = ParadiseParserRULE_argDef
	return p
}

func (*ArgDefContext) IsArgDefContext() {}

func NewArgDefContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *ArgDefContext {
	var p = new(ArgDefContext)

	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(parent, invokingState)

	p.parser = parser
	p.RuleIndex = ParadiseParserRULE_argDef

	return p
}

func (s *ArgDefContext) GetParser() antlr.Parser { return s.parser }

func (s *ArgDefContext) Identifier() IIdentifierContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*IIdentifierContext)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(IIdentifierContext)
}

func (s *ArgDefContext) TypeRef() ITypeRefContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*ITypeRefContext)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(ITypeRefContext)
}

func (s *ArgDefContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *ArgDefContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *ArgDefContext) EnterRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(ParadiseListener); ok {
		listenerT.EnterArgDef(s)
	}
}

func (s *ArgDefContext) ExitRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(ParadiseListener); ok {
		listenerT.ExitArgDef(s)
	}
}

func (s *ArgDefContext) Accept(visitor antlr.ParseTreeVisitor) interface{} {
	switch t := visitor.(type) {
	case ParadiseVisitor:
		return t.VisitArgDef(s)

	default:
		return t.VisitChildren(s)
	}
}

func (p *ParadiseParser) ArgDef() (localctx IArgDefContext) {
	localctx = NewArgDefContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 32, ParadiseParserRULE_argDef)

	defer func() {
		p.ExitRule()
	}()

	defer func() {
		if err := recover(); err != nil {
			if v, ok := err.(antlr.RecognitionException); ok {
				localctx.SetException(v)
				p.GetErrorHandler().ReportError(p, v)
				p.GetErrorHandler().Recover(p, v)
			} else {
				panic(err)
			}
		}
	}()

	p.EnterOuterAlt(localctx, 1)
	{
		p.SetState(184)
		p.Identifier()
	}
	{
		p.SetState(185)
		p.Match(ParadiseParserT__0)
	}
	{
		p.SetState(186)
		p.TypeRef()
	}

	return localctx
}

// IArgDefListContext is an interface to support dynamic dispatch.
type IArgDefListContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// IsArgDefListContext differentiates from other interfaces.
	IsArgDefListContext()
}

type ArgDefListContext struct {
	*antlr.BaseParserRuleContext
	parser antlr.Parser
}

func NewEmptyArgDefListContext() *ArgDefListContext {
	var p = new(ArgDefListContext)
	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(nil, -1)
	p.RuleIndex = ParadiseParserRULE_argDefList
	return p
}

func (*ArgDefListContext) IsArgDefListContext() {}

func NewArgDefListContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *ArgDefListContext {
	var p = new(ArgDefListContext)

	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(parent, invokingState)

	p.parser = parser
	p.RuleIndex = ParadiseParserRULE_argDefList

	return p
}

func (s *ArgDefListContext) GetParser() antlr.Parser { return s.parser }

func (s *ArgDefListContext) AllArgDef() []IArgDefContext {
	var ts = s.GetTypedRuleContexts(reflect.TypeOf((*IArgDefContext)(nil)).Elem())
	var tst = make([]IArgDefContext, len(ts))

	for i, t := range ts {
		if t != nil {
			tst[i] = t.(IArgDefContext)
		}
	}

	return tst
}

func (s *ArgDefListContext) ArgDef(i int) IArgDefContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*IArgDefContext)(nil)).Elem(), i)

	if t == nil {
		return nil
	}

	return t.(IArgDefContext)
}

func (s *ArgDefListContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *ArgDefListContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *ArgDefListContext) EnterRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(ParadiseListener); ok {
		listenerT.EnterArgDefList(s)
	}
}

func (s *ArgDefListContext) ExitRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(ParadiseListener); ok {
		listenerT.ExitArgDefList(s)
	}
}

func (s *ArgDefListContext) Accept(visitor antlr.ParseTreeVisitor) interface{} {
	switch t := visitor.(type) {
	case ParadiseVisitor:
		return t.VisitArgDefList(s)

	default:
		return t.VisitChildren(s)
	}
}

func (p *ParadiseParser) ArgDefList() (localctx IArgDefListContext) {
	localctx = NewArgDefListContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 34, ParadiseParserRULE_argDefList)
	var _la int

	defer func() {
		p.ExitRule()
	}()

	defer func() {
		if err := recover(); err != nil {
			if v, ok := err.(antlr.RecognitionException); ok {
				localctx.SetException(v)
				p.GetErrorHandler().ReportError(p, v)
				p.GetErrorHandler().Recover(p, v)
			} else {
				panic(err)
			}
		}
	}()

	p.EnterOuterAlt(localctx, 1)
	p.SetState(196)
	p.GetErrorHandler().Sync(p)
	_la = p.GetTokenStream().LA(1)

	if _la == ParadiseParserIDENTIFIER {
		{
			p.SetState(188)
			p.ArgDef()
		}
		p.SetState(193)
		p.GetErrorHandler().Sync(p)
		_la = p.GetTokenStream().LA(1)

		for _la == ParadiseParserT__22 {
			{
				p.SetState(189)
				p.Match(ParadiseParserT__22)
			}
			{
				p.SetState(190)
				p.ArgDef()
			}

			p.SetState(195)
			p.GetErrorHandler().Sync(p)
			_la = p.GetTokenStream().LA(1)
		}

	}

	return localctx
}

// IExprListContext is an interface to support dynamic dispatch.
type IExprListContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// IsExprListContext differentiates from other interfaces.
	IsExprListContext()
}

type ExprListContext struct {
	*antlr.BaseParserRuleContext
	parser antlr.Parser
}

func NewEmptyExprListContext() *ExprListContext {
	var p = new(ExprListContext)
	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(nil, -1)
	p.RuleIndex = ParadiseParserRULE_exprList
	return p
}

func (*ExprListContext) IsExprListContext() {}

func NewExprListContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *ExprListContext {
	var p = new(ExprListContext)

	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(parent, invokingState)

	p.parser = parser
	p.RuleIndex = ParadiseParserRULE_exprList

	return p
}

func (s *ExprListContext) GetParser() antlr.Parser { return s.parser }

func (s *ExprListContext) AllExpr() []IExprContext {
	var ts = s.GetTypedRuleContexts(reflect.TypeOf((*IExprContext)(nil)).Elem())
	var tst = make([]IExprContext, len(ts))

	for i, t := range ts {
		if t != nil {
			tst[i] = t.(IExprContext)
		}
	}

	return tst
}

func (s *ExprListContext) Expr(i int) IExprContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*IExprContext)(nil)).Elem(), i)

	if t == nil {
		return nil
	}

	return t.(IExprContext)
}

func (s *ExprListContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *ExprListContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *ExprListContext) EnterRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(ParadiseListener); ok {
		listenerT.EnterExprList(s)
	}
}

func (s *ExprListContext) ExitRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(ParadiseListener); ok {
		listenerT.ExitExprList(s)
	}
}

func (s *ExprListContext) Accept(visitor antlr.ParseTreeVisitor) interface{} {
	switch t := visitor.(type) {
	case ParadiseVisitor:
		return t.VisitExprList(s)

	default:
		return t.VisitChildren(s)
	}
}

func (p *ParadiseParser) ExprList() (localctx IExprListContext) {
	localctx = NewExprListContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 36, ParadiseParserRULE_exprList)
	var _la int

	defer func() {
		p.ExitRule()
	}()

	defer func() {
		if err := recover(); err != nil {
			if v, ok := err.(antlr.RecognitionException); ok {
				localctx.SetException(v)
				p.GetErrorHandler().ReportError(p, v)
				p.GetErrorHandler().Recover(p, v)
			} else {
				panic(err)
			}
		}
	}()

	p.EnterOuterAlt(localctx, 1)
	p.SetState(206)
	p.GetErrorHandler().Sync(p)
	_la = p.GetTokenStream().LA(1)

	if ((_la-24)&-(0x1f+1)) == 0 && ((1<<uint((_la-24)))&((1<<(ParadiseParserT__23-24))|(1<<(ParadiseParserT__24-24))|(1<<(ParadiseParserT__37-24))|(1<<(ParadiseParserT__38-24))|(1<<(ParadiseParserT__39-24))|(1<<(ParadiseParserLBRACE-24))|(1<<(ParadiseParserCHAR-24))|(1<<(ParadiseParserSTRING-24))|(1<<(ParadiseParserHEX-24))|(1<<(ParadiseParserBITS-24))|(1<<(ParadiseParserDEC-24))|(1<<(ParadiseParserBOOL-24))|(1<<(ParadiseParserIDENTIFIER-24)))) != 0 {
		{
			p.SetState(198)
			p.expr(0)
		}
		p.SetState(203)
		p.GetErrorHandler().Sync(p)
		_la = p.GetTokenStream().LA(1)

		for _la == ParadiseParserT__22 {
			{
				p.SetState(199)
				p.Match(ParadiseParserT__22)
			}
			{
				p.SetState(200)
				p.expr(0)
			}

			p.SetState(205)
			p.GetErrorHandler().Sync(p)
			_la = p.GetTokenStream().LA(1)
		}

	}

	return localctx
}

// IIdentifierContext is an interface to support dynamic dispatch.
type IIdentifierContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// IsIdentifierContext differentiates from other interfaces.
	IsIdentifierContext()
}

type IdentifierContext struct {
	*antlr.BaseParserRuleContext
	parser antlr.Parser
}

func NewEmptyIdentifierContext() *IdentifierContext {
	var p = new(IdentifierContext)
	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(nil, -1)
	p.RuleIndex = ParadiseParserRULE_identifier
	return p
}

func (*IdentifierContext) IsIdentifierContext() {}

func NewIdentifierContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *IdentifierContext {
	var p = new(IdentifierContext)

	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(parent, invokingState)

	p.parser = parser
	p.RuleIndex = ParadiseParserRULE_identifier

	return p
}

func (s *IdentifierContext) GetParser() antlr.Parser { return s.parser }

func (s *IdentifierContext) IDENTIFIER() antlr.TerminalNode {
	return s.GetToken(ParadiseParserIDENTIFIER, 0)
}

func (s *IdentifierContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *IdentifierContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *IdentifierContext) EnterRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(ParadiseListener); ok {
		listenerT.EnterIdentifier(s)
	}
}

func (s *IdentifierContext) ExitRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(ParadiseListener); ok {
		listenerT.ExitIdentifier(s)
	}
}

func (s *IdentifierContext) Accept(visitor antlr.ParseTreeVisitor) interface{} {
	switch t := visitor.(type) {
	case ParadiseVisitor:
		return t.VisitIdentifier(s)

	default:
		return t.VisitChildren(s)
	}
}

func (p *ParadiseParser) Identifier() (localctx IIdentifierContext) {
	localctx = NewIdentifierContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 38, ParadiseParserRULE_identifier)

	defer func() {
		p.ExitRule()
	}()

	defer func() {
		if err := recover(); err != nil {
			if v, ok := err.(antlr.RecognitionException); ok {
				localctx.SetException(v)
				p.GetErrorHandler().ReportError(p, v)
				p.GetErrorHandler().Recover(p, v)
			} else {
				panic(err)
			}
		}
	}()

	p.EnterOuterAlt(localctx, 1)
	{
		p.SetState(208)
		p.Match(ParadiseParserIDENTIFIER)
	}

	return localctx
}

// IBinaryContext is an interface to support dynamic dispatch.
type IBinaryContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// IsBinaryContext differentiates from other interfaces.
	IsBinaryContext()
}

type BinaryContext struct {
	*antlr.BaseParserRuleContext
	parser antlr.Parser
}

func NewEmptyBinaryContext() *BinaryContext {
	var p = new(BinaryContext)
	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(nil, -1)
	p.RuleIndex = ParadiseParserRULE_binary
	return p
}

func (*BinaryContext) IsBinaryContext() {}

func NewBinaryContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *BinaryContext {
	var p = new(BinaryContext)

	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(parent, invokingState)

	p.parser = parser
	p.RuleIndex = ParadiseParserRULE_binary

	return p
}

func (s *BinaryContext) GetParser() antlr.Parser { return s.parser }

func (s *BinaryContext) CopyFrom(ctx *BinaryContext) {
	s.BaseParserRuleContext.CopyFrom(ctx.BaseParserRuleContext)
}

func (s *BinaryContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *BinaryContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

type EqBinaryContext struct {
	*BinaryContext
}

func NewEqBinaryContext(parser antlr.Parser, ctx antlr.ParserRuleContext) *EqBinaryContext {
	var p = new(EqBinaryContext)

	p.BinaryContext = NewEmptyBinaryContext()
	p.parser = parser
	p.CopyFrom(ctx.(*BinaryContext))

	return p
}

func (s *EqBinaryContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *EqBinaryContext) EnterRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(ParadiseListener); ok {
		listenerT.EnterEqBinary(s)
	}
}

func (s *EqBinaryContext) ExitRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(ParadiseListener); ok {
		listenerT.ExitEqBinary(s)
	}
}

func (s *EqBinaryContext) Accept(visitor antlr.ParseTreeVisitor) interface{} {
	switch t := visitor.(type) {
	case ParadiseVisitor:
		return t.VisitEqBinary(s)

	default:
		return t.VisitChildren(s)
	}
}

type ModBinaryContext struct {
	*BinaryContext
}

func NewModBinaryContext(parser antlr.Parser, ctx antlr.ParserRuleContext) *ModBinaryContext {
	var p = new(ModBinaryContext)

	p.BinaryContext = NewEmptyBinaryContext()
	p.parser = parser
	p.CopyFrom(ctx.(*BinaryContext))

	return p
}

func (s *ModBinaryContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *ModBinaryContext) EnterRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(ParadiseListener); ok {
		listenerT.EnterModBinary(s)
	}
}

func (s *ModBinaryContext) ExitRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(ParadiseListener); ok {
		listenerT.ExitModBinary(s)
	}
}

func (s *ModBinaryContext) Accept(visitor antlr.ParseTreeVisitor) interface{} {
	switch t := visitor.(type) {
	case ParadiseVisitor:
		return t.VisitModBinary(s)

	default:
		return t.VisitChildren(s)
	}
}

type ByteOrBinaryContext struct {
	*BinaryContext
}

func NewByteOrBinaryContext(parser antlr.Parser, ctx antlr.ParserRuleContext) *ByteOrBinaryContext {
	var p = new(ByteOrBinaryContext)

	p.BinaryContext = NewEmptyBinaryContext()
	p.parser = parser
	p.CopyFrom(ctx.(*BinaryContext))

	return p
}

func (s *ByteOrBinaryContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *ByteOrBinaryContext) EnterRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(ParadiseListener); ok {
		listenerT.EnterByteOrBinary(s)
	}
}

func (s *ByteOrBinaryContext) ExitRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(ParadiseListener); ok {
		listenerT.ExitByteOrBinary(s)
	}
}

func (s *ByteOrBinaryContext) Accept(visitor antlr.ParseTreeVisitor) interface{} {
	switch t := visitor.(type) {
	case ParadiseVisitor:
		return t.VisitByteOrBinary(s)

	default:
		return t.VisitChildren(s)
	}
}

type LtBinaryContext struct {
	*BinaryContext
}

func NewLtBinaryContext(parser antlr.Parser, ctx antlr.ParserRuleContext) *LtBinaryContext {
	var p = new(LtBinaryContext)

	p.BinaryContext = NewEmptyBinaryContext()
	p.parser = parser
	p.CopyFrom(ctx.(*BinaryContext))

	return p
}

func (s *LtBinaryContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *LtBinaryContext) EnterRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(ParadiseListener); ok {
		listenerT.EnterLtBinary(s)
	}
}

func (s *LtBinaryContext) ExitRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(ParadiseListener); ok {
		listenerT.ExitLtBinary(s)
	}
}

func (s *LtBinaryContext) Accept(visitor antlr.ParseTreeVisitor) interface{} {
	switch t := visitor.(type) {
	case ParadiseVisitor:
		return t.VisitLtBinary(s)

	default:
		return t.VisitChildren(s)
	}
}

type GtBinaryContext struct {
	*BinaryContext
}

func NewGtBinaryContext(parser antlr.Parser, ctx antlr.ParserRuleContext) *GtBinaryContext {
	var p = new(GtBinaryContext)

	p.BinaryContext = NewEmptyBinaryContext()
	p.parser = parser
	p.CopyFrom(ctx.(*BinaryContext))

	return p
}

func (s *GtBinaryContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *GtBinaryContext) EnterRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(ParadiseListener); ok {
		listenerT.EnterGtBinary(s)
	}
}

func (s *GtBinaryContext) ExitRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(ParadiseListener); ok {
		listenerT.ExitGtBinary(s)
	}
}

func (s *GtBinaryContext) Accept(visitor antlr.ParseTreeVisitor) interface{} {
	switch t := visitor.(type) {
	case ParadiseVisitor:
		return t.VisitGtBinary(s)

	default:
		return t.VisitChildren(s)
	}
}

type DivBinaryContext struct {
	*BinaryContext
}

func NewDivBinaryContext(parser antlr.Parser, ctx antlr.ParserRuleContext) *DivBinaryContext {
	var p = new(DivBinaryContext)

	p.BinaryContext = NewEmptyBinaryContext()
	p.parser = parser
	p.CopyFrom(ctx.(*BinaryContext))

	return p
}

func (s *DivBinaryContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *DivBinaryContext) EnterRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(ParadiseListener); ok {
		listenerT.EnterDivBinary(s)
	}
}

func (s *DivBinaryContext) ExitRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(ParadiseListener); ok {
		listenerT.ExitDivBinary(s)
	}
}

func (s *DivBinaryContext) Accept(visitor antlr.ParseTreeVisitor) interface{} {
	switch t := visitor.(type) {
	case ParadiseVisitor:
		return t.VisitDivBinary(s)

	default:
		return t.VisitChildren(s)
	}
}

type AndBinaryContext struct {
	*BinaryContext
}

func NewAndBinaryContext(parser antlr.Parser, ctx antlr.ParserRuleContext) *AndBinaryContext {
	var p = new(AndBinaryContext)

	p.BinaryContext = NewEmptyBinaryContext()
	p.parser = parser
	p.CopyFrom(ctx.(*BinaryContext))

	return p
}

func (s *AndBinaryContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *AndBinaryContext) EnterRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(ParadiseListener); ok {
		listenerT.EnterAndBinary(s)
	}
}

func (s *AndBinaryContext) ExitRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(ParadiseListener); ok {
		listenerT.ExitAndBinary(s)
	}
}

func (s *AndBinaryContext) Accept(visitor antlr.ParseTreeVisitor) interface{} {
	switch t := visitor.(type) {
	case ParadiseVisitor:
		return t.VisitAndBinary(s)

	default:
		return t.VisitChildren(s)
	}
}

type ByteAndBinaryContext struct {
	*BinaryContext
}

func NewByteAndBinaryContext(parser antlr.Parser, ctx antlr.ParserRuleContext) *ByteAndBinaryContext {
	var p = new(ByteAndBinaryContext)

	p.BinaryContext = NewEmptyBinaryContext()
	p.parser = parser
	p.CopyFrom(ctx.(*BinaryContext))

	return p
}

func (s *ByteAndBinaryContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *ByteAndBinaryContext) EnterRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(ParadiseListener); ok {
		listenerT.EnterByteAndBinary(s)
	}
}

func (s *ByteAndBinaryContext) ExitRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(ParadiseListener); ok {
		listenerT.ExitByteAndBinary(s)
	}
}

func (s *ByteAndBinaryContext) Accept(visitor antlr.ParseTreeVisitor) interface{} {
	switch t := visitor.(type) {
	case ParadiseVisitor:
		return t.VisitByteAndBinary(s)

	default:
		return t.VisitChildren(s)
	}
}

type SubBinaryContext struct {
	*BinaryContext
}

func NewSubBinaryContext(parser antlr.Parser, ctx antlr.ParserRuleContext) *SubBinaryContext {
	var p = new(SubBinaryContext)

	p.BinaryContext = NewEmptyBinaryContext()
	p.parser = parser
	p.CopyFrom(ctx.(*BinaryContext))

	return p
}

func (s *SubBinaryContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *SubBinaryContext) EnterRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(ParadiseListener); ok {
		listenerT.EnterSubBinary(s)
	}
}

func (s *SubBinaryContext) ExitRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(ParadiseListener); ok {
		listenerT.ExitSubBinary(s)
	}
}

func (s *SubBinaryContext) Accept(visitor antlr.ParseTreeVisitor) interface{} {
	switch t := visitor.(type) {
	case ParadiseVisitor:
		return t.VisitSubBinary(s)

	default:
		return t.VisitChildren(s)
	}
}

type SumBinaryContext struct {
	*BinaryContext
}

func NewSumBinaryContext(parser antlr.Parser, ctx antlr.ParserRuleContext) *SumBinaryContext {
	var p = new(SumBinaryContext)

	p.BinaryContext = NewEmptyBinaryContext()
	p.parser = parser
	p.CopyFrom(ctx.(*BinaryContext))

	return p
}

func (s *SumBinaryContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *SumBinaryContext) EnterRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(ParadiseListener); ok {
		listenerT.EnterSumBinary(s)
	}
}

func (s *SumBinaryContext) ExitRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(ParadiseListener); ok {
		listenerT.ExitSumBinary(s)
	}
}

func (s *SumBinaryContext) Accept(visitor antlr.ParseTreeVisitor) interface{} {
	switch t := visitor.(type) {
	case ParadiseVisitor:
		return t.VisitSumBinary(s)

	default:
		return t.VisitChildren(s)
	}
}

type OrBinaryContext struct {
	*BinaryContext
}

func NewOrBinaryContext(parser antlr.Parser, ctx antlr.ParserRuleContext) *OrBinaryContext {
	var p = new(OrBinaryContext)

	p.BinaryContext = NewEmptyBinaryContext()
	p.parser = parser
	p.CopyFrom(ctx.(*BinaryContext))

	return p
}

func (s *OrBinaryContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *OrBinaryContext) EnterRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(ParadiseListener); ok {
		listenerT.EnterOrBinary(s)
	}
}

func (s *OrBinaryContext) ExitRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(ParadiseListener); ok {
		listenerT.ExitOrBinary(s)
	}
}

func (s *OrBinaryContext) Accept(visitor antlr.ParseTreeVisitor) interface{} {
	switch t := visitor.(type) {
	case ParadiseVisitor:
		return t.VisitOrBinary(s)

	default:
		return t.VisitChildren(s)
	}
}

type LtOrEtBinaryContext struct {
	*BinaryContext
}

func NewLtOrEtBinaryContext(parser antlr.Parser, ctx antlr.ParserRuleContext) *LtOrEtBinaryContext {
	var p = new(LtOrEtBinaryContext)

	p.BinaryContext = NewEmptyBinaryContext()
	p.parser = parser
	p.CopyFrom(ctx.(*BinaryContext))

	return p
}

func (s *LtOrEtBinaryContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *LtOrEtBinaryContext) EnterRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(ParadiseListener); ok {
		listenerT.EnterLtOrEtBinary(s)
	}
}

func (s *LtOrEtBinaryContext) ExitRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(ParadiseListener); ok {
		listenerT.ExitLtOrEtBinary(s)
	}
}

func (s *LtOrEtBinaryContext) Accept(visitor antlr.ParseTreeVisitor) interface{} {
	switch t := visitor.(type) {
	case ParadiseVisitor:
		return t.VisitLtOrEtBinary(s)

	default:
		return t.VisitChildren(s)
	}
}

type MulBinaryContext struct {
	*BinaryContext
}

func NewMulBinaryContext(parser antlr.Parser, ctx antlr.ParserRuleContext) *MulBinaryContext {
	var p = new(MulBinaryContext)

	p.BinaryContext = NewEmptyBinaryContext()
	p.parser = parser
	p.CopyFrom(ctx.(*BinaryContext))

	return p
}

func (s *MulBinaryContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *MulBinaryContext) EnterRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(ParadiseListener); ok {
		listenerT.EnterMulBinary(s)
	}
}

func (s *MulBinaryContext) ExitRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(ParadiseListener); ok {
		listenerT.ExitMulBinary(s)
	}
}

func (s *MulBinaryContext) Accept(visitor antlr.ParseTreeVisitor) interface{} {
	switch t := visitor.(type) {
	case ParadiseVisitor:
		return t.VisitMulBinary(s)

	default:
		return t.VisitChildren(s)
	}
}

func (p *ParadiseParser) Binary() (localctx IBinaryContext) {
	localctx = NewBinaryContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 40, ParadiseParserRULE_binary)

	defer func() {
		p.ExitRule()
	}()

	defer func() {
		if err := recover(); err != nil {
			if v, ok := err.(antlr.RecognitionException); ok {
				localctx.SetException(v)
				p.GetErrorHandler().ReportError(p, v)
				p.GetErrorHandler().Recover(p, v)
			} else {
				panic(err)
			}
		}
	}()

	p.SetState(224)
	p.GetErrorHandler().Sync(p)

	switch p.GetTokenStream().LA(1) {
	case ParadiseParserT__23:
		localctx = NewSumBinaryContext(p, localctx)
		p.EnterOuterAlt(localctx, 1)
		{
			p.SetState(210)
			p.Match(ParadiseParserT__23)
		}

	case ParadiseParserT__24:
		localctx = NewSubBinaryContext(p, localctx)
		p.EnterOuterAlt(localctx, 2)
		{
			p.SetState(211)
			p.Match(ParadiseParserT__24)
		}

	case ParadiseParserT__25:
		localctx = NewDivBinaryContext(p, localctx)
		p.EnterOuterAlt(localctx, 3)
		{
			p.SetState(212)
			p.Match(ParadiseParserT__25)
		}

	case ParadiseParserT__26:
		localctx = NewMulBinaryContext(p, localctx)
		p.EnterOuterAlt(localctx, 4)
		{
			p.SetState(213)
			p.Match(ParadiseParserT__26)
		}

	case ParadiseParserT__27:
		localctx = NewByteAndBinaryContext(p, localctx)
		p.EnterOuterAlt(localctx, 5)
		{
			p.SetState(214)
			p.Match(ParadiseParserT__27)
		}

	case ParadiseParserT__28:
		localctx = NewAndBinaryContext(p, localctx)
		p.EnterOuterAlt(localctx, 6)
		{
			p.SetState(215)
			p.Match(ParadiseParserT__28)
		}

	case ParadiseParserT__29:
		localctx = NewByteOrBinaryContext(p, localctx)
		p.EnterOuterAlt(localctx, 7)
		{
			p.SetState(216)
			p.Match(ParadiseParserT__29)
		}

	case ParadiseParserT__30:
		localctx = NewOrBinaryContext(p, localctx)
		p.EnterOuterAlt(localctx, 8)
		{
			p.SetState(217)
			p.Match(ParadiseParserT__30)
		}

	case ParadiseParserT__31:
		localctx = NewEqBinaryContext(p, localctx)
		p.EnterOuterAlt(localctx, 9)
		{
			p.SetState(218)
			p.Match(ParadiseParserT__31)
		}

	case ParadiseParserT__32:
		localctx = NewLtBinaryContext(p, localctx)
		p.EnterOuterAlt(localctx, 10)
		{
			p.SetState(219)
			p.Match(ParadiseParserT__32)
		}

	case ParadiseParserT__33:
		localctx = NewLtOrEtBinaryContext(p, localctx)
		p.EnterOuterAlt(localctx, 11)
		{
			p.SetState(220)
			p.Match(ParadiseParserT__33)
		}

	case ParadiseParserT__34:
		localctx = NewGtBinaryContext(p, localctx)
		p.EnterOuterAlt(localctx, 12)
		{
			p.SetState(221)
			p.Match(ParadiseParserT__34)
		}

	case ParadiseParserT__35:
		localctx = NewLtOrEtBinaryContext(p, localctx)
		p.EnterOuterAlt(localctx, 13)
		{
			p.SetState(222)
			p.Match(ParadiseParserT__35)
		}

	case ParadiseParserT__36:
		localctx = NewModBinaryContext(p, localctx)
		p.EnterOuterAlt(localctx, 14)
		{
			p.SetState(223)
			p.Match(ParadiseParserT__36)
		}

	default:
		panic(antlr.NewNoViableAltException(p, nil, nil, nil, nil, nil))
	}

	return localctx
}

// IUnaryContext is an interface to support dynamic dispatch.
type IUnaryContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// IsUnaryContext differentiates from other interfaces.
	IsUnaryContext()
}

type UnaryContext struct {
	*antlr.BaseParserRuleContext
	parser antlr.Parser
}

func NewEmptyUnaryContext() *UnaryContext {
	var p = new(UnaryContext)
	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(nil, -1)
	p.RuleIndex = ParadiseParserRULE_unary
	return p
}

func (*UnaryContext) IsUnaryContext() {}

func NewUnaryContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *UnaryContext {
	var p = new(UnaryContext)

	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(parent, invokingState)

	p.parser = parser
	p.RuleIndex = ParadiseParserRULE_unary

	return p
}

func (s *UnaryContext) GetParser() antlr.Parser { return s.parser }

func (s *UnaryContext) CopyFrom(ctx *UnaryContext) {
	s.BaseParserRuleContext.CopyFrom(ctx.BaseParserRuleContext)
}

func (s *UnaryContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *UnaryContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

type DecrementUnaryContext struct {
	*UnaryContext
}

func NewDecrementUnaryContext(parser antlr.Parser, ctx antlr.ParserRuleContext) *DecrementUnaryContext {
	var p = new(DecrementUnaryContext)

	p.UnaryContext = NewEmptyUnaryContext()
	p.parser = parser
	p.CopyFrom(ctx.(*UnaryContext))

	return p
}

func (s *DecrementUnaryContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *DecrementUnaryContext) EnterRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(ParadiseListener); ok {
		listenerT.EnterDecrementUnary(s)
	}
}

func (s *DecrementUnaryContext) ExitRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(ParadiseListener); ok {
		listenerT.ExitDecrementUnary(s)
	}
}

func (s *DecrementUnaryContext) Accept(visitor antlr.ParseTreeVisitor) interface{} {
	switch t := visitor.(type) {
	case ParadiseVisitor:
		return t.VisitDecrementUnary(s)

	default:
		return t.VisitChildren(s)
	}
}

type SumUnaryContext struct {
	*UnaryContext
}

func NewSumUnaryContext(parser antlr.Parser, ctx antlr.ParserRuleContext) *SumUnaryContext {
	var p = new(SumUnaryContext)

	p.UnaryContext = NewEmptyUnaryContext()
	p.parser = parser
	p.CopyFrom(ctx.(*UnaryContext))

	return p
}

func (s *SumUnaryContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *SumUnaryContext) EnterRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(ParadiseListener); ok {
		listenerT.EnterSumUnary(s)
	}
}

func (s *SumUnaryContext) ExitRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(ParadiseListener); ok {
		listenerT.ExitSumUnary(s)
	}
}

func (s *SumUnaryContext) Accept(visitor antlr.ParseTreeVisitor) interface{} {
	switch t := visitor.(type) {
	case ParadiseVisitor:
		return t.VisitSumUnary(s)

	default:
		return t.VisitChildren(s)
	}
}

type SubUnaryContext struct {
	*UnaryContext
}

func NewSubUnaryContext(parser antlr.Parser, ctx antlr.ParserRuleContext) *SubUnaryContext {
	var p = new(SubUnaryContext)

	p.UnaryContext = NewEmptyUnaryContext()
	p.parser = parser
	p.CopyFrom(ctx.(*UnaryContext))

	return p
}

func (s *SubUnaryContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *SubUnaryContext) EnterRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(ParadiseListener); ok {
		listenerT.EnterSubUnary(s)
	}
}

func (s *SubUnaryContext) ExitRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(ParadiseListener); ok {
		listenerT.ExitSubUnary(s)
	}
}

func (s *SubUnaryContext) Accept(visitor antlr.ParseTreeVisitor) interface{} {
	switch t := visitor.(type) {
	case ParadiseVisitor:
		return t.VisitSubUnary(s)

	default:
		return t.VisitChildren(s)
	}
}

type IncrementUnaryContext struct {
	*UnaryContext
}

func NewIncrementUnaryContext(parser antlr.Parser, ctx antlr.ParserRuleContext) *IncrementUnaryContext {
	var p = new(IncrementUnaryContext)

	p.UnaryContext = NewEmptyUnaryContext()
	p.parser = parser
	p.CopyFrom(ctx.(*UnaryContext))

	return p
}

func (s *IncrementUnaryContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *IncrementUnaryContext) EnterRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(ParadiseListener); ok {
		listenerT.EnterIncrementUnary(s)
	}
}

func (s *IncrementUnaryContext) ExitRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(ParadiseListener); ok {
		listenerT.ExitIncrementUnary(s)
	}
}

func (s *IncrementUnaryContext) Accept(visitor antlr.ParseTreeVisitor) interface{} {
	switch t := visitor.(type) {
	case ParadiseVisitor:
		return t.VisitIncrementUnary(s)

	default:
		return t.VisitChildren(s)
	}
}

type NotUnaryContext struct {
	*UnaryContext
}

func NewNotUnaryContext(parser antlr.Parser, ctx antlr.ParserRuleContext) *NotUnaryContext {
	var p = new(NotUnaryContext)

	p.UnaryContext = NewEmptyUnaryContext()
	p.parser = parser
	p.CopyFrom(ctx.(*UnaryContext))

	return p
}

func (s *NotUnaryContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *NotUnaryContext) EnterRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(ParadiseListener); ok {
		listenerT.EnterNotUnary(s)
	}
}

func (s *NotUnaryContext) ExitRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(ParadiseListener); ok {
		listenerT.ExitNotUnary(s)
	}
}

func (s *NotUnaryContext) Accept(visitor antlr.ParseTreeVisitor) interface{} {
	switch t := visitor.(type) {
	case ParadiseVisitor:
		return t.VisitNotUnary(s)

	default:
		return t.VisitChildren(s)
	}
}

func (p *ParadiseParser) Unary() (localctx IUnaryContext) {
	localctx = NewUnaryContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 42, ParadiseParserRULE_unary)

	defer func() {
		p.ExitRule()
	}()

	defer func() {
		if err := recover(); err != nil {
			if v, ok := err.(antlr.RecognitionException); ok {
				localctx.SetException(v)
				p.GetErrorHandler().ReportError(p, v)
				p.GetErrorHandler().Recover(p, v)
			} else {
				panic(err)
			}
		}
	}()

	p.SetState(231)
	p.GetErrorHandler().Sync(p)

	switch p.GetTokenStream().LA(1) {
	case ParadiseParserT__23:
		localctx = NewSumUnaryContext(p, localctx)
		p.EnterOuterAlt(localctx, 1)
		{
			p.SetState(226)
			p.Match(ParadiseParserT__23)
		}

	case ParadiseParserT__24:
		localctx = NewSubUnaryContext(p, localctx)
		p.EnterOuterAlt(localctx, 2)
		{
			p.SetState(227)
			p.Match(ParadiseParserT__24)
		}

	case ParadiseParserT__37:
		localctx = NewNotUnaryContext(p, localctx)
		p.EnterOuterAlt(localctx, 3)
		{
			p.SetState(228)
			p.Match(ParadiseParserT__37)
		}

	case ParadiseParserT__38:
		localctx = NewDecrementUnaryContext(p, localctx)
		p.EnterOuterAlt(localctx, 4)
		{
			p.SetState(229)
			p.Match(ParadiseParserT__38)
		}

	case ParadiseParserT__39:
		localctx = NewIncrementUnaryContext(p, localctx)
		p.EnterOuterAlt(localctx, 5)
		{
			p.SetState(230)
			p.Match(ParadiseParserT__39)
		}

	default:
		panic(antlr.NewNoViableAltException(p, nil, nil, nil, nil, nil))
	}

	return localctx
}

func (p *ParadiseParser) Sempred(localctx antlr.RuleContext, ruleIndex, predIndex int) bool {
	switch ruleIndex {
	case 14:
		var t *ExprContext = nil
		if localctx != nil {
			t = localctx.(*ExprContext)
		}
		return p.Expr_Sempred(t, predIndex)

	default:
		panic("No predicate with index: " + fmt.Sprint(ruleIndex))
	}
}

func (p *ParadiseParser) Expr_Sempred(localctx antlr.RuleContext, predIndex int) bool {
	switch predIndex {
	case 0:
		return p.Precpred(p.GetParserRuleContext(), 7)

	case 1:
		return p.Precpred(p.GetParserRuleContext(), 4)

	case 2:
		return p.Precpred(p.GetParserRuleContext(), 3)

	default:
		panic("No predicate with index: " + fmt.Sprint(predIndex))
	}
}
