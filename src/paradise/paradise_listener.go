// Generated from /Users/tva/ITMO_old/program_models_verification/Paradise.g4 by ANTLR 4.7.

package paradise // Paradise
import "github.com/antlr/antlr4/runtime/Go/antlr"

// ParadiseListener is a complete listener for a parse tree produced by ParadiseParser.
type ParadiseListener interface {
	antlr.ParseTreeListener

	// EnterSource is called when entering the source production.
	EnterSource(c *SourceContext)

	// EnterSourceItem is called when entering the sourceItem production.
	EnterSourceItem(c *SourceItemContext)

	// EnterFunctionBody is called when entering the functionBody production.
	EnterFunctionBody(c *FunctionBodyContext)

	// EnterFunctionSignature is called when entering the functionSignature production.
	EnterFunctionSignature(c *FunctionSignatureContext)

	// EnterFunctionArgs is called when entering the functionArgs production.
	EnterFunctionArgs(c *FunctionArgsContext)

	// EnterTypeRef is called when entering the typeRef production.
	EnterTypeRef(c *TypeRefContext)

	// EnterIfThenStmnt is called when entering the IfThenStmnt production.
	EnterIfThenStmnt(c *IfThenStmntContext)

	// EnterIfThenElseStmnt is called when entering the IfThenElseStmnt production.
	EnterIfThenElseStmnt(c *IfThenElseStmntContext)

	// EnterWhileStmnt is called when entering the WhileStmnt production.
	EnterWhileStmnt(c *WhileStmntContext)

	// EnterDoWhileStmnt is called when entering the DoWhileStmnt production.
	EnterDoWhileStmnt(c *DoWhileStmntContext)

	// EnterBreakStmnt is called when entering the BreakStmnt production.
	EnterBreakStmnt(c *BreakStmntContext)

	// EnterIdentifierStmnt is called when entering the IdentifierStmnt production.
	EnterIdentifierStmnt(c *IdentifierStmntContext)

	// EnterExprStmnt is called when entering the ExprStmnt production.
	EnterExprStmnt(c *ExprStmntContext)

	// EnterReturnStmnt is called when entering the ReturnStmnt production.
	EnterReturnStmnt(c *ReturnStmntContext)

	// EnterNestedFunctionDeclaration is called when entering the NestedFunctionDeclaration production.
	EnterNestedFunctionDeclaration(c *NestedFunctionDeclarationContext)

	// EnterWhileCondition is called when entering the whileCondition production.
	EnterWhileCondition(c *WhileConditionContext)

	// EnterWhileBody is called when entering the whileBody production.
	EnterWhileBody(c *WhileBodyContext)

	// EnterDoWhileBody is called when entering the doWhileBody production.
	EnterDoWhileBody(c *DoWhileBodyContext)

	// EnterDoWhileCondition is called when entering the doWhileCondition production.
	EnterDoWhileCondition(c *DoWhileConditionContext)

	// EnterIfCondition is called when entering the ifCondition production.
	EnterIfCondition(c *IfConditionContext)

	// EnterIfTrueStatement is called when entering the ifTrueStatement production.
	EnterIfTrueStatement(c *IfTrueStatementContext)

	// EnterIfFalseStatement is called when entering the ifFalseStatement production.
	EnterIfFalseStatement(c *IfFalseStatementContext)

	// EnterPrefixUnaryExpr is called when entering the PrefixUnaryExpr production.
	EnterPrefixUnaryExpr(c *PrefixUnaryExprContext)

	// EnterValueExpr is called when entering the ValueExpr production.
	EnterValueExpr(c *ValueExprContext)

	// EnterVariable is called when entering the Variable production.
	EnterVariable(c *VariableContext)

	// EnterBinaryExpr is called when entering the BinaryExpr production.
	EnterBinaryExpr(c *BinaryExprContext)

	// EnterBracedExpr is called when entering the BracedExpr production.
	EnterBracedExpr(c *BracedExprContext)

	// EnterPostfixUnaryExpr is called when entering the PostfixUnaryExpr production.
	EnterPostfixUnaryExpr(c *PostfixUnaryExprContext)

	// EnterFunctionCall is called when entering the FunctionCall production.
	EnterFunctionCall(c *FunctionCallContext)

	// EnterCondition is called when entering the condition production.
	EnterCondition(c *ConditionContext)

	// EnterArgDef is called when entering the argDef production.
	EnterArgDef(c *ArgDefContext)

	// EnterArgDefList is called when entering the argDefList production.
	EnterArgDefList(c *ArgDefListContext)

	// EnterExprList is called when entering the exprList production.
	EnterExprList(c *ExprListContext)

	// EnterIdentifier is called when entering the identifier production.
	EnterIdentifier(c *IdentifierContext)

	// EnterSumBinary is called when entering the SumBinary production.
	EnterSumBinary(c *SumBinaryContext)

	// EnterSubBinary is called when entering the SubBinary production.
	EnterSubBinary(c *SubBinaryContext)

	// EnterDivBinary is called when entering the DivBinary production.
	EnterDivBinary(c *DivBinaryContext)

	// EnterMulBinary is called when entering the MulBinary production.
	EnterMulBinary(c *MulBinaryContext)

	// EnterByteAndBinary is called when entering the ByteAndBinary production.
	EnterByteAndBinary(c *ByteAndBinaryContext)

	// EnterAndBinary is called when entering the AndBinary production.
	EnterAndBinary(c *AndBinaryContext)

	// EnterByteOrBinary is called when entering the ByteOrBinary production.
	EnterByteOrBinary(c *ByteOrBinaryContext)

	// EnterOrBinary is called when entering the OrBinary production.
	EnterOrBinary(c *OrBinaryContext)

	// EnterEqBinary is called when entering the EqBinary production.
	EnterEqBinary(c *EqBinaryContext)

	// EnterLtBinary is called when entering the LtBinary production.
	EnterLtBinary(c *LtBinaryContext)

	// EnterLtOrEtBinary is called when entering the LtOrEtBinary production.
	EnterLtOrEtBinary(c *LtOrEtBinaryContext)

	// EnterGtBinary is called when entering the GtBinary production.
	EnterGtBinary(c *GtBinaryContext)

	// EnterModBinary is called when entering the ModBinary production.
	EnterModBinary(c *ModBinaryContext)

	// EnterSumUnary is called when entering the SumUnary production.
	EnterSumUnary(c *SumUnaryContext)

	// EnterSubUnary is called when entering the SubUnary production.
	EnterSubUnary(c *SubUnaryContext)

	// EnterNotUnary is called when entering the NotUnary production.
	EnterNotUnary(c *NotUnaryContext)

	// EnterDecrementUnary is called when entering the DecrementUnary production.
	EnterDecrementUnary(c *DecrementUnaryContext)

	// EnterIncrementUnary is called when entering the IncrementUnary production.
	EnterIncrementUnary(c *IncrementUnaryContext)

	// ExitSource is called when exiting the source production.
	ExitSource(c *SourceContext)

	// ExitSourceItem is called when exiting the sourceItem production.
	ExitSourceItem(c *SourceItemContext)

	// ExitFunctionBody is called when exiting the functionBody production.
	ExitFunctionBody(c *FunctionBodyContext)

	// ExitFunctionSignature is called when exiting the functionSignature production.
	ExitFunctionSignature(c *FunctionSignatureContext)

	// ExitFunctionArgs is called when exiting the functionArgs production.
	ExitFunctionArgs(c *FunctionArgsContext)

	// ExitTypeRef is called when exiting the typeRef production.
	ExitTypeRef(c *TypeRefContext)

	// ExitIfThenStmnt is called when exiting the IfThenStmnt production.
	ExitIfThenStmnt(c *IfThenStmntContext)

	// ExitIfThenElseStmnt is called when exiting the IfThenElseStmnt production.
	ExitIfThenElseStmnt(c *IfThenElseStmntContext)

	// ExitWhileStmnt is called when exiting the WhileStmnt production.
	ExitWhileStmnt(c *WhileStmntContext)

	// ExitDoWhileStmnt is called when exiting the DoWhileStmnt production.
	ExitDoWhileStmnt(c *DoWhileStmntContext)

	// ExitBreakStmnt is called when exiting the BreakStmnt production.
	ExitBreakStmnt(c *BreakStmntContext)

	// ExitIdentifierStmnt is called when exiting the IdentifierStmnt production.
	ExitIdentifierStmnt(c *IdentifierStmntContext)

	// ExitExprStmnt is called when exiting the ExprStmnt production.
	ExitExprStmnt(c *ExprStmntContext)

	// ExitReturnStmnt is called when exiting the ReturnStmnt production.
	ExitReturnStmnt(c *ReturnStmntContext)

	// ExitNestedFunctionDeclaration is called when exiting the NestedFunctionDeclaration production.
	ExitNestedFunctionDeclaration(c *NestedFunctionDeclarationContext)

	// ExitWhileCondition is called when exiting the whileCondition production.
	ExitWhileCondition(c *WhileConditionContext)

	// ExitWhileBody is called when exiting the whileBody production.
	ExitWhileBody(c *WhileBodyContext)

	// ExitDoWhileBody is called when exiting the doWhileBody production.
	ExitDoWhileBody(c *DoWhileBodyContext)

	// ExitDoWhileCondition is called when exiting the doWhileCondition production.
	ExitDoWhileCondition(c *DoWhileConditionContext)

	// ExitIfCondition is called when exiting the ifCondition production.
	ExitIfCondition(c *IfConditionContext)

	// ExitIfTrueStatement is called when exiting the ifTrueStatement production.
	ExitIfTrueStatement(c *IfTrueStatementContext)

	// ExitIfFalseStatement is called when exiting the ifFalseStatement production.
	ExitIfFalseStatement(c *IfFalseStatementContext)

	// ExitPrefixUnaryExpr is called when exiting the PrefixUnaryExpr production.
	ExitPrefixUnaryExpr(c *PrefixUnaryExprContext)

	// ExitValueExpr is called when exiting the ValueExpr production.
	ExitValueExpr(c *ValueExprContext)

	// ExitVariable is called when exiting the Variable production.
	ExitVariable(c *VariableContext)

	// ExitBinaryExpr is called when exiting the BinaryExpr production.
	ExitBinaryExpr(c *BinaryExprContext)

	// ExitBracedExpr is called when exiting the BracedExpr production.
	ExitBracedExpr(c *BracedExprContext)

	// ExitPostfixUnaryExpr is called when exiting the PostfixUnaryExpr production.
	ExitPostfixUnaryExpr(c *PostfixUnaryExprContext)

	// ExitFunctionCall is called when exiting the FunctionCall production.
	ExitFunctionCall(c *FunctionCallContext)

	// ExitCondition is called when exiting the condition production.
	ExitCondition(c *ConditionContext)

	// ExitArgDef is called when exiting the argDef production.
	ExitArgDef(c *ArgDefContext)

	// ExitArgDefList is called when exiting the argDefList production.
	ExitArgDefList(c *ArgDefListContext)

	// ExitExprList is called when exiting the exprList production.
	ExitExprList(c *ExprListContext)

	// ExitIdentifier is called when exiting the identifier production.
	ExitIdentifier(c *IdentifierContext)

	// ExitSumBinary is called when exiting the SumBinary production.
	ExitSumBinary(c *SumBinaryContext)

	// ExitSubBinary is called when exiting the SubBinary production.
	ExitSubBinary(c *SubBinaryContext)

	// ExitDivBinary is called when exiting the DivBinary production.
	ExitDivBinary(c *DivBinaryContext)

	// ExitMulBinary is called when exiting the MulBinary production.
	ExitMulBinary(c *MulBinaryContext)

	// ExitByteAndBinary is called when exiting the ByteAndBinary production.
	ExitByteAndBinary(c *ByteAndBinaryContext)

	// ExitAndBinary is called when exiting the AndBinary production.
	ExitAndBinary(c *AndBinaryContext)

	// ExitByteOrBinary is called when exiting the ByteOrBinary production.
	ExitByteOrBinary(c *ByteOrBinaryContext)

	// ExitOrBinary is called when exiting the OrBinary production.
	ExitOrBinary(c *OrBinaryContext)

	// ExitEqBinary is called when exiting the EqBinary production.
	ExitEqBinary(c *EqBinaryContext)

	// ExitLtBinary is called when exiting the LtBinary production.
	ExitLtBinary(c *LtBinaryContext)

	// ExitLtOrEtBinary is called when exiting the LtOrEtBinary production.
	ExitLtOrEtBinary(c *LtOrEtBinaryContext)

	// ExitGtBinary is called when exiting the GtBinary production.
	ExitGtBinary(c *GtBinaryContext)

	// ExitModBinary is called when exiting the ModBinary production.
	ExitModBinary(c *ModBinaryContext)

	// ExitSumUnary is called when exiting the SumUnary production.
	ExitSumUnary(c *SumUnaryContext)

	// ExitSubUnary is called when exiting the SubUnary production.
	ExitSubUnary(c *SubUnaryContext)

	// ExitNotUnary is called when exiting the NotUnary production.
	ExitNotUnary(c *NotUnaryContext)

	// ExitDecrementUnary is called when exiting the DecrementUnary production.
	ExitDecrementUnary(c *DecrementUnaryContext)

	// ExitIncrementUnary is called when exiting the IncrementUnary production.
	ExitIncrementUnary(c *IncrementUnaryContext)
}
