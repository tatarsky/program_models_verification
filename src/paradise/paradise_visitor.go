// Generated from /Users/tva/ITMO_old/program_models_verification/Paradise.g4 by ANTLR 4.7.

package paradise // Paradise
import "github.com/antlr/antlr4/runtime/Go/antlr"

// A complete Visitor for a parse tree produced by ParadiseParser.
type ParadiseVisitor interface {
	antlr.ParseTreeVisitor

	// Visit a parse tree produced by ParadiseParser#source.
	VisitSource(ctx *SourceContext) interface{}

	// Visit a parse tree produced by ParadiseParser#sourceItem.
	VisitSourceItem(ctx *SourceItemContext) interface{}

	// Visit a parse tree produced by ParadiseParser#functionBody.
	VisitFunctionBody(ctx *FunctionBodyContext) interface{}

	// Visit a parse tree produced by ParadiseParser#functionSignature.
	VisitFunctionSignature(ctx *FunctionSignatureContext) interface{}

	// Visit a parse tree produced by ParadiseParser#functionArgs.
	VisitFunctionArgs(ctx *FunctionArgsContext) interface{}

	// Visit a parse tree produced by ParadiseParser#typeRef.
	VisitTypeRef(ctx *TypeRefContext) interface{}

	// Visit a parse tree produced by ParadiseParser#IfThenStmnt.
	VisitIfThenStmnt(ctx *IfThenStmntContext) interface{}

	// Visit a parse tree produced by ParadiseParser#IfThenElseStmnt.
	VisitIfThenElseStmnt(ctx *IfThenElseStmntContext) interface{}

	// Visit a parse tree produced by ParadiseParser#WhileStmnt.
	VisitWhileStmnt(ctx *WhileStmntContext) interface{}

	// Visit a parse tree produced by ParadiseParser#DoWhileStmnt.
	VisitDoWhileStmnt(ctx *DoWhileStmntContext) interface{}

	// Visit a parse tree produced by ParadiseParser#BreakStmnt.
	VisitBreakStmnt(ctx *BreakStmntContext) interface{}

	// Visit a parse tree produced by ParadiseParser#IdentifierStmnt.
	VisitIdentifierStmnt(ctx *IdentifierStmntContext) interface{}

	// Visit a parse tree produced by ParadiseParser#ExprStmnt.
	VisitExprStmnt(ctx *ExprStmntContext) interface{}

	// Visit a parse tree produced by ParadiseParser#ReturnStmnt.
	VisitReturnStmnt(ctx *ReturnStmntContext) interface{}

	// Visit a parse tree produced by ParadiseParser#NestedFunctionDeclaration.
	VisitNestedFunctionDeclaration(ctx *NestedFunctionDeclarationContext) interface{}

	// Visit a parse tree produced by ParadiseParser#whileCondition.
	VisitWhileCondition(ctx *WhileConditionContext) interface{}

	// Visit a parse tree produced by ParadiseParser#whileBody.
	VisitWhileBody(ctx *WhileBodyContext) interface{}

	// Visit a parse tree produced by ParadiseParser#doWhileBody.
	VisitDoWhileBody(ctx *DoWhileBodyContext) interface{}

	// Visit a parse tree produced by ParadiseParser#doWhileCondition.
	VisitDoWhileCondition(ctx *DoWhileConditionContext) interface{}

	// Visit a parse tree produced by ParadiseParser#ifCondition.
	VisitIfCondition(ctx *IfConditionContext) interface{}

	// Visit a parse tree produced by ParadiseParser#ifTrueStatement.
	VisitIfTrueStatement(ctx *IfTrueStatementContext) interface{}

	// Visit a parse tree produced by ParadiseParser#ifFalseStatement.
	VisitIfFalseStatement(ctx *IfFalseStatementContext) interface{}

	// Visit a parse tree produced by ParadiseParser#PrefixUnaryExpr.
	VisitPrefixUnaryExpr(ctx *PrefixUnaryExprContext) interface{}

	// Visit a parse tree produced by ParadiseParser#ValueExpr.
	VisitValueExpr(ctx *ValueExprContext) interface{}

	// Visit a parse tree produced by ParadiseParser#Variable.
	VisitVariable(ctx *VariableContext) interface{}

	// Visit a parse tree produced by ParadiseParser#BinaryExpr.
	VisitBinaryExpr(ctx *BinaryExprContext) interface{}

	// Visit a parse tree produced by ParadiseParser#BracedExpr.
	VisitBracedExpr(ctx *BracedExprContext) interface{}

	// Visit a parse tree produced by ParadiseParser#PostfixUnaryExpr.
	VisitPostfixUnaryExpr(ctx *PostfixUnaryExprContext) interface{}

	// Visit a parse tree produced by ParadiseParser#FunctionCall.
	VisitFunctionCall(ctx *FunctionCallContext) interface{}

	// Visit a parse tree produced by ParadiseParser#condition.
	VisitCondition(ctx *ConditionContext) interface{}

	// Visit a parse tree produced by ParadiseParser#argDef.
	VisitArgDef(ctx *ArgDefContext) interface{}

	// Visit a parse tree produced by ParadiseParser#argDefList.
	VisitArgDefList(ctx *ArgDefListContext) interface{}

	// Visit a parse tree produced by ParadiseParser#exprList.
	VisitExprList(ctx *ExprListContext) interface{}

	// Visit a parse tree produced by ParadiseParser#identifier.
	VisitIdentifier(ctx *IdentifierContext) interface{}

	// Visit a parse tree produced by ParadiseParser#SumBinary.
	VisitSumBinary(ctx *SumBinaryContext) interface{}

	// Visit a parse tree produced by ParadiseParser#SubBinary.
	VisitSubBinary(ctx *SubBinaryContext) interface{}

	// Visit a parse tree produced by ParadiseParser#DivBinary.
	VisitDivBinary(ctx *DivBinaryContext) interface{}

	// Visit a parse tree produced by ParadiseParser#MulBinary.
	VisitMulBinary(ctx *MulBinaryContext) interface{}

	// Visit a parse tree produced by ParadiseParser#ByteAndBinary.
	VisitByteAndBinary(ctx *ByteAndBinaryContext) interface{}

	// Visit a parse tree produced by ParadiseParser#AndBinary.
	VisitAndBinary(ctx *AndBinaryContext) interface{}

	// Visit a parse tree produced by ParadiseParser#ByteOrBinary.
	VisitByteOrBinary(ctx *ByteOrBinaryContext) interface{}

	// Visit a parse tree produced by ParadiseParser#OrBinary.
	VisitOrBinary(ctx *OrBinaryContext) interface{}

	// Visit a parse tree produced by ParadiseParser#EqBinary.
	VisitEqBinary(ctx *EqBinaryContext) interface{}

	// Visit a parse tree produced by ParadiseParser#LtBinary.
	VisitLtBinary(ctx *LtBinaryContext) interface{}

	// Visit a parse tree produced by ParadiseParser#LtOrEtBinary.
	VisitLtOrEtBinary(ctx *LtOrEtBinaryContext) interface{}

	// Visit a parse tree produced by ParadiseParser#GtBinary.
	VisitGtBinary(ctx *GtBinaryContext) interface{}

	// Visit a parse tree produced by ParadiseParser#ModBinary.
	VisitModBinary(ctx *ModBinaryContext) interface{}

	// Visit a parse tree produced by ParadiseParser#SumUnary.
	VisitSumUnary(ctx *SumUnaryContext) interface{}

	// Visit a parse tree produced by ParadiseParser#SubUnary.
	VisitSubUnary(ctx *SubUnaryContext) interface{}

	// Visit a parse tree produced by ParadiseParser#NotUnary.
	VisitNotUnary(ctx *NotUnaryContext) interface{}

	// Visit a parse tree produced by ParadiseParser#DecrementUnary.
	VisitDecrementUnary(ctx *DecrementUnaryContext) interface{}

	// Visit a parse tree produced by ParadiseParser#IncrementUnary.
	VisitIncrementUnary(ctx *IncrementUnaryContext) interface{}
}
