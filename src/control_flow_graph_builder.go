package main

import (
	"./paradise"
	"fmt"
	"github.com/antlr/antlr4/runtime/Go/antlr"
	"github.com/awalterschulze/gographviz"
	"math"
	"os"
	"strconv"
	"strings"
	"sync"
)

type CfgBuilder struct {
	index int
	tree  []*CfgTreeItem
	edges map[int][]int
	paradise.BaseParadiseVisitor
}

func NewCfgBuilder() *CfgBuilder {
	cfg_builder := new(CfgBuilder)
	cfg_builder.edges = map[int][]int{}
	tree_item := NewCfgTreeItem("Start", 0, "root")
	cfg_builder.tree = append(cfg_builder.tree, tree_item)
	return cfg_builder
}

//func (v *CfgBuilder) VisitBreakStmnt(ctx *paradise.IdentifierStmntContext) interface{} {
//	// @TODO: Implement Break Stmnt
//	return v.VisitChildren(ctx)
//}

func (v *CfgBuilder) VisitIdentifierStmnt(ctx *paradise.IdentifierStmntContext) interface{} {
	v.index++
	tree_item := NewCfgTreeItem(ctx.GetText(), v.index, "basic")
	v.AddNode(tree_item)
	v.AddEdge(v.index-1, v.index, false)
	return v.VisitChildren(ctx)
}

func (v *CfgBuilder) VisitExprStmnt(ctx *paradise.ExprStmntContext) interface{} {
	v.index++
	tree_item := NewCfgTreeItem(ctx.GetText(), v.index, "basic")
	v.AddNode(tree_item)
	v.AddEdge(v.index-1, v.index, false)
	return v.VisitChildren(ctx)
}

func (v *CfgBuilder) Explore(tree antlr.Tree) {
	tree.(antlr.ParseTree).Accept(v)
	for _, item := range tree.GetChildren() {
		switch item.(type) {
		case *paradise.WhileStmntContext:
			while_tree_item_index := v.AddCommonBlock(
				"condition",
				item.GetChild(1).(antlr.ParseTree).GetText(),
			)

			v.Explore(item.GetChild(2))
			end_of_while_body_index := v.index

			v.index++
			null_block_item := NewCfgTreeItem("", v.index, "null_block")
			v.AddNode(null_block_item)
			v.AddEdge(end_of_while_body_index, while_tree_item_index, false)
			v.AddEdge(while_tree_item_index, end_of_while_body_index+1, false)

		case *paradise.DoWhileStmntContext:
			v.AddCommonBlock("null_block", "")
			do_while_start_index := v.index
			v.Explore(item.GetChild(1))

			v.index++
			condition := item.GetChild(4).(antlr.ParseTree).GetText()
			do_while_item := NewCfgTreeItem(condition, v.index, "condition")
			v.AddNode(do_while_item)
			v.AddEdge(v.index-1, do_while_item.Index, false)

			v.AddEdge(v.index, do_while_start_index, false)

		case *paradise.IfThenStmntContext:
			if_then_item_id := v.AddCommonBlock(
				"condition",
				item.GetChild(1).(antlr.ParseTree).GetText(),
			)

			v.Explore(item.GetChild(3))
			end_of_while_body_index := v.index

			v.index++
			v.AddNode(NewCfgTreeItem("", v.index, "null_block"))
			v.AddEdge(end_of_while_body_index, end_of_while_body_index+1, false)
			v.AddEdge(if_then_item_id, end_of_while_body_index+1, false)
		case *paradise.IfThenElseStmntContext:
			if_tree_item_id := v.AddCommonBlock(
				"condition",
				item.GetChild(1).(antlr.ParseTree).GetText(),
			)

			// Edge from if condition block to true branch
			true_branch := item.GetChild(3).(antlr.ParseTree)
			v.Explore(true_branch)
			end_true_branch_index := v.index

			false_branch := item.GetChild(5).(antlr.ParseTree)
			v.Explore(false_branch)
			end_false_branch_index := v.index

			v.index++
			null_block_item := NewCfgTreeItem("", v.index, "null_block")
			v.AddEdge(end_true_branch_index, v.index, true)
			v.AddNode(null_block_item)

			// Relocate false branch link
			v.AddEdge(if_tree_item_id, end_true_branch_index+1, false)
			v.AddEdge(end_false_branch_index, v.index, false)
		default:
			v.Explore(item.(antlr.ParseTree))
		}
	}
}

func (v *CfgBuilder) AddCommonBlock(kind string, text string) int {
	v.index++
	item := NewCfgTreeItem(text, v.index, kind)
	v.AddNode(item)
	v.AddEdge(v.index-1, v.index, false)

	return item.Index
}

func (v *CfgBuilder) AddEdge(index_from int, index_to int, force bool) {
	if force {
		v.edges[index_from] = []int{index_to}
	} else {
		v.edges[index_from] = append(v.edges[index_from], index_to)
	}
}

func (v *CfgBuilder) AddNode(item *CfgTreeItem) {
	v.tree = append(v.tree, item)
}

func (v *CfgBuilder) Dot() *gographviz.Graph {
	graphAst, _ := gographviz.ParseString(`digraph G {}`)
	graph := gographviz.NewGraph()
	if err := gographviz.Analyse(graphAst, graph); err != nil {
		panic(err)
	}

	for _, item := range v.tree {
		label := fmt.Sprintf("\"%s\"", strings.Replace(item.Data, "\"", "\\\"", -1))
		options := map[string]string{"label": label}
		switch item.Kind {
		case "root", "end":
			options["shape"] = "Mdiamond"
		case "condition":
			options["shape"] = "diamond"
		case "null_block":
			options["shape"] = "point"
		}

		graph.AddNode("G", strconv.Itoa(item.Index), options)
		for step, index := range v.edges[item.Index] {
			options := map[string]string{}

			if item.Kind == "condition" {
				if math.Mod(float64(step), 2) == 1 {
					options["label"] = "false"
				} else {
					options["label"] = "true"
				}
			}
			graph.AddEdge(
				strconv.Itoa(item.Index),
				strconv.Itoa(index),
				true,
				options,
			)
		}
	}
	return graph
}

func (v *CfgBuilder) AddEndNode() {
	v.index++
	end_node := NewCfgTreeItem("End", v.index, "end")
	v.tree = append(v.tree, end_node)
	v.edges[v.index-1] = append(v.edges[v.index-1], v.index)
}

func (v *CfgBuilder) ShowTree() {
	for _, item := range v.tree {
		fmt.Println(item.Data, item.Index, v.edges[item.Index])
	}
}

func ExploreForEveryFunction(tree antlr.Tree) {
	for _, item := range tree.GetChildren() {
		switch item.(type) {
		case *paradise.SourceItemContext:
			cfg_builder := NewCfgBuilder()
			cfg_builder.Explore(item)
			cfg_builder.AddEndNode()

			graph := cfg_builder.Dot()
			function_name := item.GetChild(1).GetChild(0).(antlr.ParseTree).GetText()
			graph.Name = function_name

			os.MkdirAll(CONTROL_FLOW_GRAPH_OUTPUT, os.ModePerm)
			graph_dot_file_path := CONTROL_FLOW_GRAPH_OUTPUT + graph.Name

			wg := new(sync.WaitGroup)
			wg.Add(2)
			exeCmd("rm -rf "+CONTROL_FLOW_GRAPH_OUTPUT+"*", wg)
			writeToFile([]byte(graph.String()), graph_dot_file_path+".dot")
			exeCmd(
				fmt.Sprintf(
					"dot -Tpng %s -o%s",
					graph_dot_file_path+".dot",
					graph_dot_file_path+".png",
				),
				wg,
			)
			wg.Wait()
		}
	}
}
