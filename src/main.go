package main

import (
	"flag"
	"fmt"
	"os"
	"strings"
	"github.com/antlr/antlr4/runtime/Go/antlr"
)

// @TODO: Store graph in global scope for reusable purposes
// @TODO: Fix command parsing. Add modes for analyzer

func main() {
	mode := flag.String("mode", "default", "")
	file_paths := flag.String("files", "default", "")
	flag.Parse()
	list_of_files := strings.Split(*file_paths, ",")

	switch *mode {
	case "ast":
		ast := Ast{}
		ast.Print(list_of_files)

	case "cfg":
		// Build control flow graph for each function
		parser := prepareParser(list_of_files)
		tree := parser.Source()
		ExploreForEveryFunction(tree)

		// Build Call graph
		parser = prepareParser(list_of_files)
		cg := NewCallGraph()
		antlr.ParseTreeWalkerDefault.Walk(cg,tree)
		cg.Dot()

	default:
		fmt.Println(fmt.Sprintf("Invalid mode (%s)", *mode))
		os.Exit(2)
	}
}
