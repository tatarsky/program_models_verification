package main

type CfgTreeItem struct {
	Data  string
	Index int
	Kind  string
}

func NewCfgTreeItem(data string, index int, kind string) *CfgTreeItem {
	cb := new(CfgTreeItem)
	cb.Data = data
	cb.Index = index
	cb.Kind = kind
	return cb
}
