package main

import (
	"./paradise"
	"bytes"
	"fmt"
	"github.com/antlr/antlr4/runtime/Go/antlr"
	"io/ioutil"
	"os/exec"
	"reflect"
	"strings"
	"sync"
)

// Prepare parser from given files for latest usage
func prepareParser(file_path []string) *paradise.ParadiseParser {
	input := toInputStream(concatFiles(file_path))
	lexer := paradise.NewParadiseLexer(input)
	stream := antlr.NewCommonTokenStream(lexer, 0)
	parser := paradise.NewParadiseParser(stream)
	parser.BuildParseTrees = true
	return parser
}

// Concat given files into one
func concatFiles(fp []string) string {
	var result string = ""
	for _, item := range fp {
		data, err := ioutil.ReadFile(item)
		if err != nil {
			continue
		}
		result += "\n" + string(data)
	}

	return result
}

// Type converter from go's type to string
func detectType(obj interface{}) string {
	return reflect.TypeOf(obj).String()
}

// Input stream builder from given file
func toInputStream(str string) *antlr.FileStream {
	buf := bytes.NewBuffer([]byte(str))

	fs := new(antlr.FileStream)
	s := string(buf.Bytes())

	fs.InputStream = antlr.NewInputStream(s)

	return fs
}

// Identation printer
func printWhitespaces(number int) string {
	return strings.Repeat(IDENTATION, number)
}

// Text limiter for disabling long strings
func textLimiter(str string) string {
	if len(str) < STRING_LIMIT {
		return str
	} else {
		return str[0:STRING_LIMIT] + "..."
	}
}

// File writer helper
func writeToFile(data []byte, path string) {
	err := ioutil.WriteFile(path, data, 0644)
	check(err)
}

// Error checking wrapper
func check(e error) {
	if e != nil {
		panic(e)
	}
}

func addBraces(str string) string {
	str = strings.Replace(str, "\"", "\\\"", -1)
	return fmt.Sprintf("\"%s\"", str)
}

func exeCmd(cmd string, wg *sync.WaitGroup) {
	parts := strings.Fields(cmd)
	head := parts[0]
	parts = parts[1:len(parts)]

	out, err := exec.Command(head, parts...).Output()
	if err != nil {
		fmt.Printf("%s", err)
	}
	fmt.Printf("%s", out)
	wg.Done() // Need to signal to waitgroup that this goroutine is done
}
