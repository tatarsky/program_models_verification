package main

import(
	"./paradise"
	"github.com/antlr/antlr4/runtime/Go/antlr"
	"fmt"
	"github.com/awalterschulze/gographviz"
	"os"
	"sync"
)

type CallGraph struct {
	*paradise.BaseParadiseListener
	CurrentFunctionName string
	Functions map[string][]string
}

func NewCallGraph() *CallGraph{
	cg := new(CallGraph)
	cg.Functions = map[string][]string{}
	return cg
}

func (cg *CallGraph) Dot() {
	graphAst, _ := gographviz.ParseString(`digraph G {}`)
	graph := gographviz.NewGraph()
	if err := gographviz.Analyse(graphAst, graph); err != nil {
		panic(err)
	}
	graph.Name = "Call_graph"

	for k, _ := range cg.Functions {
		graph.AddNode(k, k, map[string]string{})
	}

	for k, v := range cg.Functions {
		for _, fnc := range v {
			graph.AddEdge(k, fnc, true, map[string]string{})
		}
	}

	os.MkdirAll(CONTROL_FLOW_GRAPH_OUTPUT, os.ModePerm)
	graph_dot_file_path := CONTROL_FLOW_GRAPH_OUTPUT + "call_graph"

	wg := new(sync.WaitGroup)
	wg.Add(2)
	exeCmd("rm -rf "+graph_dot_file_path+"*", wg)
	writeToFile([]byte(graph.String()), graph_dot_file_path+".dot")
	exeCmd(
		fmt.Sprintf(
			"dot -Tpng %s -o%s",
			graph_dot_file_path+".dot",
			graph_dot_file_path+".png",
		),
		wg,
	)
	wg.Wait()
}

func (cg *CallGraph) EnterSourceItem(ctx *paradise.SourceItemContext) {
	fname := ctx.GetChild(1).GetChild(0).(antlr.ParseTree).GetText()
	cg.CurrentFunctionName = fname
	cg.Functions[fname] = []string{}
}

func (cg *CallGraph) EnterFunctionCall(ctx *paradise.FunctionCallContext) {
	fname := ctx.GetChild(0).(antlr.ParseTree).GetText()
	cg.AppendFunction(fname)
}

func (cg *CallGraph) AppendFunction(fname string) {
	cg.Functions[cg.CurrentFunctionName] = append(cg.Functions[cg.CurrentFunctionName], fname)
}