package main

import (
	"./paradise"
	"fmt"
	"github.com/antlr/antlr4/runtime/Go/antlr"
)

type Ast struct {
	parser *paradise.ParadiseParser
}

// Ast printer start point
func (a *Ast) Print(file_path []string) {
	a.parser = prepareParser(file_path)
	a.explore(a.parser.Source(), 0)
}

// Custom visitor, which able to use good tree identation
func (a *Ast) explore(ctx antlr.Tree, indentation int) {
	rule_context := ctx.GetPayload().(*antlr.BaseParserRuleContext)
	ruleName := a.parser.RuleNames[rule_context.RuleIndex]

	printTreeLine(detectType(ctx), indentation, ruleName, rule_context)

	for i := 0; i < ctx.GetChildCount(); i++ {
		element := ctx.GetChild(i)
		fmt.Println(element)

		switch element.(type) {
		case antlr.RuleContext:
			a.explore(element, indentation+2)
		}
	}
}

// Tree line print helper
func printTreeLine(type_name string, indentation int, ruleName string, rule_context interface {
	GetText() string
}) {
	fmt.Println(
		fmt.Sprintf(
			"%21s%s %s %s",
			type_name[10:len(type_name)-7],
			printWhitespaces(indentation),
			ruleName,
			textLimiter(rule_context.GetText()),
		),
	)
}
