// Generated from /Users/tva/workspace/program_models_verification/Paradise.g4 by ANTLR 4.7
import org.antlr.v4.runtime.tree.ParseTreeVisitor;

/**
 * This interface defines a complete generic visitor for a parse tree produced
 * by {@link ParadiseParser}.
 *
 * @param <T> The return type of the visit operation. Use {@link Void} for
 * operations with no return type.
 */
public interface ParadiseVisitor<T> extends ParseTreeVisitor<T> {
	/**
	 * Visit a parse tree produced by the {@code SourceRule}
	 * labeled alternative in {@link ParadiseParser#source}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSourceRule(ParadiseParser.SourceRuleContext ctx);
	/**
	 * Visit a parse tree produced by the {@code SourceItemRule}
	 * labeled alternative in {@link ParadiseParser#sourceItem}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSourceItemRule(ParadiseParser.SourceItemRuleContext ctx);
	/**
	 * Visit a parse tree produced by the {@code FunctionSignatureRule}
	 * labeled alternative in {@link ParadiseParser#functionSignature}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFunctionSignatureRule(ParadiseParser.FunctionSignatureRuleContext ctx);
	/**
	 * Visit a parse tree produced by the {@code BuiltInBoolType}
	 * labeled alternative in {@link ParadiseParser#typeRef}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBuiltInBoolType(ParadiseParser.BuiltInBoolTypeContext ctx);
	/**
	 * Visit a parse tree produced by the {@code BuiltInByteType}
	 * labeled alternative in {@link ParadiseParser#typeRef}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBuiltInByteType(ParadiseParser.BuiltInByteTypeContext ctx);
	/**
	 * Visit a parse tree produced by the {@code BuiltInIntType}
	 * labeled alternative in {@link ParadiseParser#typeRef}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBuiltInIntType(ParadiseParser.BuiltInIntTypeContext ctx);
	/**
	 * Visit a parse tree produced by the {@code BuiltInUintType}
	 * labeled alternative in {@link ParadiseParser#typeRef}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBuiltInUintType(ParadiseParser.BuiltInUintTypeContext ctx);
	/**
	 * Visit a parse tree produced by the {@code BuiltInLongType}
	 * labeled alternative in {@link ParadiseParser#typeRef}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBuiltInLongType(ParadiseParser.BuiltInLongTypeContext ctx);
	/**
	 * Visit a parse tree produced by the {@code BuiltInUlongType}
	 * labeled alternative in {@link ParadiseParser#typeRef}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBuiltInUlongType(ParadiseParser.BuiltInUlongTypeContext ctx);
	/**
	 * Visit a parse tree produced by the {@code BuiltInCharType}
	 * labeled alternative in {@link ParadiseParser#typeRef}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBuiltInCharType(ParadiseParser.BuiltInCharTypeContext ctx);
	/**
	 * Visit a parse tree produced by the {@code BuiltInStringType}
	 * labeled alternative in {@link ParadiseParser#typeRef}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBuiltInStringType(ParadiseParser.BuiltInStringTypeContext ctx);
	/**
	 * Visit a parse tree produced by the {@code CustomType}
	 * labeled alternative in {@link ParadiseParser#typeRef}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCustomType(ParadiseParser.CustomTypeContext ctx);
	/**
	 * Visit a parse tree produced by the {@code IfStatement}
	 * labeled alternative in {@link ParadiseParser#statement}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIfStatement(ParadiseParser.IfStatementContext ctx);
	/**
	 * Visit a parse tree produced by the {@code WhileStatement}
	 * labeled alternative in {@link ParadiseParser#statement}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitWhileStatement(ParadiseParser.WhileStatementContext ctx);
	/**
	 * Visit a parse tree produced by the {@code DoWhileStatement}
	 * labeled alternative in {@link ParadiseParser#statement}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDoWhileStatement(ParadiseParser.DoWhileStatementContext ctx);
	/**
	 * Visit a parse tree produced by the {@code BreakStatement}
	 * labeled alternative in {@link ParadiseParser#statement}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBreakStatement(ParadiseParser.BreakStatementContext ctx);
	/**
	 * Visit a parse tree produced by the {@code AssignmentStatement}
	 * labeled alternative in {@link ParadiseParser#statement}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAssignmentStatement(ParadiseParser.AssignmentStatementContext ctx);
	/**
	 * Visit a parse tree produced by the {@code ExpressionStatement}
	 * labeled alternative in {@link ParadiseParser#statement}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExpressionStatement(ParadiseParser.ExpressionStatementContext ctx);
	/**
	 * Visit a parse tree produced by the {@code BinaryExpression}
	 * labeled alternative in {@link ParadiseParser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBinaryExpression(ParadiseParser.BinaryExpressionContext ctx);
	/**
	 * Visit a parse tree produced by the {@code PrefixUnaryExpression}
	 * labeled alternative in {@link ParadiseParser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitPrefixUnaryExpression(ParadiseParser.PrefixUnaryExpressionContext ctx);
	/**
	 * Visit a parse tree produced by the {@code LiteralExpression}
	 * labeled alternative in {@link ParadiseParser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitLiteralExpression(ParadiseParser.LiteralExpressionContext ctx);
	/**
	 * Visit a parse tree produced by the {@code PostfixUnaryExpression}
	 * labeled alternative in {@link ParadiseParser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitPostfixUnaryExpression(ParadiseParser.PostfixUnaryExpressionContext ctx);
	/**
	 * Visit a parse tree produced by the {@code PlaceExpression}
	 * labeled alternative in {@link ParadiseParser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitPlaceExpression(ParadiseParser.PlaceExpressionContext ctx);
	/**
	 * Visit a parse tree produced by the {@code CallExpression}
	 * labeled alternative in {@link ParadiseParser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCallExpression(ParadiseParser.CallExpressionContext ctx);
	/**
	 * Visit a parse tree produced by {@link ParadiseParser#argDef}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitArgDef(ParadiseParser.ArgDefContext ctx);
	/**
	 * Visit a parse tree produced by {@link ParadiseParser#argDefList}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitArgDefList(ParadiseParser.ArgDefListContext ctx);
	/**
	 * Visit a parse tree produced by {@link ParadiseParser#exprList}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExprList(ParadiseParser.ExprListContext ctx);
}