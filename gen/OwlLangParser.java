// Generated from /Users/tva/workspace/program_models_verification/Paradise.g4 by ANTLR 4.7
import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.misc.*;
import org.antlr.v4.runtime.tree.*;
import java.util.List;
import java.util.Iterator;
import java.util.ArrayList;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class ParadiseParser extends Parser {
	static { RuntimeMetaData.checkVersion("4.7", RuntimeMetaData.VERSION); }

	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		T__0=1, T__1=2, T__2=3, T__3=4, T__4=5, T__5=6, T__6=7, T__7=8, T__8=9, 
		T__9=10, T__10=11, T__11=12, T__12=13, T__13=14, T__14=15, T__15=16, T__16=17, 
		T__17=18, T__18=19, T__19=20, T__20=21, T__21=22, T__22=23, T__23=24, 
		FUNCTION=25, END_FUNCTION=26, BINARY=27, UNARY=28, CHAR=29, STRING=30, 
		HEX=31, BITS=32, DEC=33, BOOL=34, IDENTIFIER=35, COMMENT=36, WS=37;
	public static final int
		RULE_source = 0, RULE_sourceItem = 1, RULE_functionSignature = 2, RULE_typeRef = 3, 
		RULE_statement = 4, RULE_expr = 5, RULE_argDef = 6, RULE_argDefList = 7, 
		RULE_exprList = 8;
	public static final String[] ruleNames = {
		"source", "sourceItem", "functionSignature", "typeRef", "statement", "expr", 
		"argDef", "argDefList", "exprList"
	};

	private static final String[] _LITERAL_NAMES = {
		null, "'('", "')'", "'as'", "'bool'", "'byte'", "'int'", "'uint'", "'long'", 
		"'ulong'", "'char'", "'string'", "'if'", "'then'", "'else'", "'endif'", 
		"'while'", "'wend'", "'do'", "'loop'", "'until'", "'break'", "'='", "';'", 
		"','", "'function'", "'end function'"
	};
	private static final String[] _SYMBOLIC_NAMES = {
		null, null, null, null, null, null, null, null, null, null, null, null, 
		null, null, null, null, null, null, null, null, null, null, null, null, 
		null, "FUNCTION", "END_FUNCTION", "BINARY", "UNARY", "CHAR", "STRING", 
		"HEX", "BITS", "DEC", "BOOL", "IDENTIFIER", "COMMENT", "WS"
	};
	public static final Vocabulary VOCABULARY = new VocabularyImpl(_LITERAL_NAMES, _SYMBOLIC_NAMES);

	/**
	 * @deprecated Use {@link #VOCABULARY} instead.
	 */
	@Deprecated
	public static final String[] tokenNames;
	static {
		tokenNames = new String[_SYMBOLIC_NAMES.length];
		for (int i = 0; i < tokenNames.length; i++) {
			tokenNames[i] = VOCABULARY.getLiteralName(i);
			if (tokenNames[i] == null) {
				tokenNames[i] = VOCABULARY.getSymbolicName(i);
			}

			if (tokenNames[i] == null) {
				tokenNames[i] = "<INVALID>";
			}
		}
	}

	@Override
	@Deprecated
	public String[] getTokenNames() {
		return tokenNames;
	}

	@Override

	public Vocabulary getVocabulary() {
		return VOCABULARY;
	}

	@Override
	public String getGrammarFileName() { return "Paradise.g4"; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String getSerializedATN() { return _serializedATN; }

	@Override
	public ATN getATN() { return _ATN; }

	public ParadiseParser(TokenStream input) {
		super(input);
		_interp = new ParserATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}
	public static class SourceContext extends ParserRuleContext {
		public SourceContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_source; }
	 
		public SourceContext() { }
		public void copyFrom(SourceContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class SourceRuleContext extends SourceContext {
		public List<SourceItemContext> sourceItem() {
			return getRuleContexts(SourceItemContext.class);
		}
		public SourceItemContext sourceItem(int i) {
			return getRuleContext(SourceItemContext.class,i);
		}
		public SourceRuleContext(SourceContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ParadiseListener ) ((ParadiseListener)listener).enterSourceRule(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ParadiseListener ) ((ParadiseListener)listener).exitSourceRule(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ParadiseVisitor ) return ((ParadiseVisitor<? extends T>)visitor).visitSourceRule(this);
			else return visitor.visitChildren(this);
		}
	}

	public final SourceContext source() throws RecognitionException {
		SourceContext _localctx = new SourceContext(_ctx, getState());
		enterRule(_localctx, 0, RULE_source);
		int _la;
		try {
			_localctx = new SourceRuleContext(_localctx);
			enterOuterAlt(_localctx, 1);
			{
			setState(21);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==FUNCTION) {
				{
				{
				setState(18);
				sourceItem();
				}
				}
				setState(23);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class SourceItemContext extends ParserRuleContext {
		public SourceItemContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_sourceItem; }
	 
		public SourceItemContext() { }
		public void copyFrom(SourceItemContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class SourceItemRuleContext extends SourceItemContext {
		public TerminalNode FUNCTION() { return getToken(ParadiseParser.FUNCTION, 0); }
		public FunctionSignatureContext functionSignature() {
			return getRuleContext(FunctionSignatureContext.class,0);
		}
		public TerminalNode END_FUNCTION() { return getToken(ParadiseParser.END_FUNCTION, 0); }
		public List<StatementContext> statement() {
			return getRuleContexts(StatementContext.class);
		}
		public StatementContext statement(int i) {
			return getRuleContext(StatementContext.class,i);
		}
		public SourceItemRuleContext(SourceItemContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ParadiseListener ) ((ParadiseListener)listener).enterSourceItemRule(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ParadiseListener ) ((ParadiseListener)listener).exitSourceItemRule(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ParadiseVisitor ) return ((ParadiseVisitor<? extends T>)visitor).visitSourceItemRule(this);
			else return visitor.visitChildren(this);
		}
	}

	public final SourceItemContext sourceItem() throws RecognitionException {
		SourceItemContext _localctx = new SourceItemContext(_ctx, getState());
		enterRule(_localctx, 2, RULE_sourceItem);
		int _la;
		try {
			_localctx = new SourceItemRuleContext(_localctx);
			enterOuterAlt(_localctx, 1);
			{
			setState(24);
			match(FUNCTION);
			setState(25);
			functionSignature();
			setState(29);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__11) | (1L << T__15) | (1L << T__17) | (1L << T__20) | (1L << UNARY) | (1L << CHAR) | (1L << STRING) | (1L << HEX) | (1L << BITS) | (1L << DEC) | (1L << BOOL) | (1L << IDENTIFIER))) != 0)) {
				{
				{
				setState(26);
				statement();
				}
				}
				setState(31);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(32);
			match(END_FUNCTION);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class FunctionSignatureContext extends ParserRuleContext {
		public FunctionSignatureContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_functionSignature; }
	 
		public FunctionSignatureContext() { }
		public void copyFrom(FunctionSignatureContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class FunctionSignatureRuleContext extends FunctionSignatureContext {
		public TerminalNode IDENTIFIER() { return getToken(ParadiseParser.IDENTIFIER, 0); }
		public ArgDefListContext argDefList() {
			return getRuleContext(ArgDefListContext.class,0);
		}
		public TypeRefContext typeRef() {
			return getRuleContext(TypeRefContext.class,0);
		}
		public FunctionSignatureRuleContext(FunctionSignatureContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ParadiseListener ) ((ParadiseListener)listener).enterFunctionSignatureRule(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ParadiseListener ) ((ParadiseListener)listener).exitFunctionSignatureRule(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ParadiseVisitor ) return ((ParadiseVisitor<? extends T>)visitor).visitFunctionSignatureRule(this);
			else return visitor.visitChildren(this);
		}
	}

	public final FunctionSignatureContext functionSignature() throws RecognitionException {
		FunctionSignatureContext _localctx = new FunctionSignatureContext(_ctx, getState());
		enterRule(_localctx, 4, RULE_functionSignature);
		int _la;
		try {
			_localctx = new FunctionSignatureRuleContext(_localctx);
			enterOuterAlt(_localctx, 1);
			{
			setState(34);
			match(IDENTIFIER);
			setState(35);
			match(T__0);
			setState(36);
			argDefList();
			setState(37);
			match(T__1);
			setState(40);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==T__2) {
				{
				setState(38);
				match(T__2);
				setState(39);
				typeRef();
				}
			}

			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class TypeRefContext extends ParserRuleContext {
		public TypeRefContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_typeRef; }
	 
		public TypeRefContext() { }
		public void copyFrom(TypeRefContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class BuiltInCharTypeContext extends TypeRefContext {
		public BuiltInCharTypeContext(TypeRefContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ParadiseListener ) ((ParadiseListener)listener).enterBuiltInCharType(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ParadiseListener ) ((ParadiseListener)listener).exitBuiltInCharType(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ParadiseVisitor ) return ((ParadiseVisitor<? extends T>)visitor).visitBuiltInCharType(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class CustomTypeContext extends TypeRefContext {
		public TerminalNode IDENTIFIER() { return getToken(ParadiseParser.IDENTIFIER, 0); }
		public CustomTypeContext(TypeRefContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ParadiseListener ) ((ParadiseListener)listener).enterCustomType(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ParadiseListener ) ((ParadiseListener)listener).exitCustomType(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ParadiseVisitor ) return ((ParadiseVisitor<? extends T>)visitor).visitCustomType(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class BuiltInUlongTypeContext extends TypeRefContext {
		public BuiltInUlongTypeContext(TypeRefContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ParadiseListener ) ((ParadiseListener)listener).enterBuiltInUlongType(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ParadiseListener ) ((ParadiseListener)listener).exitBuiltInUlongType(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ParadiseVisitor ) return ((ParadiseVisitor<? extends T>)visitor).visitBuiltInUlongType(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class BuiltInStringTypeContext extends TypeRefContext {
		public BuiltInStringTypeContext(TypeRefContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ParadiseListener ) ((ParadiseListener)listener).enterBuiltInStringType(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ParadiseListener ) ((ParadiseListener)listener).exitBuiltInStringType(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ParadiseVisitor ) return ((ParadiseVisitor<? extends T>)visitor).visitBuiltInStringType(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class BuiltInLongTypeContext extends TypeRefContext {
		public BuiltInLongTypeContext(TypeRefContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ParadiseListener ) ((ParadiseListener)listener).enterBuiltInLongType(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ParadiseListener ) ((ParadiseListener)listener).exitBuiltInLongType(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ParadiseVisitor ) return ((ParadiseVisitor<? extends T>)visitor).visitBuiltInLongType(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class BuiltInByteTypeContext extends TypeRefContext {
		public BuiltInByteTypeContext(TypeRefContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ParadiseListener ) ((ParadiseListener)listener).enterBuiltInByteType(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ParadiseListener ) ((ParadiseListener)listener).exitBuiltInByteType(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ParadiseVisitor ) return ((ParadiseVisitor<? extends T>)visitor).visitBuiltInByteType(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class BuiltInBoolTypeContext extends TypeRefContext {
		public BuiltInBoolTypeContext(TypeRefContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ParadiseListener ) ((ParadiseListener)listener).enterBuiltInBoolType(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ParadiseListener ) ((ParadiseListener)listener).exitBuiltInBoolType(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ParadiseVisitor ) return ((ParadiseVisitor<? extends T>)visitor).visitBuiltInBoolType(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class BuiltInIntTypeContext extends TypeRefContext {
		public BuiltInIntTypeContext(TypeRefContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ParadiseListener ) ((ParadiseListener)listener).enterBuiltInIntType(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ParadiseListener ) ((ParadiseListener)listener).exitBuiltInIntType(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ParadiseVisitor ) return ((ParadiseVisitor<? extends T>)visitor).visitBuiltInIntType(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class BuiltInUintTypeContext extends TypeRefContext {
		public BuiltInUintTypeContext(TypeRefContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ParadiseListener ) ((ParadiseListener)listener).enterBuiltInUintType(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ParadiseListener ) ((ParadiseListener)listener).exitBuiltInUintType(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ParadiseVisitor ) return ((ParadiseVisitor<? extends T>)visitor).visitBuiltInUintType(this);
			else return visitor.visitChildren(this);
		}
	}

	public final TypeRefContext typeRef() throws RecognitionException {
		TypeRefContext _localctx = new TypeRefContext(_ctx, getState());
		enterRule(_localctx, 6, RULE_typeRef);
		try {
			setState(51);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case T__3:
				_localctx = new BuiltInBoolTypeContext(_localctx);
				enterOuterAlt(_localctx, 1);
				{
				setState(42);
				match(T__3);
				}
				break;
			case T__4:
				_localctx = new BuiltInByteTypeContext(_localctx);
				enterOuterAlt(_localctx, 2);
				{
				setState(43);
				match(T__4);
				}
				break;
			case T__5:
				_localctx = new BuiltInIntTypeContext(_localctx);
				enterOuterAlt(_localctx, 3);
				{
				setState(44);
				match(T__5);
				}
				break;
			case T__6:
				_localctx = new BuiltInUintTypeContext(_localctx);
				enterOuterAlt(_localctx, 4);
				{
				setState(45);
				match(T__6);
				}
				break;
			case T__7:
				_localctx = new BuiltInLongTypeContext(_localctx);
				enterOuterAlt(_localctx, 5);
				{
				setState(46);
				match(T__7);
				}
				break;
			case T__8:
				_localctx = new BuiltInUlongTypeContext(_localctx);
				enterOuterAlt(_localctx, 6);
				{
				setState(47);
				match(T__8);
				}
				break;
			case T__9:
				_localctx = new BuiltInCharTypeContext(_localctx);
				enterOuterAlt(_localctx, 7);
				{
				setState(48);
				match(T__9);
				}
				break;
			case T__10:
				_localctx = new BuiltInStringTypeContext(_localctx);
				enterOuterAlt(_localctx, 8);
				{
				setState(49);
				match(T__10);
				}
				break;
			case IDENTIFIER:
				_localctx = new CustomTypeContext(_localctx);
				enterOuterAlt(_localctx, 9);
				{
				setState(50);
				match(IDENTIFIER);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class StatementContext extends ParserRuleContext {
		public StatementContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_statement; }
	 
		public StatementContext() { }
		public void copyFrom(StatementContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class BreakStatementContext extends StatementContext {
		public BreakStatementContext(StatementContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ParadiseListener ) ((ParadiseListener)listener).enterBreakStatement(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ParadiseListener ) ((ParadiseListener)listener).exitBreakStatement(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ParadiseVisitor ) return ((ParadiseVisitor<? extends T>)visitor).visitBreakStatement(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class IfStatementContext extends StatementContext {
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public List<StatementContext> statement() {
			return getRuleContexts(StatementContext.class);
		}
		public StatementContext statement(int i) {
			return getRuleContext(StatementContext.class,i);
		}
		public IfStatementContext(StatementContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ParadiseListener ) ((ParadiseListener)listener).enterIfStatement(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ParadiseListener ) ((ParadiseListener)listener).exitIfStatement(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ParadiseVisitor ) return ((ParadiseVisitor<? extends T>)visitor).visitIfStatement(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class AssignmentStatementContext extends StatementContext {
		public TerminalNode IDENTIFIER() { return getToken(ParadiseParser.IDENTIFIER, 0); }
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public AssignmentStatementContext(StatementContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ParadiseListener ) ((ParadiseListener)listener).enterAssignmentStatement(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ParadiseListener ) ((ParadiseListener)listener).exitAssignmentStatement(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ParadiseVisitor ) return ((ParadiseVisitor<? extends T>)visitor).visitAssignmentStatement(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class ExpressionStatementContext extends StatementContext {
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public ExpressionStatementContext(StatementContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ParadiseListener ) ((ParadiseListener)listener).enterExpressionStatement(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ParadiseListener ) ((ParadiseListener)listener).exitExpressionStatement(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ParadiseVisitor ) return ((ParadiseVisitor<? extends T>)visitor).visitExpressionStatement(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class WhileStatementContext extends StatementContext {
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public List<StatementContext> statement() {
			return getRuleContexts(StatementContext.class);
		}
		public StatementContext statement(int i) {
			return getRuleContext(StatementContext.class,i);
		}
		public WhileStatementContext(StatementContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ParadiseListener ) ((ParadiseListener)listener).enterWhileStatement(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ParadiseListener ) ((ParadiseListener)listener).exitWhileStatement(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ParadiseVisitor ) return ((ParadiseVisitor<? extends T>)visitor).visitWhileStatement(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class DoWhileStatementContext extends StatementContext {
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public List<StatementContext> statement() {
			return getRuleContexts(StatementContext.class);
		}
		public StatementContext statement(int i) {
			return getRuleContext(StatementContext.class,i);
		}
		public DoWhileStatementContext(StatementContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ParadiseListener ) ((ParadiseListener)listener).enterDoWhileStatement(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ParadiseListener ) ((ParadiseListener)listener).exitDoWhileStatement(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ParadiseVisitor ) return ((ParadiseVisitor<? extends T>)visitor).visitDoWhileStatement(this);
			else return visitor.visitChildren(this);
		}
	}

	public final StatementContext statement() throws RecognitionException {
		StatementContext _localctx = new StatementContext(_ctx, getState());
		enterRule(_localctx, 8, RULE_statement);
		int _la;
		try {
			setState(109);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,10,_ctx) ) {
			case 1:
				_localctx = new IfStatementContext(_localctx);
				enterOuterAlt(_localctx, 1);
				{
				setState(53);
				match(T__11);
				setState(54);
				expr(0);
				setState(55);
				match(T__12);
				setState(59);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__11) | (1L << T__15) | (1L << T__17) | (1L << T__20) | (1L << UNARY) | (1L << CHAR) | (1L << STRING) | (1L << HEX) | (1L << BITS) | (1L << DEC) | (1L << BOOL) | (1L << IDENTIFIER))) != 0)) {
					{
					{
					setState(56);
					statement();
					}
					}
					setState(61);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				setState(71);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==T__13) {
					{
					{
					setState(62);
					match(T__13);
					setState(66);
					_errHandler.sync(this);
					_la = _input.LA(1);
					while ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__11) | (1L << T__15) | (1L << T__17) | (1L << T__20) | (1L << UNARY) | (1L << CHAR) | (1L << STRING) | (1L << HEX) | (1L << BITS) | (1L << DEC) | (1L << BOOL) | (1L << IDENTIFIER))) != 0)) {
						{
						{
						setState(63);
						statement();
						}
						}
						setState(68);
						_errHandler.sync(this);
						_la = _input.LA(1);
					}
					}
					}
					setState(73);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				setState(74);
				match(T__14);
				}
				break;
			case 2:
				_localctx = new WhileStatementContext(_localctx);
				enterOuterAlt(_localctx, 2);
				{
				setState(76);
				match(T__15);
				setState(77);
				expr(0);
				setState(81);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__11) | (1L << T__15) | (1L << T__17) | (1L << T__20) | (1L << UNARY) | (1L << CHAR) | (1L << STRING) | (1L << HEX) | (1L << BITS) | (1L << DEC) | (1L << BOOL) | (1L << IDENTIFIER))) != 0)) {
					{
					{
					setState(78);
					statement();
					}
					}
					setState(83);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				setState(84);
				match(T__16);
				}
				break;
			case 3:
				_localctx = new DoWhileStatementContext(_localctx);
				enterOuterAlt(_localctx, 3);
				{
				setState(86);
				match(T__17);
				setState(90);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__11) | (1L << T__15) | (1L << T__17) | (1L << T__20) | (1L << UNARY) | (1L << CHAR) | (1L << STRING) | (1L << HEX) | (1L << BITS) | (1L << DEC) | (1L << BOOL) | (1L << IDENTIFIER))) != 0)) {
					{
					{
					setState(87);
					statement();
					}
					}
					setState(92);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				setState(93);
				match(T__18);
				setState(95); 
				_errHandler.sync(this);
				_la = _input.LA(1);
				do {
					{
					{
					setState(94);
					_la = _input.LA(1);
					if ( !(_la==T__15 || _la==T__19) ) {
					_errHandler.recoverInline(this);
					}
					else {
						if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
						_errHandler.reportMatch(this);
						consume();
					}
					}
					}
					setState(97); 
					_errHandler.sync(this);
					_la = _input.LA(1);
				} while ( _la==T__15 || _la==T__19 );
				setState(99);
				expr(0);
				}
				break;
			case 4:
				_localctx = new BreakStatementContext(_localctx);
				enterOuterAlt(_localctx, 4);
				{
				setState(100);
				match(T__20);
				}
				break;
			case 5:
				_localctx = new AssignmentStatementContext(_localctx);
				enterOuterAlt(_localctx, 5);
				{
				setState(101);
				match(IDENTIFIER);
				setState(102);
				match(T__21);
				setState(103);
				expr(0);
				setState(104);
				match(T__22);
				}
				break;
			case 6:
				_localctx = new ExpressionStatementContext(_localctx);
				enterOuterAlt(_localctx, 6);
				{
				setState(106);
				expr(0);
				setState(107);
				match(T__22);
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ExprContext extends ParserRuleContext {
		public ExprContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_expr; }
	 
		public ExprContext() { }
		public void copyFrom(ExprContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class BinaryExpressionContext extends ExprContext {
		public List<ExprContext> expr() {
			return getRuleContexts(ExprContext.class);
		}
		public ExprContext expr(int i) {
			return getRuleContext(ExprContext.class,i);
		}
		public TerminalNode BINARY() { return getToken(ParadiseParser.BINARY, 0); }
		public BinaryExpressionContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ParadiseListener ) ((ParadiseListener)listener).enterBinaryExpression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ParadiseListener ) ((ParadiseListener)listener).exitBinaryExpression(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ParadiseVisitor ) return ((ParadiseVisitor<? extends T>)visitor).visitBinaryExpression(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class PrefixUnaryExpressionContext extends ExprContext {
		public TerminalNode UNARY() { return getToken(ParadiseParser.UNARY, 0); }
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public PrefixUnaryExpressionContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ParadiseListener ) ((ParadiseListener)listener).enterPrefixUnaryExpression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ParadiseListener ) ((ParadiseListener)listener).exitPrefixUnaryExpression(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ParadiseVisitor ) return ((ParadiseVisitor<? extends T>)visitor).visitPrefixUnaryExpression(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class LiteralExpressionContext extends ExprContext {
		public TerminalNode BOOL() { return getToken(ParadiseParser.BOOL, 0); }
		public TerminalNode STRING() { return getToken(ParadiseParser.STRING, 0); }
		public TerminalNode CHAR() { return getToken(ParadiseParser.CHAR, 0); }
		public TerminalNode HEX() { return getToken(ParadiseParser.HEX, 0); }
		public TerminalNode BITS() { return getToken(ParadiseParser.BITS, 0); }
		public TerminalNode DEC() { return getToken(ParadiseParser.DEC, 0); }
		public LiteralExpressionContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ParadiseListener ) ((ParadiseListener)listener).enterLiteralExpression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ParadiseListener ) ((ParadiseListener)listener).exitLiteralExpression(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ParadiseVisitor ) return ((ParadiseVisitor<? extends T>)visitor).visitLiteralExpression(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class PostfixUnaryExpressionContext extends ExprContext {
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public TerminalNode UNARY() { return getToken(ParadiseParser.UNARY, 0); }
		public PostfixUnaryExpressionContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ParadiseListener ) ((ParadiseListener)listener).enterPostfixUnaryExpression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ParadiseListener ) ((ParadiseListener)listener).exitPostfixUnaryExpression(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ParadiseVisitor ) return ((ParadiseVisitor<? extends T>)visitor).visitPostfixUnaryExpression(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class PlaceExpressionContext extends ExprContext {
		public TerminalNode IDENTIFIER() { return getToken(ParadiseParser.IDENTIFIER, 0); }
		public PlaceExpressionContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ParadiseListener ) ((ParadiseListener)listener).enterPlaceExpression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ParadiseListener ) ((ParadiseListener)listener).exitPlaceExpression(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ParadiseVisitor ) return ((ParadiseVisitor<? extends T>)visitor).visitPlaceExpression(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class CallExpressionContext extends ExprContext {
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public ExprListContext exprList() {
			return getRuleContext(ExprListContext.class,0);
		}
		public CallExpressionContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ParadiseListener ) ((ParadiseListener)listener).enterCallExpression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ParadiseListener ) ((ParadiseListener)listener).exitCallExpression(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ParadiseVisitor ) return ((ParadiseVisitor<? extends T>)visitor).visitCallExpression(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ExprContext expr() throws RecognitionException {
		return expr(0);
	}

	private ExprContext expr(int _p) throws RecognitionException {
		ParserRuleContext _parentctx = _ctx;
		int _parentState = getState();
		ExprContext _localctx = new ExprContext(_ctx, _parentState);
		ExprContext _prevctx = _localctx;
		int _startState = 10;
		enterRecursionRule(_localctx, 10, RULE_expr, _p);
		int _la;
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(116);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case CHAR:
			case STRING:
			case HEX:
			case BITS:
			case DEC:
			case BOOL:
				{
				_localctx = new LiteralExpressionContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;

				setState(112);
				_la = _input.LA(1);
				if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << CHAR) | (1L << STRING) | (1L << HEX) | (1L << BITS) | (1L << DEC) | (1L << BOOL))) != 0)) ) {
				_errHandler.recoverInline(this);
				}
				else {
					if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
					_errHandler.reportMatch(this);
					consume();
				}
				}
				break;
			case UNARY:
				{
				_localctx = new PrefixUnaryExpressionContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(113);
				match(UNARY);
				setState(114);
				expr(4);
				}
				break;
			case IDENTIFIER:
				{
				_localctx = new PlaceExpressionContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(115);
				match(IDENTIFIER);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
			_ctx.stop = _input.LT(-1);
			setState(130);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,13,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					if ( _parseListeners!=null ) triggerExitRuleEvent();
					_prevctx = _localctx;
					{
					setState(128);
					_errHandler.sync(this);
					switch ( getInterpreter().adaptivePredict(_input,12,_ctx) ) {
					case 1:
						{
						_localctx = new BinaryExpressionContext(new ExprContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_expr);
						setState(118);
						if (!(precpred(_ctx, 6))) throw new FailedPredicateException(this, "precpred(_ctx, 6)");
						setState(119);
						match(BINARY);
						setState(120);
						expr(7);
						}
						break;
					case 2:
						{
						_localctx = new PostfixUnaryExpressionContext(new ExprContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_expr);
						setState(121);
						if (!(precpred(_ctx, 3))) throw new FailedPredicateException(this, "precpred(_ctx, 3)");
						setState(122);
						match(UNARY);
						}
						break;
					case 3:
						{
						_localctx = new CallExpressionContext(new ExprContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_expr);
						setState(123);
						if (!(precpred(_ctx, 2))) throw new FailedPredicateException(this, "precpred(_ctx, 2)");
						setState(124);
						match(T__0);
						setState(125);
						exprList();
						setState(126);
						match(T__1);
						}
						break;
					}
					} 
				}
				setState(132);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,13,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			unrollRecursionContexts(_parentctx);
		}
		return _localctx;
	}

	public static class ArgDefContext extends ParserRuleContext {
		public TerminalNode IDENTIFIER() { return getToken(ParadiseParser.IDENTIFIER, 0); }
		public TypeRefContext typeRef() {
			return getRuleContext(TypeRefContext.class,0);
		}
		public ArgDefContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_argDef; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ParadiseListener ) ((ParadiseListener)listener).enterArgDef(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ParadiseListener ) ((ParadiseListener)listener).exitArgDef(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ParadiseVisitor ) return ((ParadiseVisitor<? extends T>)visitor).visitArgDef(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ArgDefContext argDef() throws RecognitionException {
		ArgDefContext _localctx = new ArgDefContext(_ctx, getState());
		enterRule(_localctx, 12, RULE_argDef);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(133);
			match(IDENTIFIER);
			setState(134);
			match(T__2);
			setState(135);
			typeRef();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ArgDefListContext extends ParserRuleContext {
		public List<ArgDefContext> argDef() {
			return getRuleContexts(ArgDefContext.class);
		}
		public ArgDefContext argDef(int i) {
			return getRuleContext(ArgDefContext.class,i);
		}
		public ArgDefListContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_argDefList; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ParadiseListener ) ((ParadiseListener)listener).enterArgDefList(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ParadiseListener ) ((ParadiseListener)listener).exitArgDefList(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ParadiseVisitor ) return ((ParadiseVisitor<? extends T>)visitor).visitArgDefList(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ArgDefListContext argDefList() throws RecognitionException {
		ArgDefListContext _localctx = new ArgDefListContext(_ctx, getState());
		enterRule(_localctx, 14, RULE_argDefList);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(145);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==IDENTIFIER) {
				{
				setState(137);
				argDef();
				setState(142);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==T__23) {
					{
					{
					setState(138);
					match(T__23);
					setState(139);
					argDef();
					}
					}
					setState(144);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				}
			}

			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ExprListContext extends ParserRuleContext {
		public List<ExprContext> expr() {
			return getRuleContexts(ExprContext.class);
		}
		public ExprContext expr(int i) {
			return getRuleContext(ExprContext.class,i);
		}
		public ExprListContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_exprList; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ParadiseListener ) ((ParadiseListener)listener).enterExprList(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ParadiseListener ) ((ParadiseListener)listener).exitExprList(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ParadiseVisitor ) return ((ParadiseVisitor<? extends T>)visitor).visitExprList(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ExprListContext exprList() throws RecognitionException {
		ExprListContext _localctx = new ExprListContext(_ctx, getState());
		enterRule(_localctx, 16, RULE_exprList);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(155);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << UNARY) | (1L << CHAR) | (1L << STRING) | (1L << HEX) | (1L << BITS) | (1L << DEC) | (1L << BOOL) | (1L << IDENTIFIER))) != 0)) {
				{
				setState(147);
				expr(0);
				setState(152);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==T__23) {
					{
					{
					setState(148);
					match(T__23);
					setState(149);
					expr(0);
					}
					}
					setState(154);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				}
			}

			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public boolean sempred(RuleContext _localctx, int ruleIndex, int predIndex) {
		switch (ruleIndex) {
		case 5:
			return expr_sempred((ExprContext)_localctx, predIndex);
		}
		return true;
	}
	private boolean expr_sempred(ExprContext _localctx, int predIndex) {
		switch (predIndex) {
		case 0:
			return precpred(_ctx, 6);
		case 1:
			return precpred(_ctx, 3);
		case 2:
			return precpred(_ctx, 2);
		}
		return true;
	}

	public static final String _serializedATN =
		"\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\3\'\u00a0\4\2\t\2\4"+
		"\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b\t\b\4\t\t\t\4\n\t\n\3\2\7\2"+
		"\26\n\2\f\2\16\2\31\13\2\3\3\3\3\3\3\7\3\36\n\3\f\3\16\3!\13\3\3\3\3\3"+
		"\3\4\3\4\3\4\3\4\3\4\3\4\5\4+\n\4\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5"+
		"\5\5\66\n\5\3\6\3\6\3\6\3\6\7\6<\n\6\f\6\16\6?\13\6\3\6\3\6\7\6C\n\6\f"+
		"\6\16\6F\13\6\7\6H\n\6\f\6\16\6K\13\6\3\6\3\6\3\6\3\6\3\6\7\6R\n\6\f\6"+
		"\16\6U\13\6\3\6\3\6\3\6\3\6\7\6[\n\6\f\6\16\6^\13\6\3\6\3\6\6\6b\n\6\r"+
		"\6\16\6c\3\6\3\6\3\6\3\6\3\6\3\6\3\6\3\6\3\6\3\6\5\6p\n\6\3\7\3\7\3\7"+
		"\3\7\3\7\5\7w\n\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\7\7\u0083\n"+
		"\7\f\7\16\7\u0086\13\7\3\b\3\b\3\b\3\b\3\t\3\t\3\t\7\t\u008f\n\t\f\t\16"+
		"\t\u0092\13\t\5\t\u0094\n\t\3\n\3\n\3\n\7\n\u0099\n\n\f\n\16\n\u009c\13"+
		"\n\5\n\u009e\n\n\3\n\2\3\f\13\2\4\6\b\n\f\16\20\22\2\4\4\2\22\22\26\26"+
		"\3\2\37$\2\u00b5\2\27\3\2\2\2\4\32\3\2\2\2\6$\3\2\2\2\b\65\3\2\2\2\no"+
		"\3\2\2\2\fv\3\2\2\2\16\u0087\3\2\2\2\20\u0093\3\2\2\2\22\u009d\3\2\2\2"+
		"\24\26\5\4\3\2\25\24\3\2\2\2\26\31\3\2\2\2\27\25\3\2\2\2\27\30\3\2\2\2"+
		"\30\3\3\2\2\2\31\27\3\2\2\2\32\33\7\33\2\2\33\37\5\6\4\2\34\36\5\n\6\2"+
		"\35\34\3\2\2\2\36!\3\2\2\2\37\35\3\2\2\2\37 \3\2\2\2 \"\3\2\2\2!\37\3"+
		"\2\2\2\"#\7\34\2\2#\5\3\2\2\2$%\7%\2\2%&\7\3\2\2&\'\5\20\t\2\'*\7\4\2"+
		"\2()\7\5\2\2)+\5\b\5\2*(\3\2\2\2*+\3\2\2\2+\7\3\2\2\2,\66\7\6\2\2-\66"+
		"\7\7\2\2.\66\7\b\2\2/\66\7\t\2\2\60\66\7\n\2\2\61\66\7\13\2\2\62\66\7"+
		"\f\2\2\63\66\7\r\2\2\64\66\7%\2\2\65,\3\2\2\2\65-\3\2\2\2\65.\3\2\2\2"+
		"\65/\3\2\2\2\65\60\3\2\2\2\65\61\3\2\2\2\65\62\3\2\2\2\65\63\3\2\2\2\65"+
		"\64\3\2\2\2\66\t\3\2\2\2\678\7\16\2\289\5\f\7\29=\7\17\2\2:<\5\n\6\2;"+
		":\3\2\2\2<?\3\2\2\2=;\3\2\2\2=>\3\2\2\2>I\3\2\2\2?=\3\2\2\2@D\7\20\2\2"+
		"AC\5\n\6\2BA\3\2\2\2CF\3\2\2\2DB\3\2\2\2DE\3\2\2\2EH\3\2\2\2FD\3\2\2\2"+
		"G@\3\2\2\2HK\3\2\2\2IG\3\2\2\2IJ\3\2\2\2JL\3\2\2\2KI\3\2\2\2LM\7\21\2"+
		"\2Mp\3\2\2\2NO\7\22\2\2OS\5\f\7\2PR\5\n\6\2QP\3\2\2\2RU\3\2\2\2SQ\3\2"+
		"\2\2ST\3\2\2\2TV\3\2\2\2US\3\2\2\2VW\7\23\2\2Wp\3\2\2\2X\\\7\24\2\2Y["+
		"\5\n\6\2ZY\3\2\2\2[^\3\2\2\2\\Z\3\2\2\2\\]\3\2\2\2]_\3\2\2\2^\\\3\2\2"+
		"\2_a\7\25\2\2`b\t\2\2\2a`\3\2\2\2bc\3\2\2\2ca\3\2\2\2cd\3\2\2\2de\3\2"+
		"\2\2ep\5\f\7\2fp\7\27\2\2gh\7%\2\2hi\7\30\2\2ij\5\f\7\2jk\7\31\2\2kp\3"+
		"\2\2\2lm\5\f\7\2mn\7\31\2\2np\3\2\2\2o\67\3\2\2\2oN\3\2\2\2oX\3\2\2\2"+
		"of\3\2\2\2og\3\2\2\2ol\3\2\2\2p\13\3\2\2\2qr\b\7\1\2rw\t\3\2\2st\7\36"+
		"\2\2tw\5\f\7\6uw\7%\2\2vq\3\2\2\2vs\3\2\2\2vu\3\2\2\2w\u0084\3\2\2\2x"+
		"y\f\b\2\2yz\7\35\2\2z\u0083\5\f\7\t{|\f\5\2\2|\u0083\7\36\2\2}~\f\4\2"+
		"\2~\177\7\3\2\2\177\u0080\5\22\n\2\u0080\u0081\7\4\2\2\u0081\u0083\3\2"+
		"\2\2\u0082x\3\2\2\2\u0082{\3\2\2\2\u0082}\3\2\2\2\u0083\u0086\3\2\2\2"+
		"\u0084\u0082\3\2\2\2\u0084\u0085\3\2\2\2\u0085\r\3\2\2\2\u0086\u0084\3"+
		"\2\2\2\u0087\u0088\7%\2\2\u0088\u0089\7\5\2\2\u0089\u008a\5\b\5\2\u008a"+
		"\17\3\2\2\2\u008b\u0090\5\16\b\2\u008c\u008d\7\32\2\2\u008d\u008f\5\16"+
		"\b\2\u008e\u008c\3\2\2\2\u008f\u0092\3\2\2\2\u0090\u008e\3\2\2\2\u0090"+
		"\u0091\3\2\2\2\u0091\u0094\3\2\2\2\u0092\u0090\3\2\2\2\u0093\u008b\3\2"+
		"\2\2\u0093\u0094\3\2\2\2\u0094\21\3\2\2\2\u0095\u009a\5\f\7\2\u0096\u0097"+
		"\7\32\2\2\u0097\u0099\5\f\7\2\u0098\u0096\3\2\2\2\u0099\u009c\3\2\2\2"+
		"\u009a\u0098\3\2\2\2\u009a\u009b\3\2\2\2\u009b\u009e\3\2\2\2\u009c\u009a"+
		"\3\2\2\2\u009d\u0095\3\2\2\2\u009d\u009e\3\2\2\2\u009e\23\3\2\2\2\24\27"+
		"\37*\65=DIS\\cov\u0082\u0084\u0090\u0093\u009a\u009d";
	public static final ATN _ATN =
		new ATNDeserializer().deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}