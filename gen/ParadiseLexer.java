// Generated from /Users/tva/ITMO_old/program_models_verification/Paradise.g4 by ANTLR 4.7
import org.antlr.v4.runtime.Lexer;
import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.Token;
import org.antlr.v4.runtime.TokenStream;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.misc.*;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class ParadiseLexer extends Lexer {
	static { RuntimeMetaData.checkVersion("4.7", RuntimeMetaData.VERSION); }

	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		T__0=1, T__1=2, T__2=3, T__3=4, T__4=5, T__5=6, T__6=7, T__7=8, T__8=9, 
		T__9=10, T__10=11, T__11=12, T__12=13, T__13=14, T__14=15, T__15=16, T__16=17, 
		T__17=18, T__18=19, T__19=20, T__20=21, T__21=22, T__22=23, T__23=24, 
		LBRACE=25, FUNCTION=26, END_FUNCTION=27, BINARY=28, UNARY=29, CHAR=30, 
		STRING=31, HEX=32, BITS=33, DEC=34, BOOL=35, IDENTIFIER=36, COMMENT=37, 
		WS=38;
	public static String[] channelNames = {
		"DEFAULT_TOKEN_CHANNEL", "HIDDEN"
	};

	public static String[] modeNames = {
		"DEFAULT_MODE"
	};

	public static final String[] ruleNames = {
		"T__0", "T__1", "T__2", "T__3", "T__4", "T__5", "T__6", "T__7", "T__8", 
		"T__9", "T__10", "T__11", "T__12", "T__13", "T__14", "T__15", "T__16", 
		"T__17", "T__18", "T__19", "T__20", "T__21", "T__22", "T__23", "LBRACE", 
		"LETTERS", "DIGIT", "FUNCTION", "END_FUNCTION", "BINARY", "UNARY", "CHAR", 
		"STRING", "HEX", "BITS", "DEC", "BOOL", "IDENTIFIER", "COMMENT", "WS"
	};

	private static final String[] _LITERAL_NAMES = {
		null, "')'", "'as'", "'bool'", "'byte'", "'int'", "'uint'", "'long'", 
		"'ulong'", "'char'", "'string'", "'if'", "'then'", "'else'", "'end if'", 
		"'while'", "'wend'", "'do'", "'loop'", "'until'", "'break'", "';'", "'='", 
		"'return'", "','", "'('", "'function'", "'end function'"
	};
	private static final String[] _SYMBOLIC_NAMES = {
		null, null, null, null, null, null, null, null, null, null, null, null, 
		null, null, null, null, null, null, null, null, null, null, null, null, 
		null, "LBRACE", "FUNCTION", "END_FUNCTION", "BINARY", "UNARY", "CHAR", 
		"STRING", "HEX", "BITS", "DEC", "BOOL", "IDENTIFIER", "COMMENT", "WS"
	};
	public static final Vocabulary VOCABULARY = new VocabularyImpl(_LITERAL_NAMES, _SYMBOLIC_NAMES);

	/**
	 * @deprecated Use {@link #VOCABULARY} instead.
	 */
	@Deprecated
	public static final String[] tokenNames;
	static {
		tokenNames = new String[_SYMBOLIC_NAMES.length];
		for (int i = 0; i < tokenNames.length; i++) {
			tokenNames[i] = VOCABULARY.getLiteralName(i);
			if (tokenNames[i] == null) {
				tokenNames[i] = VOCABULARY.getSymbolicName(i);
			}

			if (tokenNames[i] == null) {
				tokenNames[i] = "<INVALID>";
			}
		}
	}

	@Override
	@Deprecated
	public String[] getTokenNames() {
		return tokenNames;
	}

	@Override

	public Vocabulary getVocabulary() {
		return VOCABULARY;
	}


	public ParadiseLexer(CharStream input) {
		super(input);
		_interp = new LexerATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}

	@Override
	public String getGrammarFileName() { return "Paradise.g4"; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String getSerializedATN() { return _serializedATN; }

	@Override
	public String[] getChannelNames() { return channelNames; }

	@Override
	public String[] getModeNames() { return modeNames; }

	@Override
	public ATN getATN() { return _ATN; }

	public static final String _serializedATN =
		"\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\2(\u013d\b\1\4\2\t"+
		"\2\4\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b\t\b\4\t\t\t\4\n\t\n\4\13"+
		"\t\13\4\f\t\f\4\r\t\r\4\16\t\16\4\17\t\17\4\20\t\20\4\21\t\21\4\22\t\22"+
		"\4\23\t\23\4\24\t\24\4\25\t\25\4\26\t\26\4\27\t\27\4\30\t\30\4\31\t\31"+
		"\4\32\t\32\4\33\t\33\4\34\t\34\4\35\t\35\4\36\t\36\4\37\t\37\4 \t \4!"+
		"\t!\4\"\t\"\4#\t#\4$\t$\4%\t%\4&\t&\4\'\t\'\4(\t(\4)\t)\3\2\3\2\3\3\3"+
		"\3\3\3\3\4\3\4\3\4\3\4\3\4\3\5\3\5\3\5\3\5\3\5\3\6\3\6\3\6\3\6\3\7\3\7"+
		"\3\7\3\7\3\7\3\b\3\b\3\b\3\b\3\b\3\t\3\t\3\t\3\t\3\t\3\t\3\n\3\n\3\n\3"+
		"\n\3\n\3\13\3\13\3\13\3\13\3\13\3\13\3\13\3\f\3\f\3\f\3\r\3\r\3\r\3\r"+
		"\3\r\3\16\3\16\3\16\3\16\3\16\3\17\3\17\3\17\3\17\3\17\3\17\3\17\3\20"+
		"\3\20\3\20\3\20\3\20\3\20\3\21\3\21\3\21\3\21\3\21\3\22\3\22\3\22\3\23"+
		"\3\23\3\23\3\23\3\23\3\24\3\24\3\24\3\24\3\24\3\24\3\25\3\25\3\25\3\25"+
		"\3\25\3\25\3\26\3\26\3\27\3\27\3\30\3\30\3\30\3\30\3\30\3\30\3\30\3\31"+
		"\3\31\3\32\3\32\3\33\6\33\u00c6\n\33\r\33\16\33\u00c7\3\34\6\34\u00cb"+
		"\n\34\r\34\16\34\u00cc\3\35\3\35\3\35\3\35\3\35\3\35\3\35\3\35\3\35\3"+
		"\36\3\36\3\36\3\36\3\36\3\36\3\36\3\36\3\36\3\36\3\36\3\36\3\36\3\37\3"+
		"\37\3\37\3\37\3\37\3\37\3\37\3\37\3\37\5\37\u00ee\n\37\3 \3 \3 \3 \3 "+
		"\5 \u00f5\n \3!\3!\5!\u00f9\n!\3!\3!\3\"\3\"\7\"\u00ff\n\"\f\"\16\"\u0102"+
		"\13\"\3\"\3\"\3#\3#\3#\6#\u0109\n#\r#\16#\u010a\3$\3$\3$\6$\u0110\n$\r"+
		"$\16$\u0111\3%\6%\u0115\n%\r%\16%\u0116\3&\3&\3&\3&\3&\3&\3&\3&\3&\5&"+
		"\u0122\n&\3\'\3\'\3\'\7\'\u0127\n\'\f\'\16\'\u012a\13\'\3(\3(\7(\u012e"+
		"\n(\f(\16(\u0131\13(\3(\3(\3(\3(\3)\6)\u0138\n)\r)\16)\u0139\3)\3)\4\u0100"+
		"\u012f\2*\3\3\5\4\7\5\t\6\13\7\r\b\17\t\21\n\23\13\25\f\27\r\31\16\33"+
		"\17\35\20\37\21!\22#\23%\24\'\25)\26+\27-\30/\31\61\32\63\33\65\2\67\2"+
		"9\34;\35=\36?\37A C!E\"G#I$K%M&O\'Q(\3\2\f\5\2C\\aac|\3\2\62;\6\2((,-"+
		"//\61\61\5\2\'\'>>@@\5\2##--//\4\2ZZzz\5\2\62;CHch\4\2DDdd\3\2\62\63\5"+
		"\2\13\f\17\17\"\"\2\u014d\2\3\3\2\2\2\2\5\3\2\2\2\2\7\3\2\2\2\2\t\3\2"+
		"\2\2\2\13\3\2\2\2\2\r\3\2\2\2\2\17\3\2\2\2\2\21\3\2\2\2\2\23\3\2\2\2\2"+
		"\25\3\2\2\2\2\27\3\2\2\2\2\31\3\2\2\2\2\33\3\2\2\2\2\35\3\2\2\2\2\37\3"+
		"\2\2\2\2!\3\2\2\2\2#\3\2\2\2\2%\3\2\2\2\2\'\3\2\2\2\2)\3\2\2\2\2+\3\2"+
		"\2\2\2-\3\2\2\2\2/\3\2\2\2\2\61\3\2\2\2\2\63\3\2\2\2\29\3\2\2\2\2;\3\2"+
		"\2\2\2=\3\2\2\2\2?\3\2\2\2\2A\3\2\2\2\2C\3\2\2\2\2E\3\2\2\2\2G\3\2\2\2"+
		"\2I\3\2\2\2\2K\3\2\2\2\2M\3\2\2\2\2O\3\2\2\2\2Q\3\2\2\2\3S\3\2\2\2\5U"+
		"\3\2\2\2\7X\3\2\2\2\t]\3\2\2\2\13b\3\2\2\2\rf\3\2\2\2\17k\3\2\2\2\21p"+
		"\3\2\2\2\23v\3\2\2\2\25{\3\2\2\2\27\u0082\3\2\2\2\31\u0085\3\2\2\2\33"+
		"\u008a\3\2\2\2\35\u008f\3\2\2\2\37\u0096\3\2\2\2!\u009c\3\2\2\2#\u00a1"+
		"\3\2\2\2%\u00a4\3\2\2\2\'\u00a9\3\2\2\2)\u00af\3\2\2\2+\u00b5\3\2\2\2"+
		"-\u00b7\3\2\2\2/\u00b9\3\2\2\2\61\u00c0\3\2\2\2\63\u00c2\3\2\2\2\65\u00c5"+
		"\3\2\2\2\67\u00ca\3\2\2\29\u00ce\3\2\2\2;\u00d7\3\2\2\2=\u00ed\3\2\2\2"+
		"?\u00f4\3\2\2\2A\u00f6\3\2\2\2C\u00fc\3\2\2\2E\u0105\3\2\2\2G\u010c\3"+
		"\2\2\2I\u0114\3\2\2\2K\u0121\3\2\2\2M\u0123\3\2\2\2O\u012b\3\2\2\2Q\u0137"+
		"\3\2\2\2ST\7+\2\2T\4\3\2\2\2UV\7c\2\2VW\7u\2\2W\6\3\2\2\2XY\7d\2\2YZ\7"+
		"q\2\2Z[\7q\2\2[\\\7n\2\2\\\b\3\2\2\2]^\7d\2\2^_\7{\2\2_`\7v\2\2`a\7g\2"+
		"\2a\n\3\2\2\2bc\7k\2\2cd\7p\2\2de\7v\2\2e\f\3\2\2\2fg\7w\2\2gh\7k\2\2"+
		"hi\7p\2\2ij\7v\2\2j\16\3\2\2\2kl\7n\2\2lm\7q\2\2mn\7p\2\2no\7i\2\2o\20"+
		"\3\2\2\2pq\7w\2\2qr\7n\2\2rs\7q\2\2st\7p\2\2tu\7i\2\2u\22\3\2\2\2vw\7"+
		"e\2\2wx\7j\2\2xy\7c\2\2yz\7t\2\2z\24\3\2\2\2{|\7u\2\2|}\7v\2\2}~\7t\2"+
		"\2~\177\7k\2\2\177\u0080\7p\2\2\u0080\u0081\7i\2\2\u0081\26\3\2\2\2\u0082"+
		"\u0083\7k\2\2\u0083\u0084\7h\2\2\u0084\30\3\2\2\2\u0085\u0086\7v\2\2\u0086"+
		"\u0087\7j\2\2\u0087\u0088\7g\2\2\u0088\u0089\7p\2\2\u0089\32\3\2\2\2\u008a"+
		"\u008b\7g\2\2\u008b\u008c\7n\2\2\u008c\u008d\7u\2\2\u008d\u008e\7g\2\2"+
		"\u008e\34\3\2\2\2\u008f\u0090\7g\2\2\u0090\u0091\7p\2\2\u0091\u0092\7"+
		"f\2\2\u0092\u0093\7\"\2\2\u0093\u0094\7k\2\2\u0094\u0095\7h\2\2\u0095"+
		"\36\3\2\2\2\u0096\u0097\7y\2\2\u0097\u0098\7j\2\2\u0098\u0099\7k\2\2\u0099"+
		"\u009a\7n\2\2\u009a\u009b\7g\2\2\u009b \3\2\2\2\u009c\u009d\7y\2\2\u009d"+
		"\u009e\7g\2\2\u009e\u009f\7p\2\2\u009f\u00a0\7f\2\2\u00a0\"\3\2\2\2\u00a1"+
		"\u00a2\7f\2\2\u00a2\u00a3\7q\2\2\u00a3$\3\2\2\2\u00a4\u00a5\7n\2\2\u00a5"+
		"\u00a6\7q\2\2\u00a6\u00a7\7q\2\2\u00a7\u00a8\7r\2\2\u00a8&\3\2\2\2\u00a9"+
		"\u00aa\7w\2\2\u00aa\u00ab\7p\2\2\u00ab\u00ac\7v\2\2\u00ac\u00ad\7k\2\2"+
		"\u00ad\u00ae\7n\2\2\u00ae(\3\2\2\2\u00af\u00b0\7d\2\2\u00b0\u00b1\7t\2"+
		"\2\u00b1\u00b2\7g\2\2\u00b2\u00b3\7c\2\2\u00b3\u00b4\7m\2\2\u00b4*\3\2"+
		"\2\2\u00b5\u00b6\7=\2\2\u00b6,\3\2\2\2\u00b7\u00b8\7?\2\2\u00b8.\3\2\2"+
		"\2\u00b9\u00ba\7t\2\2\u00ba\u00bb\7g\2\2\u00bb\u00bc\7v\2\2\u00bc\u00bd"+
		"\7w\2\2\u00bd\u00be\7t\2\2\u00be\u00bf\7p\2\2\u00bf\60\3\2\2\2\u00c0\u00c1"+
		"\7.\2\2\u00c1\62\3\2\2\2\u00c2\u00c3\7*\2\2\u00c3\64\3\2\2\2\u00c4\u00c6"+
		"\t\2\2\2\u00c5\u00c4\3\2\2\2\u00c6\u00c7\3\2\2\2\u00c7\u00c5\3\2\2\2\u00c7"+
		"\u00c8\3\2\2\2\u00c8\66\3\2\2\2\u00c9\u00cb\t\3\2\2\u00ca\u00c9\3\2\2"+
		"\2\u00cb\u00cc\3\2\2\2\u00cc\u00ca\3\2\2\2\u00cc\u00cd\3\2\2\2\u00cd8"+
		"\3\2\2\2\u00ce\u00cf\7h\2\2\u00cf\u00d0\7w\2\2\u00d0\u00d1\7p\2\2\u00d1"+
		"\u00d2\7e\2\2\u00d2\u00d3\7v\2\2\u00d3\u00d4\7k\2\2\u00d4\u00d5\7q\2\2"+
		"\u00d5\u00d6\7p\2\2\u00d6:\3\2\2\2\u00d7\u00d8\7g\2\2\u00d8\u00d9\7p\2"+
		"\2\u00d9\u00da\7f\2\2\u00da\u00db\7\"\2\2\u00db\u00dc\7h\2\2\u00dc\u00dd"+
		"\7w\2\2\u00dd\u00de\7p\2\2\u00de\u00df\7e\2\2\u00df\u00e0\7v\2\2\u00e0"+
		"\u00e1\7k\2\2\u00e1\u00e2\7q\2\2\u00e2\u00e3\7p\2\2\u00e3<\3\2\2\2\u00e4"+
		"\u00ee\t\4\2\2\u00e5\u00e6\7(\2\2\u00e6\u00ee\7(\2\2\u00e7\u00ee\7~\2"+
		"\2\u00e8\u00e9\7~\2\2\u00e9\u00ee\7~\2\2\u00ea\u00eb\7?\2\2\u00eb\u00ee"+
		"\7?\2\2\u00ec\u00ee\t\5\2\2\u00ed\u00e4\3\2\2\2\u00ed\u00e5\3\2\2\2\u00ed"+
		"\u00e7\3\2\2\2\u00ed\u00e8\3\2\2\2\u00ed\u00ea\3\2\2\2\u00ed\u00ec\3\2"+
		"\2\2\u00ee>\3\2\2\2\u00ef\u00f5\t\6\2\2\u00f0\u00f1\7/\2\2\u00f1\u00f5"+
		"\7/\2\2\u00f2\u00f3\7-\2\2\u00f3\u00f5\7-\2\2\u00f4\u00ef\3\2\2\2\u00f4"+
		"\u00f0\3\2\2\2\u00f4\u00f2\3\2\2\2\u00f5@\3\2\2\2\u00f6\u00f8\7)\2\2\u00f7"+
		"\u00f9\13\2\2\2\u00f8\u00f7\3\2\2\2\u00f8\u00f9\3\2\2\2\u00f9\u00fa\3"+
		"\2\2\2\u00fa\u00fb\7)\2\2\u00fbB\3\2\2\2\u00fc\u0100\7$\2\2\u00fd\u00ff"+
		"\13\2\2\2\u00fe\u00fd\3\2\2\2\u00ff\u0102\3\2\2\2\u0100\u0101\3\2\2\2"+
		"\u0100\u00fe\3\2\2\2\u0101\u0103\3\2\2\2\u0102\u0100\3\2\2\2\u0103\u0104"+
		"\7$\2\2\u0104D\3\2\2\2\u0105\u0106\7\62\2\2\u0106\u0108\t\7\2\2\u0107"+
		"\u0109\t\b\2\2\u0108\u0107\3\2\2\2\u0109\u010a\3\2\2\2\u010a\u0108\3\2"+
		"\2\2\u010a\u010b\3\2\2\2\u010bF\3\2\2\2\u010c\u010d\7\62\2\2\u010d\u010f"+
		"\t\t\2\2\u010e\u0110\t\n\2\2\u010f\u010e\3\2\2\2\u0110\u0111\3\2\2\2\u0111"+
		"\u010f\3\2\2\2\u0111\u0112\3\2\2\2\u0112H\3\2\2\2\u0113\u0115\5\67\34"+
		"\2\u0114\u0113\3\2\2\2\u0115\u0116\3\2\2\2\u0116\u0114\3\2\2\2\u0116\u0117"+
		"\3\2\2\2\u0117J\3\2\2\2\u0118\u0119\7v\2\2\u0119\u011a\7t\2\2\u011a\u011b"+
		"\7w\2\2\u011b\u0122\7g\2\2\u011c\u011d\7h\2\2\u011d\u011e\7c\2\2\u011e"+
		"\u011f\7n\2\2\u011f\u0120\7u\2\2\u0120\u0122\7g\2\2\u0121\u0118\3\2\2"+
		"\2\u0121\u011c\3\2\2\2\u0122L\3\2\2\2\u0123\u0128\5\65\33\2\u0124\u0127"+
		"\5\65\33\2\u0125\u0127\5\67\34\2\u0126\u0124\3\2\2\2\u0126\u0125\3\2\2"+
		"\2\u0127\u012a\3\2\2\2\u0128\u0126\3\2\2\2\u0128\u0129\3\2\2\2\u0129N"+
		"\3\2\2\2\u012a\u0128\3\2\2\2\u012b\u012f\7%\2\2\u012c\u012e\13\2\2\2\u012d"+
		"\u012c\3\2\2\2\u012e\u0131\3\2\2\2\u012f\u0130\3\2\2\2\u012f\u012d\3\2"+
		"\2\2\u0130\u0132\3\2\2\2\u0131\u012f\3\2\2\2\u0132\u0133\7\f\2\2\u0133"+
		"\u0134\3\2\2\2\u0134\u0135\b(\2\2\u0135P\3\2\2\2\u0136\u0138\t\13\2\2"+
		"\u0137\u0136\3\2\2\2\u0138\u0139\3\2\2\2\u0139\u0137\3\2\2\2\u0139\u013a"+
		"\3\2\2\2\u013a\u013b\3\2\2\2\u013b\u013c\b)\2\2\u013cR\3\2\2\2\21\2\u00c7"+
		"\u00cc\u00ed\u00f4\u00f8\u0100\u010a\u0111\u0116\u0121\u0126\u0128\u012f"+
		"\u0139\3\b\2\2";
	public static final ATN _ATN =
		new ATNDeserializer().deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}