// Generated from /Users/tva/ITMO_old/program_models_verification/Paradise.g4 by ANTLR 4.7
import org.antlr.v4.runtime.tree.ParseTreeVisitor;

/**
 * This interface defines a complete generic visitor for a parse tree produced
 * by {@link ParadiseParser}.
 *
 * @param <T> The return type of the visit operation. Use {@link Void} for
 * operations with no return type.
 */
public interface ParadiseVisitor<T> extends ParseTreeVisitor<T> {
	/**
	 * Visit a parse tree produced by {@link ParadiseParser#source}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSource(ParadiseParser.SourceContext ctx);
	/**
	 * Visit a parse tree produced by {@link ParadiseParser#sourceItem}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSourceItem(ParadiseParser.SourceItemContext ctx);
	/**
	 * Visit a parse tree produced by {@link ParadiseParser#functionSignature}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFunctionSignature(ParadiseParser.FunctionSignatureContext ctx);
	/**
	 * Visit a parse tree produced by {@link ParadiseParser#typeRef}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTypeRef(ParadiseParser.TypeRefContext ctx);
	/**
	 * Visit a parse tree produced by {@link ParadiseParser#statement}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitStatement(ParadiseParser.StatementContext ctx);
	/**
	 * Visit a parse tree produced by {@link ParadiseParser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExpr(ParadiseParser.ExprContext ctx);
	/**
	 * Visit a parse tree produced by {@link ParadiseParser#condition}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCondition(ParadiseParser.ConditionContext ctx);
	/**
	 * Visit a parse tree produced by {@link ParadiseParser#if_true_statement}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIf_true_statement(ParadiseParser.If_true_statementContext ctx);
	/**
	 * Visit a parse tree produced by {@link ParadiseParser#if_false_statement}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIf_false_statement(ParadiseParser.If_false_statementContext ctx);
	/**
	 * Visit a parse tree produced by {@link ParadiseParser#argDef}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitArgDef(ParadiseParser.ArgDefContext ctx);
	/**
	 * Visit a parse tree produced by {@link ParadiseParser#argDefList}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitArgDefList(ParadiseParser.ArgDefListContext ctx);
	/**
	 * Visit a parse tree produced by {@link ParadiseParser#exprList}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExprList(ParadiseParser.ExprListContext ctx);
	/**
	 * Visit a parse tree produced by {@link ParadiseParser#identifier}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIdentifier(ParadiseParser.IdentifierContext ctx);
}