// Generated from /Users/tva/ITMO_old/program_models_verification/Paradise.g4 by ANTLR 4.7
import org.antlr.v4.runtime.tree.ParseTreeListener;

/**
 * This interface defines a complete listener for a parse tree produced by
 * {@link ParadiseParser}.
 */
public interface ParadiseListener extends ParseTreeListener {
	/**
	 * Enter a parse tree produced by {@link ParadiseParser#source}.
	 * @param ctx the parse tree
	 */
	void enterSource(ParadiseParser.SourceContext ctx);
	/**
	 * Exit a parse tree produced by {@link ParadiseParser#source}.
	 * @param ctx the parse tree
	 */
	void exitSource(ParadiseParser.SourceContext ctx);
	/**
	 * Enter a parse tree produced by {@link ParadiseParser#sourceItem}.
	 * @param ctx the parse tree
	 */
	void enterSourceItem(ParadiseParser.SourceItemContext ctx);
	/**
	 * Exit a parse tree produced by {@link ParadiseParser#sourceItem}.
	 * @param ctx the parse tree
	 */
	void exitSourceItem(ParadiseParser.SourceItemContext ctx);
	/**
	 * Enter a parse tree produced by {@link ParadiseParser#functionSignature}.
	 * @param ctx the parse tree
	 */
	void enterFunctionSignature(ParadiseParser.FunctionSignatureContext ctx);
	/**
	 * Exit a parse tree produced by {@link ParadiseParser#functionSignature}.
	 * @param ctx the parse tree
	 */
	void exitFunctionSignature(ParadiseParser.FunctionSignatureContext ctx);
	/**
	 * Enter a parse tree produced by {@link ParadiseParser#typeRef}.
	 * @param ctx the parse tree
	 */
	void enterTypeRef(ParadiseParser.TypeRefContext ctx);
	/**
	 * Exit a parse tree produced by {@link ParadiseParser#typeRef}.
	 * @param ctx the parse tree
	 */
	void exitTypeRef(ParadiseParser.TypeRefContext ctx);
	/**
	 * Enter a parse tree produced by {@link ParadiseParser#statement}.
	 * @param ctx the parse tree
	 */
	void enterStatement(ParadiseParser.StatementContext ctx);
	/**
	 * Exit a parse tree produced by {@link ParadiseParser#statement}.
	 * @param ctx the parse tree
	 */
	void exitStatement(ParadiseParser.StatementContext ctx);
	/**
	 * Enter a parse tree produced by {@link ParadiseParser#expr}.
	 * @param ctx the parse tree
	 */
	void enterExpr(ParadiseParser.ExprContext ctx);
	/**
	 * Exit a parse tree produced by {@link ParadiseParser#expr}.
	 * @param ctx the parse tree
	 */
	void exitExpr(ParadiseParser.ExprContext ctx);
	/**
	 * Enter a parse tree produced by {@link ParadiseParser#condition}.
	 * @param ctx the parse tree
	 */
	void enterCondition(ParadiseParser.ConditionContext ctx);
	/**
	 * Exit a parse tree produced by {@link ParadiseParser#condition}.
	 * @param ctx the parse tree
	 */
	void exitCondition(ParadiseParser.ConditionContext ctx);
	/**
	 * Enter a parse tree produced by {@link ParadiseParser#if_true_statement}.
	 * @param ctx the parse tree
	 */
	void enterIf_true_statement(ParadiseParser.If_true_statementContext ctx);
	/**
	 * Exit a parse tree produced by {@link ParadiseParser#if_true_statement}.
	 * @param ctx the parse tree
	 */
	void exitIf_true_statement(ParadiseParser.If_true_statementContext ctx);
	/**
	 * Enter a parse tree produced by {@link ParadiseParser#if_false_statement}.
	 * @param ctx the parse tree
	 */
	void enterIf_false_statement(ParadiseParser.If_false_statementContext ctx);
	/**
	 * Exit a parse tree produced by {@link ParadiseParser#if_false_statement}.
	 * @param ctx the parse tree
	 */
	void exitIf_false_statement(ParadiseParser.If_false_statementContext ctx);
	/**
	 * Enter a parse tree produced by {@link ParadiseParser#argDef}.
	 * @param ctx the parse tree
	 */
	void enterArgDef(ParadiseParser.ArgDefContext ctx);
	/**
	 * Exit a parse tree produced by {@link ParadiseParser#argDef}.
	 * @param ctx the parse tree
	 */
	void exitArgDef(ParadiseParser.ArgDefContext ctx);
	/**
	 * Enter a parse tree produced by {@link ParadiseParser#argDefList}.
	 * @param ctx the parse tree
	 */
	void enterArgDefList(ParadiseParser.ArgDefListContext ctx);
	/**
	 * Exit a parse tree produced by {@link ParadiseParser#argDefList}.
	 * @param ctx the parse tree
	 */
	void exitArgDefList(ParadiseParser.ArgDefListContext ctx);
	/**
	 * Enter a parse tree produced by {@link ParadiseParser#exprList}.
	 * @param ctx the parse tree
	 */
	void enterExprList(ParadiseParser.ExprListContext ctx);
	/**
	 * Exit a parse tree produced by {@link ParadiseParser#exprList}.
	 * @param ctx the parse tree
	 */
	void exitExprList(ParadiseParser.ExprListContext ctx);
	/**
	 * Enter a parse tree produced by {@link ParadiseParser#identifier}.
	 * @param ctx the parse tree
	 */
	void enterIdentifier(ParadiseParser.IdentifierContext ctx);
	/**
	 * Exit a parse tree produced by {@link ParadiseParser#identifier}.
	 * @param ctx the parse tree
	 */
	void exitIdentifier(ParadiseParser.IdentifierContext ctx);
}