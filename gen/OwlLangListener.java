// Generated from /Users/tva/workspace/program_models_verification/Paradise.g4 by ANTLR 4.7
import org.antlr.v4.runtime.tree.ParseTreeListener;

/**
 * This interface defines a complete listener for a parse tree produced by
 * {@link ParadiseParser}.
 */
public interface ParadiseListener extends ParseTreeListener {
	/**
	 * Enter a parse tree produced by the {@code SourceRule}
	 * labeled alternative in {@link ParadiseParser#source}.
	 * @param ctx the parse tree
	 */
	void enterSourceRule(ParadiseParser.SourceRuleContext ctx);
	/**
	 * Exit a parse tree produced by the {@code SourceRule}
	 * labeled alternative in {@link ParadiseParser#source}.
	 * @param ctx the parse tree
	 */
	void exitSourceRule(ParadiseParser.SourceRuleContext ctx);
	/**
	 * Enter a parse tree produced by the {@code SourceItemRule}
	 * labeled alternative in {@link ParadiseParser#sourceItem}.
	 * @param ctx the parse tree
	 */
	void enterSourceItemRule(ParadiseParser.SourceItemRuleContext ctx);
	/**
	 * Exit a parse tree produced by the {@code SourceItemRule}
	 * labeled alternative in {@link ParadiseParser#sourceItem}.
	 * @param ctx the parse tree
	 */
	void exitSourceItemRule(ParadiseParser.SourceItemRuleContext ctx);
	/**
	 * Enter a parse tree produced by the {@code FunctionSignatureRule}
	 * labeled alternative in {@link ParadiseParser#functionSignature}.
	 * @param ctx the parse tree
	 */
	void enterFunctionSignatureRule(ParadiseParser.FunctionSignatureRuleContext ctx);
	/**
	 * Exit a parse tree produced by the {@code FunctionSignatureRule}
	 * labeled alternative in {@link ParadiseParser#functionSignature}.
	 * @param ctx the parse tree
	 */
	void exitFunctionSignatureRule(ParadiseParser.FunctionSignatureRuleContext ctx);
	/**
	 * Enter a parse tree produced by the {@code BuiltInBoolType}
	 * labeled alternative in {@link ParadiseParser#typeRef}.
	 * @param ctx the parse tree
	 */
	void enterBuiltInBoolType(ParadiseParser.BuiltInBoolTypeContext ctx);
	/**
	 * Exit a parse tree produced by the {@code BuiltInBoolType}
	 * labeled alternative in {@link ParadiseParser#typeRef}.
	 * @param ctx the parse tree
	 */
	void exitBuiltInBoolType(ParadiseParser.BuiltInBoolTypeContext ctx);
	/**
	 * Enter a parse tree produced by the {@code BuiltInByteType}
	 * labeled alternative in {@link ParadiseParser#typeRef}.
	 * @param ctx the parse tree
	 */
	void enterBuiltInByteType(ParadiseParser.BuiltInByteTypeContext ctx);
	/**
	 * Exit a parse tree produced by the {@code BuiltInByteType}
	 * labeled alternative in {@link ParadiseParser#typeRef}.
	 * @param ctx the parse tree
	 */
	void exitBuiltInByteType(ParadiseParser.BuiltInByteTypeContext ctx);
	/**
	 * Enter a parse tree produced by the {@code BuiltInIntType}
	 * labeled alternative in {@link ParadiseParser#typeRef}.
	 * @param ctx the parse tree
	 */
	void enterBuiltInIntType(ParadiseParser.BuiltInIntTypeContext ctx);
	/**
	 * Exit a parse tree produced by the {@code BuiltInIntType}
	 * labeled alternative in {@link ParadiseParser#typeRef}.
	 * @param ctx the parse tree
	 */
	void exitBuiltInIntType(ParadiseParser.BuiltInIntTypeContext ctx);
	/**
	 * Enter a parse tree produced by the {@code BuiltInUintType}
	 * labeled alternative in {@link ParadiseParser#typeRef}.
	 * @param ctx the parse tree
	 */
	void enterBuiltInUintType(ParadiseParser.BuiltInUintTypeContext ctx);
	/**
	 * Exit a parse tree produced by the {@code BuiltInUintType}
	 * labeled alternative in {@link ParadiseParser#typeRef}.
	 * @param ctx the parse tree
	 */
	void exitBuiltInUintType(ParadiseParser.BuiltInUintTypeContext ctx);
	/**
	 * Enter a parse tree produced by the {@code BuiltInLongType}
	 * labeled alternative in {@link ParadiseParser#typeRef}.
	 * @param ctx the parse tree
	 */
	void enterBuiltInLongType(ParadiseParser.BuiltInLongTypeContext ctx);
	/**
	 * Exit a parse tree produced by the {@code BuiltInLongType}
	 * labeled alternative in {@link ParadiseParser#typeRef}.
	 * @param ctx the parse tree
	 */
	void exitBuiltInLongType(ParadiseParser.BuiltInLongTypeContext ctx);
	/**
	 * Enter a parse tree produced by the {@code BuiltInUlongType}
	 * labeled alternative in {@link ParadiseParser#typeRef}.
	 * @param ctx the parse tree
	 */
	void enterBuiltInUlongType(ParadiseParser.BuiltInUlongTypeContext ctx);
	/**
	 * Exit a parse tree produced by the {@code BuiltInUlongType}
	 * labeled alternative in {@link ParadiseParser#typeRef}.
	 * @param ctx the parse tree
	 */
	void exitBuiltInUlongType(ParadiseParser.BuiltInUlongTypeContext ctx);
	/**
	 * Enter a parse tree produced by the {@code BuiltInCharType}
	 * labeled alternative in {@link ParadiseParser#typeRef}.
	 * @param ctx the parse tree
	 */
	void enterBuiltInCharType(ParadiseParser.BuiltInCharTypeContext ctx);
	/**
	 * Exit a parse tree produced by the {@code BuiltInCharType}
	 * labeled alternative in {@link ParadiseParser#typeRef}.
	 * @param ctx the parse tree
	 */
	void exitBuiltInCharType(ParadiseParser.BuiltInCharTypeContext ctx);
	/**
	 * Enter a parse tree produced by the {@code BuiltInStringType}
	 * labeled alternative in {@link ParadiseParser#typeRef}.
	 * @param ctx the parse tree
	 */
	void enterBuiltInStringType(ParadiseParser.BuiltInStringTypeContext ctx);
	/**
	 * Exit a parse tree produced by the {@code BuiltInStringType}
	 * labeled alternative in {@link ParadiseParser#typeRef}.
	 * @param ctx the parse tree
	 */
	void exitBuiltInStringType(ParadiseParser.BuiltInStringTypeContext ctx);
	/**
	 * Enter a parse tree produced by the {@code CustomType}
	 * labeled alternative in {@link ParadiseParser#typeRef}.
	 * @param ctx the parse tree
	 */
	void enterCustomType(ParadiseParser.CustomTypeContext ctx);
	/**
	 * Exit a parse tree produced by the {@code CustomType}
	 * labeled alternative in {@link ParadiseParser#typeRef}.
	 * @param ctx the parse tree
	 */
	void exitCustomType(ParadiseParser.CustomTypeContext ctx);
	/**
	 * Enter a parse tree produced by the {@code IfStatement}
	 * labeled alternative in {@link ParadiseParser#statement}.
	 * @param ctx the parse tree
	 */
	void enterIfStatement(ParadiseParser.IfStatementContext ctx);
	/**
	 * Exit a parse tree produced by the {@code IfStatement}
	 * labeled alternative in {@link ParadiseParser#statement}.
	 * @param ctx the parse tree
	 */
	void exitIfStatement(ParadiseParser.IfStatementContext ctx);
	/**
	 * Enter a parse tree produced by the {@code WhileStatement}
	 * labeled alternative in {@link ParadiseParser#statement}.
	 * @param ctx the parse tree
	 */
	void enterWhileStatement(ParadiseParser.WhileStatementContext ctx);
	/**
	 * Exit a parse tree produced by the {@code WhileStatement}
	 * labeled alternative in {@link ParadiseParser#statement}.
	 * @param ctx the parse tree
	 */
	void exitWhileStatement(ParadiseParser.WhileStatementContext ctx);
	/**
	 * Enter a parse tree produced by the {@code DoWhileStatement}
	 * labeled alternative in {@link ParadiseParser#statement}.
	 * @param ctx the parse tree
	 */
	void enterDoWhileStatement(ParadiseParser.DoWhileStatementContext ctx);
	/**
	 * Exit a parse tree produced by the {@code DoWhileStatement}
	 * labeled alternative in {@link ParadiseParser#statement}.
	 * @param ctx the parse tree
	 */
	void exitDoWhileStatement(ParadiseParser.DoWhileStatementContext ctx);
	/**
	 * Enter a parse tree produced by the {@code BreakStatement}
	 * labeled alternative in {@link ParadiseParser#statement}.
	 * @param ctx the parse tree
	 */
	void enterBreakStatement(ParadiseParser.BreakStatementContext ctx);
	/**
	 * Exit a parse tree produced by the {@code BreakStatement}
	 * labeled alternative in {@link ParadiseParser#statement}.
	 * @param ctx the parse tree
	 */
	void exitBreakStatement(ParadiseParser.BreakStatementContext ctx);
	/**
	 * Enter a parse tree produced by the {@code AssignmentStatement}
	 * labeled alternative in {@link ParadiseParser#statement}.
	 * @param ctx the parse tree
	 */
	void enterAssignmentStatement(ParadiseParser.AssignmentStatementContext ctx);
	/**
	 * Exit a parse tree produced by the {@code AssignmentStatement}
	 * labeled alternative in {@link ParadiseParser#statement}.
	 * @param ctx the parse tree
	 */
	void exitAssignmentStatement(ParadiseParser.AssignmentStatementContext ctx);
	/**
	 * Enter a parse tree produced by the {@code ExpressionStatement}
	 * labeled alternative in {@link ParadiseParser#statement}.
	 * @param ctx the parse tree
	 */
	void enterExpressionStatement(ParadiseParser.ExpressionStatementContext ctx);
	/**
	 * Exit a parse tree produced by the {@code ExpressionStatement}
	 * labeled alternative in {@link ParadiseParser#statement}.
	 * @param ctx the parse tree
	 */
	void exitExpressionStatement(ParadiseParser.ExpressionStatementContext ctx);
	/**
	 * Enter a parse tree produced by the {@code BinaryExpression}
	 * labeled alternative in {@link ParadiseParser#expr}.
	 * @param ctx the parse tree
	 */
	void enterBinaryExpression(ParadiseParser.BinaryExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code BinaryExpression}
	 * labeled alternative in {@link ParadiseParser#expr}.
	 * @param ctx the parse tree
	 */
	void exitBinaryExpression(ParadiseParser.BinaryExpressionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code PrefixUnaryExpression}
	 * labeled alternative in {@link ParadiseParser#expr}.
	 * @param ctx the parse tree
	 */
	void enterPrefixUnaryExpression(ParadiseParser.PrefixUnaryExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code PrefixUnaryExpression}
	 * labeled alternative in {@link ParadiseParser#expr}.
	 * @param ctx the parse tree
	 */
	void exitPrefixUnaryExpression(ParadiseParser.PrefixUnaryExpressionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code LiteralExpression}
	 * labeled alternative in {@link ParadiseParser#expr}.
	 * @param ctx the parse tree
	 */
	void enterLiteralExpression(ParadiseParser.LiteralExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code LiteralExpression}
	 * labeled alternative in {@link ParadiseParser#expr}.
	 * @param ctx the parse tree
	 */
	void exitLiteralExpression(ParadiseParser.LiteralExpressionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code PostfixUnaryExpression}
	 * labeled alternative in {@link ParadiseParser#expr}.
	 * @param ctx the parse tree
	 */
	void enterPostfixUnaryExpression(ParadiseParser.PostfixUnaryExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code PostfixUnaryExpression}
	 * labeled alternative in {@link ParadiseParser#expr}.
	 * @param ctx the parse tree
	 */
	void exitPostfixUnaryExpression(ParadiseParser.PostfixUnaryExpressionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code PlaceExpression}
	 * labeled alternative in {@link ParadiseParser#expr}.
	 * @param ctx the parse tree
	 */
	void enterPlaceExpression(ParadiseParser.PlaceExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code PlaceExpression}
	 * labeled alternative in {@link ParadiseParser#expr}.
	 * @param ctx the parse tree
	 */
	void exitPlaceExpression(ParadiseParser.PlaceExpressionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code CallExpression}
	 * labeled alternative in {@link ParadiseParser#expr}.
	 * @param ctx the parse tree
	 */
	void enterCallExpression(ParadiseParser.CallExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code CallExpression}
	 * labeled alternative in {@link ParadiseParser#expr}.
	 * @param ctx the parse tree
	 */
	void exitCallExpression(ParadiseParser.CallExpressionContext ctx);
	/**
	 * Enter a parse tree produced by {@link ParadiseParser#argDef}.
	 * @param ctx the parse tree
	 */
	void enterArgDef(ParadiseParser.ArgDefContext ctx);
	/**
	 * Exit a parse tree produced by {@link ParadiseParser#argDef}.
	 * @param ctx the parse tree
	 */
	void exitArgDef(ParadiseParser.ArgDefContext ctx);
	/**
	 * Enter a parse tree produced by {@link ParadiseParser#argDefList}.
	 * @param ctx the parse tree
	 */
	void enterArgDefList(ParadiseParser.ArgDefListContext ctx);
	/**
	 * Exit a parse tree produced by {@link ParadiseParser#argDefList}.
	 * @param ctx the parse tree
	 */
	void exitArgDefList(ParadiseParser.ArgDefListContext ctx);
	/**
	 * Enter a parse tree produced by {@link ParadiseParser#exprList}.
	 * @param ctx the parse tree
	 */
	void enterExprList(ParadiseParser.ExprListContext ctx);
	/**
	 * Exit a parse tree produced by {@link ParadiseParser#exprList}.
	 * @param ctx the parse tree
	 */
	void exitExprList(ParadiseParser.ExprListContext ctx);
}